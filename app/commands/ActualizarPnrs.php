<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ActualizarPnrs extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'pnrs:actualizar';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Actualiza pnrs y emisiones.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// Se encargara de actualizar estados de pnrs y emisiones que probablemente no han sido procesados

		// Establecemos como tiempo limite de espera de las emisiones, un total de 5 minutos
			$tiempo_limite 	= strtotime('-5 minutes');
			$actualizacion 	= Emision::where('created_at', '<=', date('Y-m-d H:i:s', $tiempo_limite))->update(array('estado' => 2));

		// Actualizamos los pnrs que estan registrados en boliviabooking
			$pnrs 	= Pnr::where('estado', '=', 1)->get();
			$a_pnrs = array();
			foreach ($pnrs as $pnr) 
			{
				$a_pnrs[] 	= (object)array('id' 		=> $pnr->id, 
											'codigo' 	=> $pnr->codigo, 
											'estado' 	=> 1);
			}

			if(count($a_pnrs) > 0)
			{
				// Obtenemos estado de cada reserva
				$postdata = http_build_query(
				    array(
				        'consulta' => json_encode($a_pnrs)
				    )
				);

				$result = AirConexion::consultar('estados_pnr', $postdata);	

				if(!property_exists($result, 'error'))
				{
					foreach ($result->respuesta as $pnr) 
					{
						if($pnr->estado != 1)
							$actualizacion = Pnr::where('id', '=', $pnr->id)->update(array('estado' => $pnr->estado));
					}
				}
				else
					echo "ERROR EN ACTUALIZAR ESTADOS DE PNR";
			}
			

		echo "PROCESO DE ACTUALIZACION COMPLETADA !!!\n";
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			/*array('example', InputArgument::REQUIRED, 'An example argument.'),*/
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
