@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-md-8">
                        
                        <h4 class="classic-title"><span>Itinerario Seleccionado</span></h4>

                        <div class="row latest-posts-classic">

                            @define $fecha_salida_vuelo = -1

                        	@foreach(Session::get('seleccionado')->orig_dest as $tramo)

                        		<h4 class="tramo-itinerario">{{strtoupper($tramo->tramo->tipo_tramo)}}</h4>

                        		<div class="panel-tramo">
                        		
	                        		@foreach($tramo->segmentos as $segmento)

                                        @if($fecha_salida_vuelo < 0)
                                            @define $fecha_salida_vuelo = strtotime($segmento->fecha_salida)
                                        @endif
		                            
	                        			<div class="clear-segment"></div>

			                            <div class="col-sm-1 col-xs-2 post-row">
			                                <div class="left-meta-post">
			                                    <div class="post-date"><span class="day">{{date('d', strtotime($segmento->fecha_salida))}}</span><span class="month">{{date('M', strtotime($segmento->fecha_salida))}}</span></div>
			                                </div>
			                            </div>
			                            <div class="col-sm-10 col-xs-10">
			                                <div class="row">
			                                    <div class="col-sm-3 aepto-tramo">
			                                        <i class="fa fa-arrow-right icon-air-tramo"></i>
			                                        <div class="tramo-city">{{$segmento->nombre_origen}}({{$segmento->origen}})</div>
			                                        <div class="aeropuerto">{{$segmento->nombre_aepto_origen}}</div>
			                                        <div>{{$segmento->hora_salida}}</div>
			                                    </div>
			                                    <div class="col-sm-3 aepto-tramo">
			                                        <div class="tramo-city">{{$segmento->nombre_destino}}({{$segmento->destino}})</div>
			                                        <div class="aeropuerto">{{$segmento->nombre_aepto_destino}}</div>
			                                        <div>{{$segmento->hora_llegada}} 
			                                        	@if($segmento->dias_despues > 0)
	                                                        @if($segmento->dias_despues == 1)
	                                                            <span class="dias-transc">+1d&iacute;a</span>
	                                                        @else
	                                                            <span class="dias-transc">+{{$segmento->dias_despues}}d&iacute;as</span>
	                                                        @endif
	                                                    @endif
			                                        </div>
			                                    </div>
			                                    <div class="col-sm-4 descr-vuelo">
			                                        <div>{{$segmento->t_vuelo_literal}} - Vuelo {{$segmento->nro_vuelo}}</div>
			                                        <div>{{Session::get('busqueda')->cabina}}({{$segmento->clase_srv}})</div>
			                                        <div>Operado por {{$segmento->nombre_linea_op}}</div>
			                                    </div>
			                                    <div class="col-sm-2">
			                                    	{{HTML::image('http://viacontours.com/air-ws/public/airline/' . $segmento->mark_air . '.png', '', array('class' => 'price-airline'))}}
			                                    </div>
			                                    <div class="clear"></div>
			                                    
			                                    @if(property_exists($segmento, 'paradas'))

													@foreach($segmento->paradas as $parada)
						                            
					                                    <div class="col-sm-12 air-stop">
					                                        Parada en {{$parada->nombre_aepto}}({{$parada->aepto}}) <strong>Llega</strong> {{$parada->hora_llega}} <strong>Sale</strong> {{$parada->hora_sale}}
					                                    </div>
					                                    <div class="clear"></div>

					                                @endforeach

					                            @endif

			                                </div>
			                            </div>

		                            @endforeach

		                            <div class="clear"></div>

	                            </div>

                            @endforeach

                        </div>

                        

                    </div>

                    <div class="col-md-4">

                        <h4 class="classic-title"><span>Precios</span></h4>

                        <div class="row prices">
                            <div class="col-xs-12"><h4>Por Persona</h4></div>

                            @foreach(Session::get('seleccionado')->precios as $precio)

	                            <div class="col-xs-7">{{$precio->tipo_pax}} :</div>
	                            <div class="col-xs-5">{{str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), Session::get('seleccionado')->costo->moneda)}} {{ceil($precio->precio_final_pax)}}</div>

                            @endforeach

                            <div class="col-xs-12"><h4>Totales</h4></div>

                            @foreach(Session::get('seleccionado')->precios as $precio)

	                            <div class="col-xs-7">{{$precio->cant}} {{$precio->tipo_pax_p}} :</div>
	                            <div class="col-xs-5">{{str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), Session::get('seleccionado')->costo->moneda)}} {{ceil($precio->precio_final_subtotal)}}</div>

	                        @endforeach

                            <div class="col-xs-12 total-price">
                                <div>Precio Total</div>
                                <div class="total-amount">{{str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), Session::get('seleccionado')->costo->moneda)}} {{Session::get('seleccionado')->costo->precio_final}}</div>
                            </div>

                            <div class="clear"></div>

							@if(Session::get('seleccionado')->cambio)

								<!-- MENSAJE COSTO -->
									<div class="col-sm-12 loading hidden">
					
										&iquest; Por qu&eacute; cambi&oacute; el precio ?
										
										<div class="clear"></div>

										<div class="col-sm-12 panel-explain err">
						                    <ul>
						                        <li>A veces los espacios mostrados con ciertas tarifas ya fueron tomados justo en el momento.</li>
						                        <li>Existen aerol&iacute;neas que ocasionan variaciones en el precio por no operar en Bolivia.</li>
						                        <li>Si lo desea, ll&aacute;menos y lo atenderemos con gusto.</li>
						                    </ul>
						                </div>

									</div>
								<!-- /MENSAJE COSTO -->

							@endif

                            @if(property_exists(Session::get('seleccionado'), 'es_tarifa_operadora'))

                                <div class="hidden-separator"></div>
                                <div class="col-sm-12">
                                    <div class="alert alert-warning" role="alert">
                                        <strong>Importante: </strong>
                                        Esta tarifa s&oacute;lo podr&aacute; ser emitida con la compra del hotel.
                                    </div>
                                </div>

                            @endif

                        </div>
                        
                    </div>

                    <div class="hidden-separator"></div>

                    @if(count(Session::get('avisos')) > 0)
                        
                        @foreach(Session::get('avisos') as $aviso)
                            
                            <div class="col-md-12">
                            	<div class="alert alert-danger">
                            		<div class="col-xs-1">
                            			<i class="fa fa-exclamation-triangle warning-itinerario"></i>
                            		</div>
                            		<div class="col-xs-11">{{$aviso}}</div>
                            		<div class="clear"></div>
                            	</div>
                            </div>
                            <div class="mini-hidden-separator"></div>

                        @endforeach

                    @endif

                    <div class="col-md-12">

                        <h4 class="classic-title"><span>Datos de Pasajeros</span></h4>
                        
                        {{Form::open(array('url' => 'cierre_reserva', 'class' => 'submit-booking'))}}

                        	@foreach(Session::get('seleccionado')->precios as $precio)

								@for($i = 1; $i <= $precio->cant; $i++)

		                            <div class="col-sm-12">
		                                <h4 title="Buscar Pasajero" class="title-pax" id="{{$precio->pax . $i}}">
		                                    {{strtoupper($precio->tipo_pax)}} {{$i}}
		                                    <!--<i class="fa fa-search icon-search-pax"></i>-->
		                                </h4>
		                            </div>
		                            <div class="panel-pax">
		                                
		                                <div class="col-sm-2">
		                                    {{Form::label('', 'Nombre *')}}
		                                    {{Form::text('nombre_' . $precio->pax . $i, Session::get('datos_pasajeros')['nombre_' . $precio->pax . $i], array('class' => 'field-required', 'id' => 'nombre_' . $precio->pax . $i))}}
		                                    {{Form::hidden('id_' . $precio->pax . $i, '', array('id' => 'id_' . $precio->pax . $i))}}
		                                </div>
		                                <div class="col-sm-2">
		                                    {{Form::label('', 'Apellido *')}}
		                                    {{Form::text('apellido_' . $precio->pax . $i, Session::get('datos_pasajeros')['apellido_' . $precio->pax . $i], array('class' => 'field-required', 'id' => 'apellido_' . $precio->pax . $i))}}
		                                </div>

		                                @if(Session::get('info')->carnet || Session::get('info')->pasaporte)
			                                
			                                <div class="col-sm-2">
			                                    @define $document = 'Pasaporte *'
												@if(Session::get('info')->carnet)
													@define $document = 'Carnet o Pasaporte *'
												@endif
												
												{{Form::label('', $document)}}
			                                    {{Form::text('documento_' . $precio->pax . $i, Session::get('datos_pasajeros')['documento_' . $precio->pax . $i], array('class' => 'field-required', 'id' => 'documento_' . $precio->pax . $i))}}
			                                </div>
			                                <div class="col-sm-2">
			                                    {{Form::label('', 'Pais de Documento')}}
			                                    <select name="pais_{{$precio->pax . $i}}" id="pais_{{$precio->pax . $i}}">
													@foreach($paises as $pais)
														<option value="{{$pais->codigo_iata}}" 
															@if($pais->codigo_iata == 'BO')
																{{'selected'}}
															@endif
														>{{$pais->nombre}}</option>
													@endforeach
												</select>
			                                </div>

			                            @endif

		                                <div class="col-sm-2">
		                                    {{Form::label('', 'Nit')}}
		                                    {{Form::text('nit_' . $precio->pax . $i, Session::get('datos_pasajeros')['nit_' . $precio->pax . $i], array('id' => 'nit_' . $precio->pax . $i))}}
		                                </div>
		                                <div class="col-sm-2">
		                                    {{Form::label('', 'Correo')}}
		                                    {{Form::text('correo_' . $precio->pax . $i, Session::get('datos_pasajeros')['correo_' . $precio->pax . $i], array('id' => 'correo_' . $precio->pax . $i))}}
		                                </div>
		                                <div class="clear"></div>
		                                <div class="panel-buscar-pax">
		                                    <div class="buscar-pasajero panel-pax{{$precio->pax . $i}}">
		                                        <i class="fa fa-times icon-close" id="{{$precio->pax . $i}}"></i>
		                                        <div class="busqueda_pasajeros{{$precio->pax . $i}}">
		                                        </div>
		                                    </div>
		                                </div>
		                                
		                                @if(Session::get('info')->sec_fly || $precio->pax == 'INF')
			                                
			                                <div class="col-sm-2">
			                                    {{Form::label('', 'Nacimiento *')}}
			                                    {{Form::text('nacimiento_' . $precio->pax . $i, Session::get('datos_pasajeros')['nacimiento_' . $precio->pax . $i], array('id' => 'nacimiento_' . $precio->pax . $i, 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10', 'class' => 'field-required fecha_nac_' . $precio->pax))}}
			                                </div>
			                                <div class="col-sm-2">
			                                    {{Form::label('', 'Sexo')}}
			                                    {{Form::select('sexo_' . $precio->pax . $i, array('M' => 'Varón','F' => 'Mujer'), Session::get('datos_pasajeros')['sexo_' . $precio->pax . $i], array('id' => 'sexo_' . $precio->pax . $i))}}
			                                </div>

		                                @endif

		                                <div class="col-sm-2">
		                                    {{Form::label('', 'Viajero frecuente')}}
		                                    {{Form::text('ffly_' . $precio->pax . $i, Session::get('datos_pasajeros')['ffly_' . $precio->pax . $i], array('placeholder' => 'Código', 'id' => 'ffly_' . $precio->pax . $i))}}
		                                </div>
		                                <div class="col-sm-2">
		                                    {{Form::label('', 'Aerolínea ViajFrec')}}
		                                    <select name="programa_aerolinea_{{$precio->pax . $i}}" id="programa_aerolinea_{{$precio->pax . $i}}">
												@foreach(Session::get('info')->aerolineas as $aerolinea)
													<option value="{{$aerolinea->codigo_iata_linea_aerea}}">
														@if(trim($aerolinea->nombre_programa_linea_aerea) != "")
															{{$aerolinea->nombre_programa_linea_aerea}}
														@else
															{{$aerolinea->nombre_linea_aerea}}
														@endif
													</option>
												@endforeach
											</select>
		                                </div>
		                                <div class="col-sm-2">
		                                    {{Form::label('', 'Teléfono')}}
		                                    {{Form::text('telefono_' . $precio->pax . $i, Session::get('datos_pasajeros')['telefono_' . $precio->pax . $i], array('id' => 'telefono_' . $precio->pax . $i))}}
		                                </div>
		                                <div class="col-sm-2">
		                                	{{Form::label('', 'Ciudad Teléfono')}}
		                                	<select name="cod_telefono_{{$precio->pax . $i}}" id="cod_telefono_{{$precio->pax . $i}}">
												@foreach($ciudades as $ciudad)
													<option value="{{$ciudad->codigo_iata}}" 
														@if($ciudad->codigo_iata == 'SRZ')
															{{'selected'}}
														@endif
													>{{$ciudad->nombre}}</option>
												@endforeach
											</select>
		                                </div>

		                            </div>
		                            <div class="clear"></div>

                            	@endfor

                            @endforeach

                            <div class="hidden-separator"></div>

                            <div class="panel-pax">
                                <div class="col-sm-6">
                                    {{Form::label('', 'Teléfono de Contacto')}}
                                    <div class="row">
                                    	<div class="col-sm-6">
                                    		{{Form::text('num_telefono', '', array('placeholder' => 'Número', 'class' => 'field-required'))}}
                                    	</div>
                                    	<div class="col-sm-6">
                                    		<select name="cod_telefono">
												@foreach($ciudades as $ciudad)
													@if($ciudad->codigo_iata == '')
														@define $ciudad->codigo_iata = 'SRZ'
													@endif
													<option value="{{$ciudad->codigo_iata}}" 
                                                        @if($ciudad->codigo_iata == 'SRZ')
                                                            {{'selected'}}
                                                        @endif
                                                    >{{$ciudad->nombre}}</option>
												@endforeach
											</select>
                                    	</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    {{Form::label('', 'Email')}}
                                    {{Form::text('email', '', array('class' => 'field-required'))}}
                                </div>
                            </div>

                            <div class="hidden-separator"></div>

                            <div class="col-sm-6">
                            	<a href="consulta_atras" > 
                            		{{Form::button('Volver', array('class' => 'btn-volver'))}}
                            	</a>
                            </div>
                            <div class="col-sm-6">
                                {{Form::hidden('fecha_actual', date('d', $fecha_salida_vuelo), array('id' => 'fecha_actual'))}}
                                {{Form::hidden('mes_actual', date('m', $fecha_salida_vuelo), array('id' => 'mes_actual'))}}
                                {{Form::hidden('anio_actual', date('Y', $fecha_salida_vuelo), array('id' => 'anio_actual'))}}
                            	{{Form::submit('Reservar')}}
                            </div>

                        {{Form::close()}}

                        <!-- LOADING -->
							<div class="col-sm-12 loading hidden">
					
								<div class="clear"></div>

								{{HTML::image('images/loading_vuelos.gif', '')}}
								
								<div class="clear"></div>

								Un momento por favor ...
								
								<div class="clear"></div>

								<span class="msj-booking">
									Estamos procesando la reserva.
								</span>

								<div class="clear"></div>

							</div>
						<!-- /LOADING -->

						<!-- ERROR -->
							<div class="col-sm-12 error-panel hidden">
					
								<div class="clear"></div>

								{{HTML::image('images/oops_icon.png', '')}}
								
								<div class="clear"></div>

								<h4>Lo sentimos, por favor intente nuevamente o cont&aacute;ctenos.</h4>

								<div class="clear"></div>

								<div class="col-sm-12 panel-explain err">
									<ul>
										<li>El proceso de reservaci&oacute;n no ha sido completado.</li>
										<li>El tiempo de espera para los resultados se ha excedido.</li>
										<li>Verifique su conexi&oacute;n a Internet.</li>
									</ul>
								</div>

								<div class="clear"></div>

								<a href="#" class="show-form">Mostrar Formulario</a>

							</div>
						<!-- /ERROR -->

						<!-- ERROR -->
							<div class="col-sm-12 duplicado-panel hidden">
					
								<div class="clear"></div>

								{{HTML::image('images/oops_icon.png', '')}}
								
								<div class="clear"></div>

								<h4>No es posible cerrar la reserva por duplicidad.</h4>

								<div class="clear"></div>

								<div class="col-sm-12 panel-explain err">
									<ul>
										<li>El proceso de reservaci&oacute;n no ha sido completado.</li>
										<li>Ya existen reservas con el mismo pasajero en la misma fecha.</li>
										<li>Cons&uacute;ltenos y solicite la anulaci&oacute;n de las reservas.</li>
									</ul>
								</div>

								<div class="clear"></div>

								<a href="#" class="show-form">Mostrar Formulario</a>

							</div>
						<!-- /ERROR -->

                    </div>
                    
                </div>

            </div>
        </div>
    <!-- End content -->

@stop

@section('code_js')

	{{HTML::script('js/datos_pax.js?201511260711')}}

@stop