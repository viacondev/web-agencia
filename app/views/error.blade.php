@extends('layouts.master')

@section('content')

	<div class="container">
		<div class="row">

			<div class="col-sm-12 page404">
				<h1>404</h1>
				<p>No podemos encontrar la página.</p>
				<p><a class="btn" href="aereo" title="HOMEPAGE">INICIO</a></p>
			</div>

		</div>
	</div>

@stop