<div class="header-pax hidden-xs">
    <div class="col-sm-3">Pasajero</div>
    <div class="col-sm-2">Telf</div>
    <div class="col-sm-2">FNac/Sexo</div>
    <div class="col-sm-2">Documento</div>
    <div class="col-sm-2">Viaj Frec</div>
    <div class="col-sm-2"></div>
</div>

<div class="pax-separator hidden-xs"></div>

@foreach($pasajeros as $key => $pasajero)

	<div class="item-pax">
	    <div class="col-sm-3">{{$pasajero->nombre}} {{$pasajero->primer_apellido}} {{$pasajero->segundo_apellido}}</div>
	    <div class="col-sm-2">
	    	<select id="telf{{$id}}-{{$key}}">
	        	@foreach($pasajero->telefonos as $telf)
	            	<option data-numero="{{$telf->numero}}" data-ciudad="{{$telf->ciudad->codigo_iata}}" >{{$telf->numero}} - {{$telf->ciudad->codigo_iata}}</option>
	            @endforeach
	        </select>
	    </div>
	    <div class="col-sm-2">{{date('d/m/Y', strtotime($pasajero->fecha_nacimiento))}} - {{$pasajero->sexo}}</div>
	    <div class="col-sm-2">
	        <select id="doc{{$id}}-{{$key}}">
	        	@foreach($pasajero->documentos as $doc)
	            	<option data-numero="{{$doc->numero}}" data-tipodoc="{{$doc->tipo_doc}}" data-pais="{{$doc->pais->codigo_iata}}" >{{$doc->numero}} - {{$doc->pais->codigo_iata}} - {{$doc->tipo_doc}}</option>
	            @endforeach
	        </select>
	    </div>
	    <div class="col-sm-2">
	        <select id="ff{{$id}}-{{$key}}">
	        	@foreach($pasajero->frequent_fly as $ff)
	            	<option data-codigo="{{$ff->codigo}}" data-aerolinea="{{$ff->aerolinea->codigo_iata}}" data-nombre="{{$ff->nombre_registrado}}" data-apellido="{{$ff->apellido_registrado}}" >{{$ff->codigo}} {{$ff->aerolinea->codigo_iata}}</option>
	            @endforeach
	        </select>
	    </div>
	    <div class="col-sm-1">
	        <input type="button" value="+" class="btn-select-pax{{$id}}" data-id="{{$id}}" data-index="{{$key}}" />
	        {{Form::hidden('', $pasajero->id, array('id' => 'id' . $id . '-' . $key))}}
	        {{Form::hidden('', $pasajero->nombre, array('id' => 'nombre' . $id . '-' . $key))}}
	        {{Form::hidden('', $pasajero->primer_apellido, array('id' => 'apellido' . $id . '-' . $key))}}
	        {{Form::hidden('', $pasajero->nit, array('id' => 'nit' . $id . '-' . $key))}}
	        {{Form::hidden('', date('d/m/Y', strtotime($pasajero->fecha_nacimiento)), array('id' => 'nacimiento' . $id . '-' . $key))}}
	        {{Form::hidden('', $pasajero->sexo, array('id' => 'sexo' . $id . '-' . $key))}}
	        {{Form::hidden('', explode("\n", $pasajero->mails)[0], array('id' => 'mail' . $id . '-' . $key))}}
	    </div>
	</div>
	<div class="clear"></div>

@endforeach

<!--Separador-->
{{$id}}