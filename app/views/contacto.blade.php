@extends('layouts.master')

@section('content')

    <!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-6">
                        <!-- Start Map -->
                            <div id="map" data-position-latitude="{{Config::get('app.ubicacion')->latitud}}" data-position-longitude="{{Config::get('app.ubicacion')->longitud}}" data-marker-img="http://maps.google.com/mapfiles/kml/paddle/red-circle.png"></div>
                        <!-- End Map -->
                    </div>
                    
                    <div class="col-md-6">
                        
                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Env&iacute;anos un mensaje</span></h4>
                        
                        <!-- Start Contact Form -->
                        <div id="contact-form" class="contatct-form">
                            {{Form::open(array('url' => URL::to('send_mail_contacto'), 'class' => 'contactForm', 'id' => 'form_contacto'))}}
                                <div class="row">
                                    <div class="col-md-6">
                                        {{Form::label('', 'Tu Nombre', array('for' => 'name'))}}
                                        <!--<span class="name-missing">Por favor ingrese su nombre</span> --> 
                                        {{Form::text('name', '', array('id' => 'name', 'size' => 30))}}
                                    </div>
                                    <div class="col-md-6">
                                        {{Form::label('', 'Tu Email', array('for' => 'e-mail'))}}
                                        <!--<span class="email-missing">Por favor Ingrese un email v&aacute;lido</span> -->
                                        {{Form::text('email', '', array('id' => 'email', 'size' => 50))}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{Form::label('', 'Agrega un comentario', array('for' => 'message'))}}
                                        <!--<span class="message-missing">Por favor, escribe algo</span>-->
                                        {{Form::textarea('message', '', array('id' => 'message', 'cols' => 45, 'rows' => 7))}}
                                        {{Form::submit('Enviar', array('class' => 'button'))}}
                                    </div>
                                </div>
                            {{Form::close()}}
                        </div>
                        <div class="col-sm-12 loading hidden">
                    
                            <div class="clear"></div>

                            {{HTML::image('images/cargando.gif', '')}}
                            
                            <div class="clear"></div>

                            <span class="msj-booking">
                                Enviando mensaje.
                            </span>

                            <div class="clear"></div>

                        </div>
                        <div id="msj-danger" class="alert alert-danger hidden">El mensaje no pudo enviarse.</div>
                        <div id="msj-success" class="alert alert-success hidden">Mensaje enviado correctamente.</div>
                        <!-- End Contact Form -->
                        
                    </div>

                    <div class="hidden-separator"></div>
                    <div class="hidden-separator"></div>
                    
                    <div class="col-md-6">
                        
                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Informacion</span></h4>
                        
                        <!-- Info - Icons List -->
                        <ul class="icons-list">
                            <li><i class="fa fa-globe"></i> <strong>Direcci&oacute;n:</strong> {{Config::get('app.direccion')}}</li>
                            <li><i class="fa fa-envelope-o"></i> <strong>Email:</strong> {{Config::get('app.mail')}}</li>
                            <li><i class="fa fa-mobile"></i> <strong>Tel&eacute;fono:</strong> {{Config::get('app.telefonos')}}</li>
                            @if(Config::get('app.whatsapp'))
                                <li><i class="fa fa-mobile"></i> <strong>Whatsapp:</strong> {{Config::get('app.whatsapp')}}</li>
                            @endif
                        </ul>
                    
                    </div>
                    <div class="col-md-6">
                        <!-- Classic Heading -->
                        <h4 class="classic-title"><span>Horarios de Atenci&oacute;n</span></h4>
                        
                        <!-- Info - List -->
                        <ul class="icons-list">
                            @foreach(Config::get('app.atencion') as $key => $horario)
                                <li><i class="fa fa-calendar"></i> <strong>{{$key}}</strong> - {{$horario}}</li>
                            @endforeach
                        </ul>
                        
                    </div>
                    
                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop

@section('code_js')

    {{HTML::script('js/contacto.js?201506181159')}}
    {{HTML::script('https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true')}}

@stop