@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <h4 class="classic-title"><span>Mis Pasajeros</span></h4>

                    <div class="panel-pax">
                        {{Form::open(array('url' => 'mis_pasajeros'))}}
                            <div class="col-sm-10">
                            	{{Form::text('criterio', Input::get('criterio', ''), array('placeholder' => 'Introduzca nombre y/o apellido'))}}
                            </div>
                            <div class="col-sm-2">
                                {{Form::submit('Buscar')}}
                            </div>
                        {{Form::close()}}
                    </div>

                    <div class="hidden-separator"></div>

                    @if(isset($pasajeros))

                    	<div class="header-pnrs hidden-xs">
	                        <div class="col-sm-1">Nro</div>
	                        <div class="col-sm-3">Nombre</div>
	                        <div class="col-sm-2">Edad</div>
	                        <div class="col-sm-2">Reservas</div>
	                        <div class="col-sm-2"></div>
	                        <div class="col-sm-2"></div>
	                    </div>

	                    <div class="pax-separator"></div>

	                    @foreach($pasajeros as $key => $pasajero)

	                    	<div class="item-pax">
		                        <div class="col-sm-1"><strong class="num-seg">{{$key + 1}}.-</strong></div>
		                        <div class="col-sm-3">{{$pasajero->nombre}} {{$pasajero->primer_apellido}} {{$pasajero->segundo_apellido}} </div>
		                        <div class="col-sm-2">{{$pasajero->edad}} a&ntilde;os</div>
		                        <div class="col-sm-2">{{$pasajero->reservas}} reservas</div>
		                        <div class="col-sm-2 col-xs-6">
		                            <a href="{{URL::to('perfil_pasajero/' . $pasajero->id)}}">
		                            	{{Form::submit('Ver')}}
		                            </a>
		                        </div>
		                        <div class="col-sm-2 col-xs-6">
		                            <a href="{{URL::to('editar_pasajero/' . $pasajero->id)}}">
		                            	{{Form::submit('Editar')}}
		                            </a>
		                        </div>
		                    </div>
		                    <div class="clear"></div>

	                    @endforeach
	                    
	                    <div class="hidden-separator"></div>

	                    <!-- Start Pagination -->
		                    <div id="pagination">
		                        <span class="all-pages">P&aacute;g {{$pagina}} de {{$paginas}}</span>
		                        @for($i = 1; $i <= $paginas; $i++)

		                        	@if($i == $pagina)
		                        		<span class="current page-num">{{$i}}</span>
		                        	@else
		                        		<a class="page-num" href="{{URL::to('mis_pasajeros/' . $i)}}">{{$i}}</a>
		                        	@endif

		                        @endfor

		                        @if($pagina < $paginas)
		                        	<a class="next-page" href="{{URL::to('mis_pasajeros/' . ($pagina+1))}}">Sig</a>
		                        @endif
		                    </div>
	                    <!-- End Pagination -->

                    @endif

                    <div class="hidden-separator"></div>

                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop