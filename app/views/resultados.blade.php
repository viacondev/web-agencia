@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">
                    
                    <div class="col-lg-9 col-lg-push-3">
                    
                         <!-- Start Call Action -->
                        <div class="call-action call-action-book call-action-style3 clearfix">
                            
                            @foreach($busqueda->tramos as $tramo)

	                            <div class="col-sm-5 city tright">{{$tramo->origen}}</div>
	                            <div class="col-sm-2 hidden-xs city center">
	                            	<i class="fa fa-chevron-right"></i>
	                            </div>
	                            <div class="col-sm-5 city tleft">{{$tramo->destino}}</div>
	                            <div class="col-sm-12 date center">{{$tramo->fecha}}</div>

	                        @endforeach

	                        <div class="col-sm-12 date center">
	                        	{{implode(' - ', $busqueda->pasajeros)}}
	                        </div>
                            
                        </div>
                        <!-- End Call Action -->

                        <div class="hidden-separator"></div>

                        <div class="results">

                        	
                        </div>

                        <!-- LOADING -->
							<div class="col-sm-12 loading hidden">
					
								<div class="clear"></div>

								{{HTML::image('images/loading_vuelos.gif', '')}}
								
								<div class="clear"></div>

								Un momento por favor ...
								
								<div class="clear"></div>

								<span class="msj-booking">
									Estamos buscando las mejores tarifas.
								</span>

								<div class="clear"></div>

							</div>
						<!-- /LOADING -->

						<!-- ERROR -->
							<div class="col-sm-12 error-panel hidden">
					
								<div class="clear"></div>

								{{HTML::image('images/oops_icon.png', '')}}
								
								<div class="clear"></div>

								<h4>Lo sentimos, por favor intente nuevamente o cont&aacute;ctenos.</h4>

								<div class="clear"></div>

								<div class="col-sm-12 panel-explain err">
									<ul>
										<li>El proceso de reservaci&oacute;n no ha sido completado.</li>
										<li>El tiempo de espera para los resultados se ha excedido.</li>
										<li>Verifique su conexi&oacute;n a Internet.</li>
									</ul>
								</div>

								<div class="clear"></div>

							</div>
						<!-- /ERROR -->

                    </div>

                    <div class="col-lg-3 col-lg-pull-9">

                        <!-- Start Call Action -->
                        <div class="call-action call-action-price call-action-style3 clearfix">
                            
                            <!-- Call Action Text -->
                            <h4 class="primary">
                                <strong>Precios Encontrados</strong>
                            </h4>

                            @if(count($precios_anteriores) > 0)

                                <div class="clear"></div>

								@foreach($precios_anteriores as $key => $vuelo)

									<a class="before-price" href="#" id="{{$key}}">
		                                <span class="before-price-date">{{$vuelo->fechas}}</span>&nbsp;&nbsp;
		                                <span class="before-price-price">{{$monedas[$vuelo->moneda] . ' ' . $vuelo->precio}}</span> 
		                                <span class="before-price-book">hace {{$vuelo->time}}</span>
		                                {{Form::open(array('url' => 'consulta', 'class' => 'consulta_anterior' . $key . ' hidden'))}}
											
                                            @if($vuelo->tipo_busqueda == 3)

                                                @foreach($vuelo->segmentos as $i => $segmento)

                                                    {{Form::hidden('origen_' . $i, $segmento->nombre_origen . '(' . $segmento->origen . ')')}}
                                                    {{Form::hidden('codigo_origen_' . $i, $segmento->origen)}}
                                                    {{Form::hidden('destino_' . $i, $segmento->nombre_destino . '(' . $segmento->destino . ')')}}
                                                    {{Form::hidden('codigo_destino_' . $i, $segmento->destino)}}
                                                    {{Form::hidden('fecha_salida_' . $i, date('d/m/Y', strtotime($segmento->fecha)))}}

                                                @endforeach

                                            @else

                                                {{Form::hidden('origen', $vuelo->segmentos[0]->nombre_origen . '(' . $vuelo->segmentos[0]->origen . ')')}}
                                                {{Form::hidden('codigo_origen', $vuelo->segmentos[0]->origen)}}
                                                {{Form::hidden('destino', $vuelo->segmentos[0]->nombre_destino . '(' . $vuelo->segmentos[0]->destino . ')')}}
                                                {{Form::hidden('codigo_destino', $vuelo->segmentos[0]->destino)}}

                                                {{Form::hidden('fecha_salida', date('d/m/Y', strtotime($vuelo->segmentos[0]->fecha)))}}

                                                @if($vuelo->tipo_busqueda == 2)
                                                    {{Form::hidden('fecha_retorno', date('d/m/Y', strtotime($vuelo->segmentos[1]->fecha)))}}
                                                @endif

                                            @endif

                                            {{Form::hidden('modo_vuelo', $vuelo->tipo_busqueda)}}
                                            {{Form::hidden('adultos', 1)}}
                                            {{Form::hidden('menores', 0)}}
                                            {{Form::hidden('infantes', 0)}}
                                            {{Form::hidden('max_conexiones', 'n')}}
                                            {{Form::hidden('cabina', 'Y')}}
                                            {{Form::hidden('aerolinea', '')}}

                                            {{Form::hidden('consulta', json_encode($vuelo))}}

										{{Form::close()}}
		                            </a>
		                            <div class="clear"></div>

								@endforeach

							@endif

                        </div>
                        <!-- End Call Action -->
                        
                        <div class="hidden-separator"></div>

                        <!-- Classic Heading -->
                        <div class="tabs-section form-booking">
                            
                            <!-- Nav Tabs -->
                            <ul class="nav nav-tabs">
                                <li 
                                	@if(Input::get('modo_vuelo') == 1) 
                                		class="active" 
                                	@endif
                                >
                                	<a href="#tab-1" data-toggle="tab"><i class="fa fa-long-arrow-right"></i></a>
                                </li>
                                <li 
                                	@if(Input::get('modo_vuelo') == 2) 
                                		class="active" 
                                	@endif
                                >
                                	<a href="#tab-2" data-toggle="tab"><i class="fa fa-exchange"></i></a>
                                </li>
                                <li 
                                	@if(Input::get('modo_vuelo') == 3) 
                                		class="active" 
                                	@endif
                                >
                                	<a href="#tab-3" data-toggle="tab"><i class="fa fa-bars"></i></a>
                                </li>
                            </ul>
                            
                            <!-- Tab panels -->
                            <div class="tab-content purple">
                                <!-- Tab Content 1 -->
                                <div class="tab-pane fade 
                                	@if(Input::get('modo_vuelo') == 1) 
                                		in active
                                	@endif
                                " id="tab-1">
                                    
                                    {{Form::open(array('url' => 'consulta', 'class' => 'submit-booking', 'id' => '1'))}}

                                        <div class="col-sm-12">
                                            {{Form::label('', 'Origen')}}
                                            {{Form::text('origen', Input::get('origen'), array('id' => 'origen1', 'autocomplete' => 'off'))}}
				                            {{Form::hidden('codigo_origen', Input::get('codigo_origen'), array('id' => 'codigo_origen1'))}}
                                        </div>
                                        <div class="col-sm-12">
                                            {{Form::label('', 'Destino')}}
                                            {{Form::text('destino', Input::get('destino'), array('id' => 'destino1', 'autocomplete' => 'off'))}}
				                            {{Form::hidden('codigo_destino', Input::get('codigo_destino'), array('id' => 'codigo_destino1'))}}
                                        </div>
                                        <div class="col-sm-12">
                                            {{Form::label('', 'Salida')}}
                                            {{Form::text('fecha_salida', Input::get('fecha_salida'), array('id' => 'fecha_salida1', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                        </div>
                                        
                                        <div class="separator"></div>

                                        <!-- Datos Generales -->

                                            <div class="col-sm-12">
                                                {{Form::label('', 'Adulto(12+)', array('class' => 'mini-label'))}}
                                                {{Form::select('adultos', array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), Input::get('adultos'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Menor(2-12)', array('class' => 'mini-label'))}}
                                                {{Form::select('menores', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), Input::get('menores'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Infante(0-2)', array('class' => 'mini-label'))}}
                                                {{Form::select('infantes', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), Input::get('infantes'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::checkbox('fecha_flexible', '1', false, array('class' => 'hidden fecha_flexible'))}}
                                                <i class="fa fa-square-o check-flex"></i>
                                                <span class="text-flex">Buscar 3 d&iacute;as antes y despu&eacute;s</span>
                                            </div>

                                            <div class="clear"></div>

                                            <div class="col-sm-12">
                                                {{Form::label('', 'Escalas', array('class' => 'mini-label'))}}
                                                {{Form::select('max_conexiones', array('n' => 'Cualquiera', '3' => '3','2' => '2','1' => '1','0' => 'Directo'), Input::get('max_conexiones'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Cabina', array('class' => 'mini-label'))}}
                                                {{Form::select('cabina', array('Y' => 'Turista', 'S' => 'Económica Superior','C' => 'Ejecutiva','F' => 'Primera'), Input::get('cabina'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Aerolinea', array('class' => 'mini-label'))}}
                                                {{Form::select('aerolinea',  array('' => 'Cualquiera', 'AA' => 'American Airlines','AR' => 'Aerolíneas Argentinas','AF' => 'AirFrance','AM'=>'AeroMéxico','AV'=>'Avianca','AZ'=>'Alitalia','A4'=>'Aerocon','CM'=>'CopaAirlines','G3'=>'GrupoGol','H2'=>'Sky','IB'=>'Iberia','KE'=>'KoreanAir','LA'=>'LAN','LH'=>'Lufthansa','PZ'=>'TAM Mercosur','UX'=>'AirEuropa','Z8'=>'Amaszonas'), Input::get('aerolinea'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                <br/>
                                                {{Form::hidden('modo_vuelo', 1)}}
                                                {{Form::submit('Buscar')}}
                                            </div>

                                        <!-- /Datos Generales -->

                                    {{Form::close()}}

                                    <div class="clear"></div>

                                </div>
                                <!-- Tab Content 2 -->
                                <div class="tab-pane fade
                                	@if(Input::get('modo_vuelo') == 2) 
                                		in active
                                	@endif
                                " id="tab-2">

                                    {{Form::open(array('url' => 'consulta', 'class' => 'submit-booking', 'id' => '2'))}}
                                    
                                        <div class="col-sm-12">
                                            {{Form::label('', 'Origen')}}
                                            {{Form::text('origen', Input::get('origen'), array('id' => 'origen2', 'autocomplete' => 'off'))}}
				                            {{Form::hidden('codigo_origen', Input::get('codigo_origen'), array('id' => 'codigo_origen2'))}}
                                        </div>
                                        <div class="col-sm-12">
                                            {{Form::label('', 'Destino')}}
                                            {{Form::text('destino', Input::get('destino'), array('id' => 'destino2', 'autocomplete' => 'off'))}}
				                            {{Form::hidden('codigo_destino', Input::get('codigo_destino'), array('id' => 'codigo_destino2'))}}
                                        </div>
                                        <div class="col-sm-6">
                                            {{Form::label('', 'Salida')}}
                                            {{Form::text('fecha_salida', Input::get('fecha_salida'), array('id' => 'fecha_salida2', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                        </div>
                                        <div class="col-sm-6">
                                            {{Form::label('', 'Retorno')}}
                                            {{Form::text('fecha_retorno', Input::get('fecha_retorno'), array('id' => 'fecha_retorno', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                        </div>
                                        
                                        <div class="separator"></div>

                                        <!-- Datos Generales -->

                                            <div class="col-sm-12">
                                                {{Form::label('', 'Adulto(12+)', array('class' => 'mini-label'))}}
                                                {{Form::select('adultos', array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9'), Input::get('adultos'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Menor(2-12)', array('class' => 'mini-label'))}}
                                                {{Form::select('menores', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), Input::get('menores'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Infante(0-2)', array('class' => 'mini-label'))}}
                                                {{Form::select('infantes', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), Input::get('infantes'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::checkbox('fecha_flexible', '1', false, array('class' => 'hidden fecha_flexible'))}}
                                                <i class="fa fa-square-o check-flex"></i>
                                                <span class="text-flex">Buscar 3 d&iacute;as antes y despu&eacute;s</span>
                                            </div>

                                            <div class="clear"></div>

                                            <div class="col-sm-12">
                                                {{Form::label('', 'Escalas', array('class' => 'mini-label'))}}
                                                {{Form::select('max_conexiones', array('n' => 'Cualquiera', '3' => '3','2' => '2','1' => '1','0' => 'Directo'), Input::get('max_conexiones'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Cabina', array('class' => 'mini-label'))}}
                                                {{Form::select('cabina', array('Y' => 'Turista', 'S' => 'Económica Superior','C' => 'Ejecutiva','F' => 'Primera'), Input::get('cabina'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Aerolinea', array('class' => 'mini-label'))}}
                                                {{Form::select('aerolinea',  array('' => 'Cualquiera', 'AA' => 'American Airlines','AR' => 'Aerolíneas Argentinas','AF' => 'AirFrance','AM'=>'AeroMéxico','AV'=>'Avianca','AZ'=>'Alitalia','A4'=>'Aerocon','CM'=>'CopaAirlines','G3'=>'GrupoGol','H2'=>'Sky','IB'=>'Iberia','KE'=>'KoreanAir','LA'=>'LAN','LH'=>'Lufthansa','PZ'=>'TAM Mercosur','UX'=>'AirEuropa','Z8'=>'Amaszonas'), Input::get('aerolinea'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                <br/>
                                                {{Form::hidden('modo_vuelo', 2)}}
                                                {{Form::submit('Buscar')}}
                                            </div>

                                        <!-- /Datos Generales -->

                                    {{Form::close()}}

                                    <div class="clear"></div>

                                </div>
                                <!-- Tab Content 3 -->
                                <div class="tab-pane fade
                                	@if(Input::get('modo_vuelo') == 3) 
                                		in active
                                	@endif
                                " id="tab-3">

                                    {{Form::open(array('url' => 'consulta', 'class' => 'submit-booking', 'id' => '3'))}}

                                        <div id="multidestino-container">
                                            <div id="fly-1">
                                                <div class="col-sm-12">
                                                    <br/>
                                                    {{Form::label('', 'Vuelo 1:', array('class' => 'title-mutidestino'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Origen')}}
                                                    {{Form::text('origen_1', Input::get('origen_1'), array('id' => 'origen_1', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_origen_1', Input::get('codigo_origen_1'), array('id' => 'codigo_origen_1'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Destino')}}
                                                    {{Form::text('destino_1', Input::get('destino_1'), array('id' => 'destino_1', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_destino_1', Input::get('codigo_destino_1'), array('id' => 'codigo_destino_1'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Salida')}}
                                                    {{Form::text('fecha_salida_1', Input::get('fecha_salida_1'), array('id' => 'fecha_salida_1', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                                </div>
                                            </div>
                                            <div id="fly-2">
                                                <div class="col-sm-12">
                                                    <br/>
                                                    {{Form::label('', 'Vuelo 2:', array('class' => 'title-mutidestino'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Origen')}}
                                                    {{Form::text('origen_2', Input::get('origen_2'), array('id' => 'origen_2', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_origen_2', Input::get('codigo_origen_2'), array('id' => 'codigo_origen_2'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Destino')}}
                                                    {{Form::text('destino_2', Input::get('destino_2'), array('id' => 'destino_2', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_destino_2', Input::get('codigo_destino_2'), array('id' => 'codigo_destino_2'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Salida')}}
                                                    {{Form::text('fecha_salida_2', Input::get('fecha_salida_2'), array('id' => 'fecha_salida_2', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                                </div>
                                            </div>

                                            @for($i = 3; Input::has('origen_' . $i); $i++)

                                            	<div id="fly-{{$i}}">
	                                                <div class="col-sm-12">
	                                                    <br/>
	                                                    {{Form::label('', 'Vuelo ' . $i . ':', array('class' => 'title-mutidestino'))}}
	                                                </div>
	                                                <div class="col-sm-12">
	                                                    {{Form::label('', 'Origen')}}
	                                                    {{Form::text('origen_' . $i, Input::get('origen_' . $i), array('id' => 'origen_' . $i, 'autocomplete' => 'off'))}}
	                                                    {{Form::hidden('codigo_origen_' . $i, Input::get('codigo_origen_' . $i), array('id' => 'codigo_origen_' . $i))}}
	                                                </div>
	                                                <div class="col-sm-12">
	                                                    {{Form::label('', 'Destino')}}
	                                                    {{Form::text('destino_' . $i, Input::get('destino_' . $i), array('id' => 'destino_' . $i, 'autocomplete' => 'off'))}}
	                                                    {{Form::hidden('codigo_destino_' . $i, Input::get('codigo_destino_' . $i), array('id' => 'codigo_destino_' . $i))}}
	                                                </div>
	                                                <div class="col-sm-12">
	                                                    {{Form::label('', 'Salida')}}
	                                                    {{Form::text('fecha_salida_' . $i, Input::get('fecha_salida_' . $i), array('id' => 'fecha_salida_' . $i, 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
	                                                </div>
	                                            </div>

                                            @endfor

                                        </div>
                                        
                                        <div id="multidestino-template" class="hidden">
                                            <div id="fly-_x_">
                                                <div class="col-sm-12">
                                                    <br/>
                                                    {{Form::label('', 'Vuelo _x_:', array('class' => 'title-mutidestino'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Origen')}}
                                                    {{Form::text('origen__x_', '', array('id' => 'origen__x_', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_origen__x_', '', array('id' => 'codigo_origen__x_'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Destino')}}
                                                    {{Form::text('destino__x_', '', array('id' => 'destino__x_', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_destino__x_', '', array('id' => 'codigo_destino__x_'))}}
                                                </div>
                                                <div class="col-sm-12">
                                                    {{Form::label('', 'Salida')}}
                                                    {{Form::text('fecha_salida__x_', '', array('id' => 'fecha_salida__x_', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            {{Form::hidden('cant_vuelos', Input::get('cant_vuelos', 0), array('id' => 'vuelos'))}}
                                            <a id="delete-fly">- Quitar Vuelo</a>
                                            <a id="add-fly">+ Agregar Vuelo</a>
                                        </div>
                                        
                                        <div class="separator"></div>

                                        <!-- Datos Generales -->

                                            <div class="col-sm-12">
                                                {{Form::label('', 'Adulto(12+)', array('class' => 'mini-label'))}}
                                                {{Form::select('adultos', array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), Input::get('adultos'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Menor(2-12)', array('class' => 'mini-label'))}}
                                                {{Form::select('menores', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), Input::get('menores'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Infante(0-2)', array('class' => 'mini-label'))}}
                                                {{Form::select('infantes', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), Input::get('infantes'))}}
                                            </div>

                                            <div class="clear"></div>

                                            <div class="col-sm-12">
                                                {{Form::label('', 'Escalas', array('class' => 'mini-label'))}}
                                                {{Form::select('max_conexiones', array('n' => 'Cualquiera', '3' => '3','2' => '2','1' => '1','0' => 'Directo'), Input::get('max_conexiones'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Cabina', array('class' => 'mini-label'))}}
                                                {{Form::select('cabina', array('Y' => 'Turista', 'S' => 'Económica Superior','C' => 'Ejecutiva','F' => 'Primera'), Input::get('cabina'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                {{Form::label('', 'Aerolinea', array('class' => 'mini-label'))}}
                                                {{Form::select('aerolinea',  array('' => 'Cualquiera', 'AA' => 'American Airlines','AR' => 'Aerolíneas Argentinas','AF' => 'AirFrance','AM'=>'AeroMéxico','AV'=>'Avianca','AZ'=>'Alitalia','A4'=>'Aerocon','CM'=>'CopaAirlines','G3'=>'GrupoGol','H2'=>'Sky','IB'=>'Iberia','KE'=>'KoreanAir','LA'=>'LAN','LH'=>'Lufthansa','PZ'=>'TAM Mercosur','UX'=>'AirEuropa','Z8'=>'Amaszonas'), Input::get('aerolinea'))}}
                                            </div>
                                            <div class="col-sm-12">
                                                <br/>
                                                {{Form::hidden('modo_vuelo', 3)}}
                                                {{Form::submit('Buscar')}}
                                            </div>

                                        <!-- /Datos Generales -->

                                    {{Form::close()}}

                                    <div class="clear"></div>

                                </div>
                            </div>
                            <!-- End Tab Panels -->
                            
                        </div>

                        <div class="hidden-separator"></div>
                        
                    </div>
                    
                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop

@section('code_js')

	{{HTML::script('http://viacontours.com/air-ws/public/js/aerolineas_aeropuertos.js?201508101225')}}
    {{HTML::script('js/busqueda.js?201510211536')}}
    {{HTML::script('js/resultados.js?201601051008')}}

@stop