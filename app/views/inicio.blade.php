@extends('layouts.master')

@section('content')

	<!-- MAIN SLIDER ================================================== -->
	<div class="mainSliderContainer">
		<ul id="mainSlider">
            <li><img src="images/Sin titulo-1.jpg" alt="" /></li>
            <li><img src="images/Sin titulo-2.jpg" alt="" /></li>
            <li><img src="images/Sin titulo-3.jpg" alt="" /></li>
            <li><img src="images/Sin titulo-4.jpg" alt="" /></li>
		</ul><!-- /mainSlider -->
		<div class="mainSliderNav"></div>
		
		<!-- BOOKING TABS, SHORT OFFERS and FORMS ================================================== -->
		<div class="bookingTabsContainer">
			<div class="container">
				<div class="row">
                    <!-- BOOKING TABS -->
					<div class="col-lg-6 col-md-7 col-sm-12 col-xs-12 bookingTabsCol">
						<div id="bookingTabs">          
							<ul class="resp-tabs-list clearfix">
								<li class="flights"><span>VUELOS</span></li>
							</ul> 

							<div class="resp-tabs-container">                                                        
								<!-- AEREO ==================== -->
                                <div>
                                    <form method="get" action="../online/interfaces/buscar_pasajes.php" id="" class="generalForm bookingForm" onsubmit="return ValidarBusquedaPasajes();"/>
                                        <!--OPCION DE BUSQUEDA-->
                                        <div id="tipo_vuelo_containter">
                                            <input type="hidden" name="tipo_vuelo" id="tipo_vuelo" value="ida_vuelta" />
                                            <span class="col-sm-3 checkFilter"><div><input type="radio" name="rb_modo_vuelo" value="0" class="customCheck" onclick="seleccionar_tipo_vuelo('solo_ida');"/>Solo Ida</div></span>
                                            <span class="col-sm-3 checkFilter"><div><input type="radio" name="rb_modo_vuelo" value="1" class="customCheck" onclick="seleccionar_tipo_vuelo('ida_vuelta');" checked="checked"/>Ida - Vuelta</div></span>
                                            <span class="col-sm-5 checkFilter"><div><input type="radio" name="rb_modo_vuelo" value="2" class="customCheck" onclick="seleccionar_tipo_vuelo('multidestino');"/>Multi - Destino</div></span>
                                        </div>
                                        <!-- SOLO IDA, IDA Y VUELTA -->
                                        <div id="vuelo_dos_rutas_container">
            								<div class="row">
                                        		<div class="col-sm-7">
                									<label>Origen <span>*</span></label>
                    								<input type="text" name="origen" id="origen" autocomplete="off" />
                                            		<input type="hidden" name="codigo_origen" id="codigo_origen" />
            									</div>
                                                <div class="col-sm-5 datePickContainer">
                                                	<label>Sale el <span>*</span></label>
                                                    <input type="text" name="fecha_salida" id="fecha_salida" placeholder="dd/mm/aaaa" autocomplete="off"/>
                                                    <img class="datePickImg" alt="date picker" src="images/calendar.png" />
                                                    <select name="hora_salida" id="hora_salida" style="display:none">
                                                        <option value="0000">Cualquiera</option>
                                                        <option value="0630">Ma&ntilde;ana</option>
                                                        <option value="1200">Mediod&iacute;a</option>
                                                        <option value="1700">Tarde</option>
                                                        <option value="2000">Noche</option>
                                                        <option value="0100">01:00</option>
                                                        <option value="0200">02:00</option>
                                                        <option value="0300">03:00</option>
                                                        <option value="0400">04:00</option>
                                                        <option value="0500">05:00</option>
                                                        <option value="0600">06:00</option>
                                                        <option value="0700">07:00</option>
                                                        <option value="0800">08:00</option>
                                                        <option value="0900">09:00</option>
                                                        <option value="1000">10:00</option>
                                                        <option value="1100">11:00</option>
                                                        <option value="1200">12:00</option>
                                                        <option value="1300">13:00</option>
                                                        <option value="1400">14:00</option>
                                                        <option value="1500">15:00</option>
                                                        <option value="1600">16:00</option>
                                                        <option value="1700">17:00</option>
                                                        <option value="1800">18:00</option>
                                                        <option value="1900">19:00</option>
                                                        <option value="2000">20:00</option>
                                                        <option value="2100">21:00</option>
                                                        <option value="2200">22:00</option>
                                                        <option value="2300">23:00</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-7">
                                                    <label>Destino <span>*</span></label>
                                                    <input type="text" name="destino" id="destino" autocomplete="off" />
                                                    <input type="hidden" name="codigo_destino" id="codigo_destino" />
												</div>
                                                <div class="col-sm-5" id="fecha_retorno_container">
                                                    <label>Vuelve el <span>*</span></label>
                                                    <input type="text" name="fecha_retorno" id="fecha_retorno" placeholder="dd/mm/aaaa" autocomplete="off"/>
                                                    <img class="datePickImg" alt="date picker" src="images/calendar.png" />
                                                    <select name="hora_retorno" id="hora_retorno" style="display:none;">
                                                        <option value="0000">Cualquiera</option>
                                                        <option value="0630">Ma&ntilde;ana</option>
                                                        <option value="1200">Mediod&iacute;a</option>
                                                        <option value="1700">Tarde</option>
                                                        <option value="2000">Noche</option>
                                                        <option value="0100">01:00</option>
                                                        <option value="0200">02:00</option>
                                                        <option value="0300">03:00</option>
                                                        <option value="0400">04:00</option>
                                                        <option value="0500">05:00</option>
                                                        <option value="0600">06:00</option>
                                                        <option value="0700">07:00</option>
                                                        <option value="0800">08:00</option>
                                                        <option value="0900">09:00</option>
                                                        <option value="1000">10:00</option>
                                                        <option value="1100">11:00</option>
                                                        <option value="1200">12:00</option>
                                                        <option value="1300">13:00</option>
                                                        <option value="1400">14:00</option>
                                                        <option value="1500">15:00</option>
                                                        <option value="1600">16:00</option>
                                                        <option value="1700">17:00</option>
                                                        <option value="1800">18:00</option>
                                                        <option value="1900">19:00</option>
                                                        <option value="2000">20:00</option>
                                                        <option value="2100">21:00</option>
                                                        <option value="2200">22:00</option>
                                                        <option value="2300">23:00</option>
                                                    </select>
												</div>
        									</div>
										</div>                         
                                        <!-- MULTIDESTINO -->               
                                        <div id="vuelo_multidestino_container">
                                            <input type="hidden" name="cantidad_multidestinos" id="cantidad_multidestinos" value="1" />
                                        	<div id="vuelto_multidestino__1">
                                                <h4>Vuelo 1</h4>
                                                <div class="col-sm-7 multidest">
                									<label>Origen <span>*</span></label>
                    								<input type="text" name="origen__1" id="origen__1" autocomplete="off"/>
                                            		<input type="hidden" name="codigo_origen__1" id="codigo_origen__1" />
            									</div>
                                                <div class="col-sm-5 datePickContainer multidest">
                                                	<label>Sale el <span>*</span></label>
                                                    <input type="text" name="fecha_salida__1" id="fecha_salida__1" placeholder="dd/mm/aaaa" autocomplete="off"/>
                                                    <img class="datePickImg" alt="date picker" src="images/calendar.png" />
                                                    <select name="hora_salida__1" id="hora_salida__1" style="display:none;">
                                                        <option value="0000">Cualquiera</option>
                                                        <option value="0630">Ma&ntilde;ana</option>
                                                        <option value="1200">Mediod&iacute;a</option>
                                                        <option value="1700">Tarde</option>
                                                        <option value="2000">Noche</option>
                                                        <option value="0100">01:00</option>
                                                        <option value="0200">02:00</option>
                                                        <option value="0300">03:00</option>
                                                        <option value="0400">04:00</option>
                                                        <option value="0500">05:00</option>
                                                        <option value="0600">06:00</option>
                                                        <option value="0700">07:00</option>
                                                        <option value="0800">08:00</option>
                                                        <option value="0900">09:00</option>
                                                        <option value="1000">10:00</option>
                                                        <option value="1100">11:00</option>
                                                        <option value="1200">12:00</option>
                                                        <option value="1300">13:00</option>
                                                        <option value="1400">14:00</option>
                                                        <option value="1500">15:00</option>
                                                        <option value="1600">16:00</option>
                                                        <option value="1700">17:00</option>
                                                        <option value="1800">18:00</option>
                                                        <option value="1900">19:00</option>
                                                        <option value="2000">20:00</option>
                                                        <option value="2100">21:00</option>
                                                        <option value="2200">22:00</option>
                                                        <option value="2300">23:00</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-7 multidest">
                                                    <label>Destino <span>*</span></label>
                                                    <input type="text" name="destino__1" id="destino__1" autocomplete="off" />
                                                    <input type="hidden" name="codigo_destino__1" id="codigo_destino__1" />
												</div>
                                                <div class="clearfix">&nbsp;</div>
                                            </div>
										</div>    
                                        <div id="agregar_multidestino" class="col-sm-14" onclick="agregar_multidestino()" style="cursor:pointer; float:right;">Agregar otro vuelo</div>
                                        <!-- CANTIDAD DE PASAJEROS -->
                                        <div class="col-sm-7">
											<div class="col-sm-4 pRel">
												<label>Adulto <span>*</span></label>
												<select name="adultos" class="customSelectInput required">
													<option value="0">0</option>
                                                    <option value="1" selected="selected">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
											</div>
											<div class="col-sm-4 pRel">
                                                <label>Menor <span>*</span></label>
                                                <select name="menores" class="customSelectInput required">
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
											<div class="col-sm-4 pRel">
                                                <label>Infante <span>*</span></label>
                                                <select name="infantes" class="customSelectInput required">
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
										</div>
                                        <div class="col-sm-5">
                                            <input type="submit" name="buscar_boletos" value="Buscar" />
                                        </div>
                                        <div>
                                            <div id="mensaje" class="messageError"></div>
                                        </div>
                                        <!-- CONFIGURACION ADICIONAL -->
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Max Escalas</label>
                                                <select name="max_conexiones">
                                                    <option value="n" selected="selected">Cualquiera</option>
                                                    <option value="3">3</option>
                                                    <option value="2">2</option>
                                                    <option value="1">1</option>
                                                    <option value="0">Vuelos Directos</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Cabina</label>
                                                <select name="cabina">
                                                    <option value="Y" selected="selected">Turista</option>
                                                    <option value="S">Econ&oacute;mica Superior</option>
                                                    <option value="C">Ejecutiva</option>
                                                    <option value="F">Primera</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Lineas Aereas</label>
                                                <input type="text" id="lineas_aereas" class="lineas_preferidas" name="lineas_aereas" placeholder="(opcional)" />
                                            </div>
										</div>  
                                    </form>                                    
								</div><!-- /flights -->
							</div>
						</div><!-- /bookingTabs -->
					</div><!-- /col -->
                    <!-- SHORT OFFERS -->
                    <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12 promoPanel">
                        <div class="promoItemNone">
                            &nbsp;
                        </div>
                    </div><!-- /col -->
				</div><!-- /row -->
			</div><!-- /container -->
		</div><!-- /bookingTabsContainer -->
	</div><!-- /mainSliderContainer -->

	{{ Aereo::buscarVuelos(); }}
	
@stop