@if(isset($sin_espacio) || Session::has('no_hay_espacio'))
    <!-- SIN ESPACIO -->
        <div class="sin-espacio">
            Lo sentimos, ya no existen espacios disponibles para el itinerario seleccionado. 
            @if(property_exists(Session::get('resultado'), 'opciones'))
                @if(count(Session::get('resultado')->opciones) > 0)
                    &iexcl; Pero tenemos m&aacute;s opciones !
                @endif
            @endif
        </div>
        <div class="hidden-separator"></div>
    <!-- /SIN ESPACIO -->
@endif

<!-- ERROR INTERNO -->
    @if(property_exists(Session::get('resultado'), 'error'))

        <!-- ERROR -->
            <div class="col-sm-12 error-panel">
    
                <div class="clear"></div>

                {{HTML::image('images/oops_icon.png', '')}}
                
                <div class="clear"></div>

                <h4>Lo sentimos, por favor intente nuevamente o cont&aacute;ctenos.</h4>

                <div class="clear"></div>

                <div class="col-sm-12 panel-explain err">
                    <ul>
                        <li>El proceso de reservaci&oacute;n no ha sido completado.</li>
                        <li>El tiempo de espera para los resultados se ha excedido.</li>
                        <li>Verifique su conexi&oacute;n a Internet.</li>
                    </ul>
                </div>

                <div class="clear"></div>

            </div>
        <!-- /ERROR -->

    @endif
<!-- /ERROR INTERNO -->

@if(isset($fecha_flexible))

    <!-- AQUI MOSTRAMOS EL RESULTADO DE LA MATRIZ DEL CALENDAR SHOPPING -->
    <div class=" portfolio-page portfolio-7column">

        <!-- ENCABEZADO SUPERIOR DE LAS FECHAS DE SALIDA -->

            <ul id="list-dates">
                <li class="date-salida">
                    <div>Sale&nbsp;<i class="fa fa-arrow-right"></i></div>

                    @if($consulta->tipo_busqueda == 2)
                        <div class="mini-separator"></div>
                        <div>Llega&nbsp;<i class="fa fa-arrow-down"></i></div>
                    @endif

                </li>

                @foreach($fechas_salida as $j => $salida)

                    <li 
                        @if($j == 3)
                            class="date-selected"
                        @endif    
                    >
                        {{$salida->descripcion->dia}}<br/>{{$salida->descripcion->fecha}}
                    </li>    

                @endforeach

            </ul>

        <!-- /ENCABEZADO SUPERIOR DE LAS FECHAS DE SALIDA -->

        <!-- RESULTADOS -->

            <ul id="portfolio-list" data-animated="fadeIn">

                @define $mas_bajo           = 999999
                @define $mas_bajo_moneda    = ''

                @foreach($fechas_retorno as $i => $retorno)

                    <li class="date-return 
                        @if($i == 3)
                            date-selected
                        @endif
                    ">
                        {{$retorno->descripcion->dia}}<br/>{{$retorno->descripcion->fecha}}
                    </li>

                    @foreach($fechas_salida as $j => $salida)

                        @if(array_key_exists($retorno->time, Session::get('resultado')->opciones))
                            
                            @if(array_key_exists($salida->time, Session::get('resultado')->opciones[$retorno->time]))

                                @define $opcion     = Session::get('resultado')->opciones[$retorno->time][$salida->time];
                                @define $monedas    = array('USD' => 'Usd', 'BOB' => 'Bs')
                                
                                <li 
                                    @if(Session::get('resultado')->precio_mas_bajo == ceil($opcion->precio_final))
                                        class="precio-mas-bajo"
                                    @endif
                                >
                                    <div class="calendar-airline">
                                        <img src="http://viacontours.com/air-ws/public/airline/{{$opcion->aerolinea}}.png" alt="" />
                                    </div>
                                    <div class="calendar-ruta">{{$opcion->ruta}}</div>
                                    <div class="calendar-precio">{{$monedas[$opcion->moneda]}} {{ceil($opcion->precio_final)}}</div>
                                    <div class="calendar-escalas">
                                        @if($opcion->escalas == 0)
                                            Vuelo directo
                                        @else
                                            {{$opcion->escalas}} Escalas
                                        @endif
                                    </div>
                                    <div class="calendar-fechas">
                                        {{$salida->descripcion->fecha}}

                                        @if($consulta->tipo_busqueda == 2)
                                            &nbsp;-&nbsp;{{$retorno->descripcion->fecha}}
                                        @endif
                                    </div>
                                    <div class="portfolio-item-content">
                                        <span class="header">{{$monedas[$opcion->moneda]}} {{ceil($opcion->precio_final)}}</span>
                                    </div>
                                    {{Form::open(array('url' => 'consulta', 'class' => 'hidden form-' . $i . '-' . $j))}}
                                        {{Form::hidden('origen', $consulta->nombre_origen)}}
                                        {{Form::hidden('codigo_origen', $consulta->segmentos[0]->origen)}}
                                        {{Form::hidden('destino', $consulta->nombre_destino)}}
                                        {{Form::hidden('codigo_destino', $consulta->segmentos[0]->destino)}}
                                        {{Form::hidden('fecha_salida', date('d/m/Y', $salida->time))}}
                                        @if($consulta->tipo_busqueda == 2)
                                            {{Form::hidden('fecha_retorno', date('d/m/Y', $retorno->time))}}
                                        @endif
                                        {{Form::hidden('adultos', $consulta->cant_adt)}}
                                        {{Form::hidden('menores', $consulta->cant_cnn)}}
                                        {{Form::hidden('infantes', $consulta->cant_inf)}}
                                        {{Form::hidden('max_conexiones', $consulta->max_conexiones)}}
                                        {{Form::hidden('cabina', $consulta->cabina)}}
                                        {{Form::hidden('aerolinea', $consulta->aerolineas_pref)}}
                                        {{Form::hidden('modo_vuelo', $consulta->tipo_busqueda)}}
                                        @if(property_exists(Session::get('resultado'), 'tarifa_mayorista'))
                                            {{Form::hidden('tarifas_operadora', 1)}}
                                        @endif
                                    {{Form::close()}}
                                    <a href="#" class="link-busqueda" id="{{$i}}-{{$j}}"><i class="more">+</i></a>

                                    @if(ceil($opcion->precio_final) < $mas_bajo)
                                        @define $mas_bajo = ceil($opcion->precio_final)
                                        @define $mas_bajo_moneda = $monedas[$opcion->moneda]
                                    @endif

                                </li>

                            @else

                                <li></li>

                            @endif

                        @else

                            <li></li>

                        @endif

                    @endforeach

                @endforeach

                @if($mas_bajo_moneda != '')
                    {{Form::hidden('', $mas_bajo_moneda . ' ' . $mas_bajo, array('id' => 'mejor-precio'))}}
                @else
                    {{Form::hidden('', ' ', array('id' => 'mejor-precio'))}}
                @endif

            </ul>

        <!-- /RESULTADOS -->

    </div>

@else

    @if(property_exists(Session::get('resultado'), 'opciones'))

        @if(count(Session::get('resultado')->opciones) <= 0)

            <!-- RESULTADO VACIO -->
                <div class="col-sm-12 error-panel">
        
                    <div class="clear"></div>

                    {{HTML::image('images/empty11.png', '')}}
                    
                    <div class="clear"></div>

                    <h4>No se encontraron resultados.</h4>

                    <div class="clear"></div>

                    <div class="col-sm-12 panel-explain err">
                        <ul>
                            <li>
                                Si eligi&oacute; alguna aerol&iacute;nea de preferencia, la cantidad de resultados es menor y existe la probabilidad que no se encuentren resultados.
                            </li>
                            <li>
                                De igual manera si selecciona cantidad de escalas, reduce la probabilidad de encontrar resultados, y m&aacute;s econ&oacute;micos.
                            </li>
                            <li>
                                Tal vez no existan vuelos para la fecha indicada, intente para algunos d&iacute;as antes o despu&eacute;s.
                            </li>
                            <li>
                                Puede verificar si existen resultados anteriores en la secci&oacute;n "Precios Encontrados".
                            </li>
                            <li>
                                Si no encuentra a&uacute;n resultados tomando lo anterior, ll&aacute;menos y lo atenderemos con gusto.
                            </li>
                        </ul>
                    </div>

                    <div class="clear"></div>

                </div>
            <!-- /RESULTADO VACIO -->

        @else

            <!-- RESULTADOS -->

                <!-- Precios por Aerolinea -->

                    <div class="our-clients">
                        <div class="clients-carousel custom-carousel touch-carousel navigation-3" data-appeared-items="5" data-navigation="true">
                            
                            @foreach($resumen_precios as $item_resumen)
                            
                                <div class="client-item item">
                                    <a href="#" class="item-resumen" data-airline="{{$item_resumen->aerolinea}}" data-escalas="{{$item_resumen->escalas}}" >
                                        <img src="http://viacontours.com/air-ws/public/airline/{{$item_resumen->aerolinea}}.png" class="price-airline" alt="" />
                                        <div class="price-airline-amount">{{$item_resumen->moneda}} {{ceil($item_resumen->precio)}}</div>
                                        <div class="price-airline-stops">
                                            @if($item_resumen->escalas > 0)
                                                {{$item_resumen->escalas}} escalas
                                            @else
                                                Vuelo directo
                                            @endif
                                        </div>
                                    </a>
                                </div>
                            
                            @endforeach
                            
                        </div>
                    </div>

                <!-- /Precios por Aerolinea -->

                <div class="hidden-separator"></div>

                <!-- Opciones Encontradas -->

                    @foreach($display as $option)

                        <div class="call-action-air row">
                
                            <!-- Itinerario -->
                                <div class="col-sm-10 air-details">

                                    <!-- Tramos -->

                                        @foreach($option->orig_dest as $key => $tramo)

                                            @if($tramo->index > 1)

                                                <div class="air-separator"></div>

                                            @endif

                                            <div class="row-air">
                                                
                                                <!-- Encabezado Tramo -->

                                                    <div class="col-sm-1">
                                                        <i class="fa 
                                                            @if(array_key_exists($tramo->index - 1 , Session::get('filtros')))
                                                                fa-check-square-o
                                                            @else
                                                                fa-square-o
                                                            @endif
                                                         icon-air-select" id="{{$option->index}}_{{$tramo->index}}"></i>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="tipo-tramo">{{$tramo->tramo->tipo_tramo}}</div>
                                                        <div class="fecha-tramo">{{$tramo->tramo->fecha_salida_literal}}</div>
                                                        <div class="tiempo-tramo">{{$tramo->tramo->t_vuelo_literal}}</div>
                                                    </div>
                                                    <div class="col-sm-3 aepto-tramo">
                                                        <i class="fa fa-arrow-right icon-air-tramo"></i>
                                                        <div class="tramo-city">{{$tramo->tramo->nombre_origen}}({{$tramo->tramo->origen}})</div>
                                                        <div>{{$tramo->tramo->hora_sale}}</div>
                                                    </div>
                                                    <div class="col-sm-3 aepto-tramo">
                                                        <div class="tramo-city">{{$tramo->tramo->nombre_destino}}({{$tramo->tramo->destino}})</div>
                                                        <div>{{$tramo->tramo->hora_llega}}</div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <img src="http://viacontours.com/air-ws/public/airline/{{$tramo->tramo->aerolinea}}.png" class="airline-tramo" alt="" />
                                                        <div class="escala-tramo">
                                                            @if($tramo->tramo->escalas > 1)
                                                                {{$tramo->tramo->escalas - 1}} Escalas
                                                            @else
                                                                Vuelo Directo
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <i class="fa fa-arrow-circle-down icon-air" id="{{$option->index}}_{{$tramo->index}}"></i>
                                                    </div>

                                                <!-- /Encabezado Tramo -->

                                                <div class="clear"></div>

                                                <!-- Detalle del Tramo -->

                                                <div class="row-air-details-container details{{$option->index}}_{{$tramo->index}}">
                                                    
                                                    <div class="details-separator"></div>

                                                    @foreach($tramo->segmentos as $segmento)

                                                        <div class="col-sm-3">
                                                            <i class="fa fa-caret-right icon-segment"></i>
                                                            <div class="segment-city">{{$segmento->nombre_origen}}({{$segmento->origen}})</div>
                                                            <div class="aeropuerto">{{$segmento->nombre_aepto_origen}}</div>
                                                            <div>{{$segmento->hora_salida}}</div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="segment-city">{{$segmento->nombre_destino}}({{$segmento->destino}})</div>
                                                            <div class="aeropuerto">{{$segmento->nombre_aepto_destino}}</div>
                                                            <div>{{$segmento->hora_llegada}} 
                                                                @if($segmento->dias_despues > 0)
                                                                    @if($segmento->dias_despues == 1)
                                                                        <span class="dias-transc">+1d&iacute;a</span>
                                                                    @else
                                                                        <span class="dias-transc">+{{$segmento->dias_despues}}d&iacute;as</span>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4 descr-vuelo">
                                                            <div>{{$segmento->t_vuelo_literal}} - Vuelo {{$segmento->nro_vuelo}}</div>
                                                            <div>{{Session::get('busqueda')->cabina}}({{$segmento->clase_srv}})</div>
                                                            <div>Operado por {{$segmento->nombre_linea_op}}</div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <img src="http://viacontours.com/air-ws/public/airline/{{$segmento->mark_air}}.png" class="airline-segmento" alt="" />
                                                        </div>

                                                        @if(property_exists($segmento, 'paradas'))

                                                            @foreach($segmento->paradas as $parada)

                                                                <div class="clear"></div>
                                                                <div class="col-sm-12 air-stop">
                                                                    Parada en {{$parada->nombre_aepto}}({{$parada->aepto}}) <strong>Llega</strong> {{$parada->hora_llega}} <strong>Sale</strong> {{$parada->hora_sale}}
                                                                </div>

                                                            @endforeach

                                                        @endif
                                                        
                                                        <div class="clear-segment"></div>

                                                    @endforeach

                                                </div>

                                                <!-- /Detalle del Tramo -->

                                            </div>

                                        @endforeach

                                    <!-- /Tramos -->

                                </div>
                            <!-- /Itinerario -->

                            <!-- Precios -->
                                <div class="col-sm-2">
                                    <div class="price-air-option">
                                        @define $moneda = str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $option->costo->moneda)
                                        @define $neto   = $option->costo->monto + $option->costo->fee
                                        @define $ahorro = ceil($neto * (Config::get('app.comparar')['default']/100))
                                        <div class="por-persona" style="font-weight: bold;">Precio {{$moneda}} {{$option->costo->precio_final + $ahorro}}</div>
                                        <div class="por-persona" style="font-size: 7.5pt;color: #f49600;font-weight: bold;">&iexcl; ahorre {{$ahorro}} {{$moneda}} reservando en línea !</div>
                                        <div>
                                            {{$moneda}} {{$option->costo->precio_final}}
                                        </div>
                                        <div class="por-persona">precio total</div>
                                        <!--<div class="price-details price-details{{$option->index}}">-->

                                            <!-- Detalle de los Precios-->

                                                <!--@define $total  = 0
                                                @define $text   = ''

                                                @foreach($option->precios as $precio)

                                                    <div>{{$precio->pax}}: <span class="price-detail-amount">{{$moneda}} {{ceil($precio->total_pax)}}</span></div>

                                                    @define $total  += $precio->subtotal
                                                    @define $text   .= $precio->cant . $precio->pax . ' '

                                                @endforeach-->

                                            <!-- /Detalle de los Precios-->

                                            <!-- Precio total -->
                                            
                                                <!--<div class="price-detail-total">Total {{$text}}</div>
                                                <div class="price-detail-total-amount">{{$moneda}} {{ceil($total)}}</div>-->

                                            <!-- /Precio total -->

                                        <!--</div>-->
                                        @if(property_exists($option, 'es_tarifa_operadora'))

                                            <div class="tarifa-op">Tarifa Operadora</div>

                                        @endif
                                    </div>

                                    @if($option->index == 1)
                                        {{Form::hidden('', $moneda . ' ' . $option->costo->avg_mas_fee, array('id' => 'mejor-precio'))}}
                                    @endif

                                    @if(property_exists($option, 'es_tarifa_operadora'))
                                        {{Form::button('Seleccionar', array('class' => 'btn-select', 'id' => $option->index, 'data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Estas tarifas son validas si se compran mas hotel.'))}}
                                    @else
                                        {{Form::button('Seleccionar', array('class' => 'btn-select', 'id' => $option->index))}}
                                    @endif

                                </div>
                            <!-- /Precios -->
                            
                        </div>

                    @endforeach

                <!-- /Opciones Encontradas -->

                <!-- Paginación -->

                    <div id="pagination">
                        <span class="all-pages">P&aacute;g {{$pagina}} de {{$max_pagina}}</span>

                        @for($i=1; $i<=$max_pagina; $i++)
                            @if($pagina == $i)
                                <span class="current page-num">{{$i}}</span>                           
                            @else
                                <a class="page-num paginacion" href="#" id="{{$i}}" >{{$i}}</a>        
                            @endif
                        @endfor

                        @if($pagina < $max_pagina)
                            <a class="next-page paginacion" href="#" id="{{$pagina+1}}">Sig</a>
                        @endif

                        <span class="current page-num ver-todos">Ver todos</span>
                    </div>
                <!-- /Paginación -->

                <div class="hidden-separator hidden-lg"></div>

            <!-- /RESULTADOS -->

        @endif

    @endif

@endif


