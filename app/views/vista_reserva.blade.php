@extends('layouts.master')

@section('content')

    <!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="big-title text-center">
                        @if(!Session::get('pnr')->emitido)
                            <h1><span class="accent-color">Reserva</span> realizada con <span class="accent-color">&eacute;xito</span>.</h1>
                        @else
                            <h1><span class="accent-color">Boleto</span>&nbsp;<span class="accent-color">Electr&oacute;nico</span>.</h1>
                        @endif
                        <p class="title-desc">El c&oacute;digo es <strong class="code-pnr">{{Session::get('pnr')->codigo_pnr}}</strong>.</p>
                    </div>

                    <div class="hidden-separator"></div>
                    
                    @if(Session::has('show_error'))

                        <div class="alert alert-danger">
                            {{Session::get('show_error')}}
                        </div>

                    @endif

                    @if(Session::has('show_success'))

                        <div class="alert alert-success">
                            {{Session::get('show_success')}}
                        </div>

                    @endif

                    <div class="views">
                        <div class="col-sm-6">
                            <button class="btn-view view-current" id="vista-pantalla">
                                <i class="fa fa-tablet"></i>&nbsp;Vista Web
                            </button>
                        </div>
                        <div class="col-sm-6">
                            <button class="btn-view" id="vista-impresion">
                                <i class="fa fa-file-text"></i>&nbsp;Vista Impresi&oacute;n
                            </button>
                        </div>
                        <!--<div class="col-sm-4">
                            <button class="btn-view" id="vista-detalles">
                                <i class="fa fa-th"></i>&nbsp;Detalles
                            </button>
                        </div>-->
                    </div>

                    <div class="hidden-separator"></div>

                    <div class="vista-pantalla vista-pnr">
                        
                        <div class="col-md-8">
                        
                            <h4 class="classic-title"><span>Itinerario</span></h4>

                            <div class="row latest-posts-classic">

                                @if(count(Session::get('pnr')->itinerario) > 0)
                                
                                    <div class="panel-tramo">

                                        @foreach(Session::get('pnr')->itinerario as $segmento)

                                            <div class="clear-segment"></div>
                                            <div class="col-sm-1 col-xs-2 post-row">
                                                <div class="left-meta-post">
                                                    <div class="post-date">
                                                        <span class="day">{{date('d', strtotime($segmento->fecha_salida))}}</span>
                                                        <span class="month">{{date('M', strtotime($segmento->fecha_salida))}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-10 col-xs-10">
                                                <div class="row">
                                                    <div class="col-sm-3 aepto-tramo">
                                                        <i class="fa fa-arrow-right icon-air-tramo"></i>
                                                        <div class="tramo-city">{{$segmento->nombre_origen}}({{$segmento->origen}})</div>
                                                        <div class="aeropuerto">{{$segmento->nombre_aepto_origen}}</div>
                                                        <div>{{$segmento->hora_salida}}</div>
                                                    </div>
                                                    <div class="col-sm-3 aepto-tramo">
                                                        <div class="tramo-city">{{$segmento->nombre_destino}}({{$segmento->destino}})</div>
                                                        <div class="aeropuerto">{{$segmento->nombre_aepto_destino}}</div>
                                                        <div>{{$segmento->hora_llegada}} 
                                                            @if($segmento->dias_despues > 0)
                                                                @if($segmento->dias_despues == 1)
                                                                    <span class="dias-transc">+1d&iacute;a</span>
                                                                @else
                                                                    <span class="dias-transc">+{{$segmento->dias_despues}}d&iacute;as</span>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 descr-vuelo">
                                                        <div>{{$segmento->t_vuelo_literal}} - Vuelo {{$segmento->nro_vuelo}}</div>
                                                        <div>{{Session::get('pnr')->cabina}}({{$segmento->clase_srv}})</div>
                                                        
                                                        @if(property_exists($segmento, 'linea_op'))
                                                            <div>Operado por {{$segmento->linea_op}}</div>
                                                        @endif

                                                    </div>
                                                    <div class="col-sm-2">
                                                        {{HTML::image('http://viacontours.com/air-ws/public/airline/' . $segmento->mark_air . '.png', '', array('class' => 'price-airline'))}}
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                        <div class="clear"></div>

                                    </div>

                                @else

                                    @if(Session::get('pnr')->emitido)
                                        <div class="col-sm-12">Los vuelos han sido concretados.</div>
                                    @else
                                        <div class="col-sm-12">El itinerario ha sido cancelado.</div>
                                    @endif

                                @endif

                            </div>

                            <div class="hidden-separator"></div>

                                <!-- ASIENTOS VIAJERO FRECUENTE -->
                                    <h4 class="classic-title"><span>Pasajeros y Viajero Frecuente</span></h4>
                                    
                                    @foreach(Session::get('pnr')->pasajeros as $i => $pax)

                                        <div class="col-md-4 col-sm-12">
                                            <strong>{{$i + 1}}. </strong>{{strtoupper($pax->nombre)}} {{strtoupper($pax->apellido)}}
                                        </div>
                                        
                                        <div class="col-md-7 col-sm-12">

                                            @if(property_exists($pax, 'viajero_frecuente'))
                                                @foreach($pax->viajero_frecuente as $ff)

                                                    {{$ff->codigo}} <span>({{$ff->nombre_programa}}) </span><br/>

                                                @endforeach
                                            @else
                                                Viajero frecuente no ingresado
                                            @endif

                                        </div>
                                        <div class="clear"></div>

                                    @endforeach

                                <!-- /ASIENTOS VIAJERO FRECUENTE -->

                                <!-- BOLETOS EMITIDOS -->
                                    @if(Session::get('pnr')->emitido == 1)

                                        <div class="hidden-separator"></div>
                                        <h4 class="classic-title"><span>Boletos Emitidos</span></h4>

                                        @foreach(Session::get('pnr')->pasajeros as $i => $pax)

                                            <div class="col-md-4 col-sm-12">
                                                <strong>{{$i + 1}}. </strong>{{strtoupper($pax->nombre)}} {{strtoupper($pax->apellido)}}
                                            </div>
                                            <div class="col-md-8 col-sm-12">
                                                <div class="row">

                                                    @if(property_exists($pax, 'tickets'))
                                                        @foreach($pax->tickets as $key_ticket => $ticket)
                                                            
                                                            @if($key_ticket % 2 == 0)
                                                                <div class="clear"></div>                    
                                                            @endif

                                                            <div class="col-sm-4">    
                                                                {{$ticket->numero}} <span>({{$ticket->aerolinea}}) </span>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="http://viacontours.com/file_green/{{$ticket->numero}}_ETKT.pdf" target="_blank">
                                                                    <button class="btn-print view-current">Fact</button>
                                                                </a>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="col-md-12">
                                                            A&uacute;n no tiene boletos emitidos
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="clear"></div>

                                        @endforeach

                                    @endif

                                <!-- /BOLETOS EMITIDOS -->

                                <div class="hidden-separator"></div>

                                <!-- IMPORTANTE -->
                                    <h4 class="classic-title"><span>Importante</span></h4>
                                    
                                    <div class="col-sm-12 descr-importante">
                                        {{Config::get('app.empresa')}} agradece su preferencia.
                                    </div>
                                    <div class="col-sm-12 descr-importante">
                                        Para emisi&oacute;n, cont&aacute;ctenos o visite nuestras oficinas.
                                    </div>
                                    <div class="col-sm-12 descr-importante">
                                        Todas las tarifas son garantizadas con la emisi&oacute;n del boleto por lo que se recomienda concretar su compra a la brevedad.
                                    </div>
                                <!-- /IMPORTANTE -->

                        </div>

                        <div class="col-md-4 border-left-price">

                            <div class="mostrar-precios">

                                <h4 class="classic-title"><span>Precios</span></h4>

                                <div class="row prices precios-finales">
                                    
                                    @define $pnrBd = Session::get('pnr')->registro

                                    @if(count($pnrBd->precios) > 0)

                                        <div class="col-xs-12"><h4>Por Persona</h4></div>

                                        @foreach($pnrBd->precios as $precio) 

                                            <div class="col-xs-7">{{$precio->n_tipo_pax_s}} :</div>
                                            <div class="col-xs-5">{{$precio->n_moneda}} {{ceil($precio->fee + $precio->precio)}}</div>

                                        @endforeach

                                        <div class="col-xs-12"><h4>Totales</h4></div>

                                        @foreach($pnrBd->precios as $precio)

                                            <div class="col-xs-7">{{$precio->cantidad_pax}} {{$precio->n_tipo_pax_p}} :</div>
                                            <div class="col-xs-5">{{$precio->n_moneda}} {{ceil($precio->subtotal)}}</div>

                                        @endforeach

                                        <div class="col-xs-12 total-price">
                                            <div>Precio Total</div>

                                            @foreach($pnrBd->totales as $moneda => $total)
                                                <div class="total-amount">{{$total->n_moneda}} {{ceil($total->monto)}}</div>
                                            @endforeach

                                        </div>

                                    @else
                                        <div class="col-xs-12">NO TENEMOS UNA TARIFA PARA MOSTRAR EN ESTE MOMENTO, POR FAVOR CONSULTE A NUESTROS AGENTES DE VIAJES.</div>
                                    @endif

                                </div>

                                <div class="hidden-separator"></div>

                            </div>

                            <h4 class="classic-title"><span>Condiciones de Tarifa</span></h4>
                            
                            @foreach(Session::get('pnr')->condiciones as $key => $condicion)

                                <div class="col-sm-12">     
                                    <ul>
                                        <li class="cond-air">{{$condicion->nombre_carrier}}({{$key}}) INDICA:</li>

                                        @if(property_exists($condicion, 'max_equip'))

                                            <li class="condicion">Maximo de equipaje permitido..</li>

                                            @foreach($condicion->max_equip as $equip)
                                                <li class="equipaje"><strong>{{$equip->equipaje}}</strong> en vuelo {{$equip->n_vuelo}}</li>
                                            @endforeach
                                            
                                        @endif
                                        
                                        @if(property_exists($condicion, 'cond_gral'))
                                            <li class="condicion">{{$condicion->cond_gral}}</li>
                                        @endif
                                           
                                    </ul>
                                </div>

                            @endforeach

                            <div class="hidden-separator"></div>

                            <div class="col-sm-12">     
                                <ul>
                                    <li class="cond-air">
                                        FECHA L&Iacute;MITE DE PAGO Y EMISI&Oacute;N: 
                                    </li>
                                    <li class="equipaje importante negrita">
                                        {{Session::get('pnr')->time_limit_oficina_descripcion}}.
                                    </li>
                                </ul>
                            </div>
                            
                        </div>

                    </div>

                    <div class="vista-impresion vista-pnr hidden">

                        <center id="print-area">
                            <table class="marco-impresion" >
                                <tr class="head-print">
                                    <td class="address">
                                        {{Config::get('app.direccion')}}
                                        <br/>
                                        {{Config::get('app.telefonos')}}
                                        <br/>
                                        {{Config::get('app.mail')}}
                                    </td>
                                    <td class="title-print">
                                        @if(Session::get('pnr')->emitido == 1)
                                            BOLETO ELECTR&Oacute;NICO
                                        @else
                                            RESERVA
                                        @endif
                                    </td>
                                    <td class="img-logo">
                                        {{HTML::image('images/logo.png', '', array('class' => 'img-logo fright'))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="cell-pax">
                                        <strong class="fourteen">PASAJEROS</strong>
                                        <ul class="l-pax">
                                            @if(Session::get('pnr')->emitido == 1)
                                                                
                                                @foreach(Session::get('pnr')->pasajeros as $i => $pax)
                                                    <li>
                                                        <strong>{{$i+1}}.</strong> {{$pax->nombre}} {{$pax->apellido}}&nbsp;&nbsp;&nbsp;
                                                        @if(property_exists($pax, 'tickets'))
                                                            @foreach($pax->tickets as $ticket)
                                                                <strong>TKT</strong>{{$ticket->numero}}({{$ticket->aerolinea}})&nbsp;&nbsp;
                                                            @endforeach
                                                        @endif
                                                    </li>
                                                @endforeach

                                            @else

                                                @foreach(Session::get('pnr')->pasajeros as $i => $pax)
                                                    <li>
                                                        <strong>{{$i+1}}.</strong> {{$pax->nombre}} {{$pax->apellido}}
                                                    </li>
                                                @endforeach

                                            @endif
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <strong class="fourteen">ITINERARIO</strong>

                                        @if(count(Session::get('pnr')->itinerario) > 0)

                                            <table class="tabla_reserva itin">  
                                                <tr>
                                                    <th>NRO<br>SEG</th>
                                                    <th>LINEA AEREA</th>
                                                    <th>VUELO</th>
                                                    <th>FECHA <br>SALIDA</th>
                                                    <th>ORIGEN</th>
                                                    <th>SALE</th>
                                                    <th>DESTINO</th>
                                                    <th>LLEGA</th>
                                                    <th>TIEMPO<br>VUELO</th>
                                                    <th>ESTADO</th>
                                                </tr>

                                                @foreach(Session::get('pnr')->itinerario as $i => $segmento)
                                                    <tr>
                                                        <td>{{$segmento->numero_segmento}}</td>
                                                        <td>{{$segmento->nombre_mark_air}}({{$segmento->mark_air}})</td>
                                                        <td>{{$segmento->nro_vuelo}}</td>
                                                        <td>{{date('d/m/y', strtotime($segmento->fecha_salida))}}</td>
                                                        <td>
                                                            {{$segmento->nombre_origen}}({{$segmento->origen}})
                                                            <br/>
                                                            <span class="dias-transc">{{$segmento->nombre_aepto_origen}}</span>
                                                        </td>
                                                        <td>{{$segmento->hora_salida}}</td>
                                                        <td>
                                                            {{$segmento->nombre_destino}}({{$segmento->destino}})
                                                            <br/>
                                                            <span class="dias-transc">{{$segmento->nombre_aepto_destino}}</span>
                                                        </td>
                                                        <td>{{$segmento->hora_llegada}} 
                                                            @if($segmento->dias_despues > 0)
                                                                @if($segmento->dias_despues == 1)
                                                                    <span class="dias-transc">+1d&iacute;a</span>
                                                                @else
                                                                    <span class="dias-transc">+{{$segmento->dias_despues}}d&iacute;as</span>
                                                                @endif
                                                            @endif
                                                        </td>
                                                        <td>{{$segmento->t_vuelo}}</td>
                                                        <td>{{$segmento->estado}}</td>
                                                    </tr>
                                                @endforeach

                                            </table>  

                                            <table class="tabla_reserva asiento-ff">
                                                <tr>
                                                    <th rowspan="2">PASAJERO</th>
                                                    <th colspan="{{count(Session::get('pnr')->itinerario)}}">ASIENTOS POR SEG</th>
                                                    <th rowspan="2">COD VIAJERO<br/>FRECUENTE</th>
                                                </tr>
                                                <tr>
                                                    @foreach(Session::get('pnr')->itinerario as $i => $seg)
                                                        <th>{{$seg->numero_segmento}}</th>
                                                    @endforeach
                                                </tr>
                                                @foreach(Session::get('pnr')->pasajeros as $i => $pax)
                                                    <tr>
                                                        <td>{{$i+1}}. {{$pax->nombre}} {{$pax->apellido}}</td>
                                                        @foreach(Session::get('pnr')->itinerario as $i => $seg)
                                                            <td class="center">
                                                                @if(property_exists($pax, 'asientos'))
                                                                    @if(array_key_exists($seg->numero_segmento, $pax->asientos))
                                                                        {{$pax->asientos[$seg->numero_segmento]}}
                                                                    @else
                                                                        N/A
                                                                    @endif
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                        @endforeach
                                                        <td>
                                                            @if(property_exists($pax, 'viajero_frecuente'))
                                                                @foreach($pax->viajero_frecuente as $ff)
                                                                    {{$ff->codigo}}({{$ff->nombre_programa}})&nbsp;&nbsp;
                                                                @endforeach
                                                            @else
                                                                Viajero frecuente no ingresado
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>

                                        @else

                                            @if(Session::get('pnr')->emitido)
                                                <br/>&nbsp;&nbsp;LOS VUELOS HAN SIDO CONCRETADOS.<br/>
                                            @else
                                                <br/>&nbsp;&nbsp;EL ITINERARIO HA SIDO CANCELADO.<br/>
                                            @endif

                                        @endif

                                    </td>
                                    <tr class="mostrar-precios">
                                        <td colspan="3">
                                            <strong class="fourteen">PRECIOS</strong>

                                            @define $pnrBd = Session::get('pnr')->registro

                                            @if(count($pnrBd->precios) > 0)

                                                <table class="tabla_reserva t-price precios-finales">
                                                    <tr>
                                                        <th rowspan="2">PASAJEROS</th>
                                                        <th colspan="2">USD</th>
                                                        <th colspan="2">BOB</th>
                                                    </tr>
                                                    <tr>
                                                        <th>PAX</th>
                                                        <th>TOTAL</th>
                                                        <th>PAX</th>
                                                        <th>TOTAL</th>
                                                    </tr>
                                                    @foreach($pnrBd->precios as $precio) 
                                                        @if($precio->moneda == 'USD')
                                                            <tr>
                                                                <td>{{$precio->cantidad_pax}} {{$precio->n_tipo_pax_p}}</td>
                                                                <td class="textright">{{ceil($precio->fee + $precio->precio)}}</td>
                                                                <td class="textright">{{ceil($precio->subtotal)}}</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        @elseif($precio->moneda == 'BOB')
                                                            <tr>
                                                                <td>{{$precio->cant_pax}} {{$precio->n_tipo_pax_p}}</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td class="textright">{{ceil($precio->fee + $precio->precio)}}</td>
                                                                <td class="textright">{{ceil($precio->subtotal)}}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    <tr>
                                                        <td></td>
                                                        <td><strong>TOTAL USD</strong></td>
                                                        <td class="textright">
                                                            <strong>
                                                                @if(property_exists($pnrBd->totales, 'USD'))
                                                                    {{ceil($pnrBd->totales->USD->monto)}}
                                                                @endif
                                                            </strong>
                                                        </td>
                                                        <td><strong>TOTAL BOB</strong></td>
                                                        <td class="textright">
                                                            <strong>
                                                                @if(property_exists($pnrBd->totales, 'BOB'))
                                                                    {{ceil($pnrBd->totales->BOB->monto)}}
                                                                @endif
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </table>

                                            @else

                                                <ul class="equipaje precios-finales">
                                                    <li>NO TENEMOS UNA TARIFA PARA MOSTRAR EN ESTE MOMENTO, POR FAVOR CONSULTE A NUESTROS AGENTES DE VIAJES.</li>
                                                </ul>

                                            @endif

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <strong class="fourteen importante">CONDICIONES DE TARIFA</strong>

                                            @foreach(Session::get('pnr')->condiciones as $key => $condicion)
                                                <ul class="equipaje">

                                                    <li class="cond-air"><strong>{{strtoupper($condicion->nombre_carrier)}} ({{$key}}) INDICA:</strong></li>

                                                    @if(property_exists($condicion, 'max_equip'))
                                                        <li class="condicion">MAXIMO DE EQUIPAJE PERMITIDO..</li>

                                                        @foreach($condicion->max_equip as $equip)
                                                            <li class="equipaje">{{$equip->equipaje}} EN VUELO {{$equip->n_vuelo}}</li>
                                                        @endforeach
                                                    @endif
                                                    
                                                    @if(property_exists($condicion, 'cond_gral'))
                                                        <li class="condicion">{{$condicion->cond_gral}}</li>
                                                    @endif

                                                </ul>
                                            @endforeach

                                            <br/>

                                            <ul class="equipaje">
                                                <li class="cond-air">
                                                    <strong>FECHA L&Iacute;MITE DE PAGO Y EMISI&Oacute;N:</strong>
                                                </li>
                                                <li class="condicion importante negrita">
                                                    {{strtoupper(Session::get('pnr')->time_limit_oficina_descripcion)}}
                                                </li>
                                            </ul>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <strong class="fourteen importante">IMPORTANTE</strong>
                                            <ul class="lista-descripcion equipaje">
                                                <li>{{Config::get('app.empresa')}} AGRADECE SU PREFERENCIA.</li>
                                                <li>PARA EMISI&Oacute;N, CONT&Aacute;CTENOS O VISITE NUESTRAS OFICINAS.</li>
                                                <li>TODAS LAS TARIFAS SON GARANTIZADAS CON LA EMISI&Oacute;N DEL BOLETO POR LO QUE SE RECOMIENDA CONCRETAR SU COMPRA A LA BREVEDAD.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <strong class="fourteen importante">REQUISITOS DE VIAJE</strong>
                                            <ul class="lista-descripcion equipaje">
                                                <li>
                                                    NUESTRO PERSONAL EST&Aacute; DISPUESTO A ASESORARLO SOBRE REQUISITOS PARA EL DESTINO ELEGIDO, O CONSULTE EN L&Iacute;NEA LOS MISMOS EN LA DIRECCI&Oacute;N <a href="https://www.klm.com/travel/ar_es/prepare_for_travel/travel_planning/travel_clinic/visaform.htm" target="_blank">https://www.klm.com/travel/ar_es/prepare_for_travel/travel_planning/travel_clinic/visaform.htm</a>.
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tr>
                            </table>
                        </center>

                    </div>

                    <div class="hidden-separator"></div>

                    <div class="col-md-6">
                    </div>
                    <div class="col-md-3">
                        <button class="btn-print view-current" data-original-title="EnviarMail" data-toggle="modal" data-target=".enviar-mail">
                            <i class="fa fa-envelope-o"></i>&nbsp;Enviar a Mail
                        </button>
                        <br/>
                    </div>
                    <div class="col-md-3">
                        <button class="btn-print view-current link-print-area">
                            <i class="fa fa-print"></i>&nbsp;Imprimir Reserva
                        </button>
                        <br/>
                    </div>

                    <!-- Ventana modal para el enviar por mail -->
                    <div class="modal fade enviar-mail" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-md">
                            <div class="modal-content modal-fee">
                                <!-- Start Contact Form -->
                                    <div id="contact-form" class="contatct-form form-login">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <div class="loader"></div>
                                        
                                        {{Form::open(array('class' => 'form-login form-mail-pnr', 'url' => 'enviar_mail_pnr'))}}
                                            <div class="row">

                                                <div class="col-md-12">
                                                    {{Form::label('', 'Ingrese Dirección de Email')}}
                                                    {{Form::text('email', '', array('id' => 'direccion-email'))}}
                                                </div>

                                                <div class="hidden-separator"></div>

                                                <div class="col-md-12" id="btn-enviar-mail">
                                                    {{Form::submit('Enviar')}}
                                                </div>
                                                <div class="col-md-12 hidden" id="loading-enviar-mail">
                                                    {{HTML::image('images/cargando.gif', '')}}
                                                    <br/>
                                                    Espere por favor ...
                                                </div>

                                                <div class="enviar-mail-sucess ms-sucess hidden">Enviado Correctamente !!</div>

                                                <div class="enviar-mail-error ms-error hidden">Ocurrió un error, intente nuevamente.</div>

                                            </div>
                                        {{Form::close()}}

                                    </div>
                                <!-- End Contact Form -->
                            </div>
                        </div>
                    </div>        
                    <!-- Fin ventana modal -->

                    
                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop

@section('code_js')

    {{HTML::script('js/jquery.progressTimer.min.js')}}
    {{HTML::script('js/vista_reserva.js?201511251743')}}
    {{HTML::script('js/jquery_printarea.js')}}

@stop