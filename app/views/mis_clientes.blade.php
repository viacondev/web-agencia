@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="classic-title"><span>Lista de Clientes</span></h4>

                        {{Form::open(array('url' => 'search_cliente', 'id' => 'form-pax', 'class'=> 'form-horizontal','id'=>'search_client'))}}
                            
                            <div class="panel-pax">
                                <div class="col-sm-9">
                                    {{Form::text('txt_nombre', Input::get('txt_nombre'), array('placeholder'=>'Ingresa Nombre o Apellido'))}}
                                </div>
                                <div class="col-sm-3">
                                    {{Form::submit('Buscar', Input::get('Buscar', ''), array('class' => 'field-required','id'=>'buscar_cliente'))}}
                                </div>
                            </div>

                        {{Form::close()}}

                        <div class="hidden-separator"></div>

                        <div class="pax-separator"></div>
                          
                                <div class="header-pnrs hidden-xs hidden-sm">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-2">N.</div>
                                            <div class="col-md-8">Nombre</div>
                                            <div class="col-md-2">Emis</div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">Mails</div>
                                    <div class="col-md-2">Direcci&oacute;n</div>
                                    <div class="col-md-2">Telefonos</div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="pax-separator"></div>
         
                                 @foreach($clientes as $key => $cliente)
                                 
                                    <div class="item-pax">
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-2"><strong class="num-seg">{{$key+1}}.-</strong></div>
                                                <div class="col-md-8">{{$cliente->nombre}}</div>
                                                <div class="col-md-2">
                                                    @if($cliente->autorizado_emision_aereo == 1)
                                                        Si
                                                    @else
                                                        No
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">{{nl2br($cliente->mails)}}</div>
                                        <div class="col-md-2">{{$cliente->ciudad->nombre}}<br/>{{nl2br($cliente->direccion)}}</div>
                                        <div class="col-md-2">{{nl2br($cliente->telefonos)}}</div>
                                        <div class="col-md-2">
                                            <a href="ver_cliente/{{$cliente->id}}">
                                               <input type="submit" value="Ver" />
                                            </a>
                                            <a href="editar_cliente/{{$cliente->id}}">
                                               <input type="submit" value="Editar" />
                                            </a>
                                        </div>          
                                    </div>
                                    <div class="pax-separator"></div>

                                 @endforeach

                        </div>
                    </div>

            </div>
        </div>
@stop
