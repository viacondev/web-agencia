@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="classic-title"><span>Editar Usuario</span></h4>

                        <div class="panel-pax" style="margin: 1em 10%;width:80%">
                        	{{Form::open(array('url' => 'update_usuario', 'id' => 'form-pax'))}}
                            
                                <div class="col-sm-6">
                                    {{Form::label('', 'Nombre')}}
                                    {{Form::text('nombre_completo', $usuario->nombre_completo, array('class' => 'field-required'))}}
                                </div>

                                <div class="col-sm-6">
                                	{{Form::label('', 'Rol')}}
                                    <select class="field-required" name="rol">
                                    	<option value="0" @if($usuario->rol == 0) selected @endif >Admin de Clientes</option>
                                    	<option value="1" @if($usuario->rol == 1) selected @endif >Admin</option>
                                    	<option value="2" @if($usuario->rol == 2) selected @endif >Normal</option>
                                    </select>
                                </div>
                                <div class="clear"></div>
                                <div class="col-sm-6">
                                    {{Form::label('', 'Telefono')}}
                                    {{Form::textarea('telefono', $usuario->telefonos, array('class' => 'field-required','rows'=>'2'))}}
                                </div>
                                
                                <div class="col-sm-6">
                                    {{Form::label('', 'Mail')}}
                                    {{Form::textarea('mail', $usuario->mails, array('class' => 'field-required','rows'=>'2'))}}
                                </div>

                                
                                <div class="clearfix" ></div>
                                <input type="hidden" name="id" value="<? echo $usuario->id ?>">
                                <input type="hidden" name="idcliente" value="<? echo $usuario->idcliente ?>">
                                
                                <div class="col-sm-4">
                                    {{Form::submit('Actualizar', Input::get('password', ''), array('class' => 'field-required'))}}
                                </div>
                            {{Form::close()}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
@stop
