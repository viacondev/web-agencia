@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="classic-title"><span>Nuevo Pasajero</span></h4>

                        <div class="panel-pax">
                            
                            {{Form::open(array('url' => 'editar_pasajero', 'id' => 'form-pax'))}}
                            
                                <div class="col-sm-4">
                                    {{Form::label('', 'Nombre')}}
                                    {{Form::text('nombre', Input::get('nombre', $pasajero->nombre), array('class' => 'field-required'))}}
                                </div>
                                <div class="col-sm-4">
                                    {{Form::label('', 'Primer Apellido')}}
                                    {{Form::text('primer_apellido', Input::get('primer_apellido', $pasajero->primer_apellido), array('class' => 'field-required'))}}
                                </div>
                                <div class="col-sm-4">
                                    {{Form::label('', 'Segundo Apellido')}}
                                    {{Form::text('segundo_apellido', Input::get('segundo_apellido', $pasajero->segundo_apellido))}}
                                </div>
                                <div class="col-sm-4">
                                    {{Form::label('', 'Nit')}}
                                    {{Form::text('nit', Input::get('nit', $pasajero->nit))}}
                                </div>
                                <div class="col-sm-4">
                                    {{Form::label('', 'Fecha Nacimiento')}}
                                    {{Form::text('fecha_nacimiento', Input::get('fecha_nacimiento', $pasajero->nacimiento), array('class' => 'field-required'))}}
                                </div>
                                <div class="col-sm-4">
                                    {{Form::label('', 'Sexo')}}
                                    {{Form::select('sexo', array('M' => 'Varón', 'F' => 'Mujer'), Input::get('sexo', $pasajero->sexo))}}
                                </div>

                                <div class="hidden-separator"></div>

                                <div class="col-sm-6">

                                    {{Form::label('', 'Documentos')}}

                                    <div class="row">

                                        <div class="col-sm-10">
                                            
                                            <div class="row panel-doc">

                                                @foreach($pasajero->docs as $key => $doc)

                                                    <div class="doc{{$key+1}}">
                                                        <div class="col-sm-4">
                                                            {{Form::text('numero_doc' . ($key+1), $doc->numero, array('placeholder' => 'Número Doc', 'class' => 'field-required'))}}
                                                            {{Form::hidden('iddoc' . ($key+1), $doc->id, array('id' => 'iddoc' . ($key+1)))}}
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <select name="pais_doc{{$key+1}}">
                                                                
                                                                @foreach($paises as $pais)

                                                                    <option value="{{$pais->id}}" 
                                                                        @if($pais->id == $doc->idpais)
                                                                            selected
                                                                        @endif 
                                                                    >
                                                                        {{$pais->nombre}}
                                                                    </option>

                                                                @endforeach
                                                                
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            {{Form::select('tipo_doc' . ($key+1), array('PP' => 'Pasaporte', 'CI' => 'Carnet Identidad', 'DN' => 'DNI'), $doc->tipo_doc)}}
                                                        </div>
                                                    </div>

                                                @endforeach

                                            </div>  

                                            <div class="template-doc hidden">
                                            	<div class="doc_x_">
	                                            	<div class="col-sm-4">
                                                        {{Form::text('numero_doc_x_', '', array('placeholder' => 'Número Doc', 'class' => 'field-required'))}}
	                                                </div>
	                                                <div class="col-sm-4">
	                                                    <select name="pais_doc_x_">
                                                            
                                                            @foreach($paises as $pais)

                                                                <option value="{{$pais->id}}" 
                                                                    @if($pais->codigo_iata == 'BO')
                                                                        selected
                                                                    @endif 
                                                                >
                                                                    {{$pais->nombre}}
                                                                </option>

                                                            @endforeach
	                                                        
	                                                    </select>
	                                                </div>
	                                                <div class="col-sm-4">
                                                        {{Form::select('tipo_doc_x_', array('PP' => 'Pasaporte', 'CI' => 'Carnet Identidad', 'DN' => 'DNI'), 'PP')}}
	                                                </div>
                                                </div>
                                            </div>  

                                        </div>

                                        <div class="col-sm-2">
                                        	{{Form::hidden('cant_doc', count($pasajero->docs), array('id' => 'cant_doc'))}}
                                            {{Form::hidden('docs_deleted', '', array('id' => 'docs_deleted'))}}
		                                    {{Form::button('+', array('class' => 'btn-add add-doc'))}}
		                                    {{Form::button('-', array('class' => 'btn-add del-doc'))}}
                                        </div>

                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    {{Form::label('', 'Teléfonos')}}

                                    <div class="row">

                                        <div class="col-sm-10">

                                            <div class="row panel-telf">

                                                @foreach($pasajero->telfs as $key => $telf)

                                                    <div class="telf{{$key+1}}">
                                                        <div class="col-sm-6">
                                                            {{Form::text('numero_telf' . ($key+1), $telf->numero, array('placeholder' => 'Número', 'class' => 'field-required'))}}
                                                            {{Form::hidden('idtelf' . ($key+1), $telf->id, array('id' => 'idtelf' . ($key+1)))}}
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select name="ciudad_telf{{$key+1}}">

                                                                @foreach($ciudades as $ciudad)

                                                                    <option value="{{$ciudad->id}}" 
                                                                        @if($ciudad->id == $telf->idciudad)
                                                                            selected
                                                                        @endif 
                                                                    >
                                                                        {{$ciudad->nombre}}
                                                                    </option>

                                                                @endforeach
                                                                
                                                            </select>
                                                        </div>
                                                    </div>    

                                                @endforeach

                                            </div>  

                                            <div class="template-telf hidden">
                                                <div class="telf_x_">
                                                    <div class="col-sm-6">
                                                        {{Form::text('numero_telf_x_', '', array('placeholder' => 'Número', 'class' => 'field-required'))}}
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <select name="ciudad_telf_x_">

                                                            @foreach($ciudades as $ciudad)

                                                                <option value="{{$ciudad->id}}" 
                                                                    @if($ciudad->codigo_iata == 'SRZ')
                                                                        selected
                                                                    @endif 
                                                                >
                                                                    {{$ciudad->nombre}}
                                                                </option>

                                                            @endforeach
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-sm-2">
                                            {{Form::hidden('cant_telf', count($pasajero->telfs), array('id' => 'cant_telf'))}}
                                            {{Form::hidden('telfs_deleted', '', array('id' => 'telfs_deleted'))}}
                                            {{Form::button('+', array('class' => 'btn-add add-telf'))}}
                                            {{Form::button('-', array('class' => 'btn-add del-telf'))}}
                                        </div>

                                    </div>
                                    
                                </div>

                                <div class="hidden-separator"></div>

                                <div class="col-sm-6">
                                    {{Form::label('', 'Mails')}}
                                    {{Form::textarea('mails', Input::get('mails', $pasajero->mails), array('rows' => 2))}}
                                </div>
                                <div class="col-sm-6">
                                    {{Form::label('', 'Dirección')}}
                                    {{Form::textarea('direccion', Input::get('direccion', $pasajero->direccion), array('class' => 'field-required', 'rows' => 2))}}
                                </div>

                                <div class="hidden-separator"></div>

                                <div class="col-sm-12">

                                    {{Form::label('', 'Viajero Frecuente')}}

                                    <div class="row">

                                        <div class="col-sm-10">

                                            <div class="row panel-ff">

                                                @foreach($pasajero->ffs as $key => $ff)

                                                    <div class="ff{{$key+1}}">
                                                        <div class="col-sm-3">
                                                            {{Form::text('codigo_ff' . ($key+1), $ff->codigo, array('placeholder' => 'Código', 'class' => 'field-required'))}}
                                                            {{Form::hidden('idff' . ($key+1), $ff->id, array('id' => 'idff' . ($key+1)))}}
                                                        </div>
                                                        <div class="col-sm-3">
                                                            {{Form::text('nombre_ff' . ($key+1), $ff->nombre_registrado, array('placeholder' => 'Nombre de Registro', 'class' => 'field-required'))}}
                                                        </div>
                                                        <div class="col-sm-3">
                                                            {{Form::text('apellido_ff' . ($key+1), $ff->apellido_registrado, array('placeholder' => 'Apellido de Registro', 'class' => 'field-required'))}}
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <select name="aerolinea_ff{{$key+1}}">
                                                                
                                                                @foreach($programas as $programa)

                                                                    <option value="{{$programa->id}}" 
                                                                        @if($programa->id == $ff->idaerolinea)
                                                                            selected
                                                                        @endif
                                                                    >
                                                                        {{$programa->programa_millas}}
                                                                    </option>

                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>

                                                @endforeach

                                            </div>  

                                            <div class="template-ff hidden">
                                                <div class="ff_x_">
                                                    <div class="col-sm-3">
                                                        {{Form::text('codigo_ff_x_', '', array('placeholder' => 'Código', 'class' => 'field-required'))}}
                                                    </div>
                                                    <div class="col-sm-3">
                                                        {{Form::text('nombre_ff_x_', '', array('placeholder' => 'Nombre de Registro', 'class' => 'field-required'))}}
                                                    </div>
                                                    <div class="col-sm-3">
                                                        {{Form::text('apellido_ff_x_', '', array('placeholder' => 'Apellido de Registro', 'class' => 'field-required'))}}
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select name="aerolinea_ff_x_">
                                                            
                                                            @foreach($programas as $programa)

                                                                <option value="{{$programa->id}}">
                                                                    {{$programa->programa_millas}}
                                                                </option>

                                                            @endforeach

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-sm-2">
                                            {{Form::hidden('cant_ff', count($pasajero->ffs), array('id' => 'cant_ff'))}}
                                            {{Form::hidden('ffs_deleted', '', array('id' => 'ffs_deleted'))}}
                                            {{Form::button('+', array('class' => 'btn-add add-ff'))}}
                                            {{Form::button('-', array('class' => 'btn-add del-ff'))}}
                                        </div>

                                    </div>
                                    
                                </div>

                                <div class="hidden-separator"></div>
                                
                                <div class="col-sm-6">
                                    <a href="{{Request::url()}}">
                                        <input type="button" value="Resetear" />
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    {{Form::hidden('id', $pasajero->id)}}
                                    {{Form::submit('Guardar')}}
                                </div>

                            {{Form::close()}}

                        </div>


                        <div class="hidden-separator"></div>

                    </div>

                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop

@section('code_js')

    {{HTML::script('js/crud_pax.js')}}

@stop