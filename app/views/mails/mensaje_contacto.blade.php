<table width="100%" height="100%" style="width:100%; height:100%;">
    <tr>
        <td style="background-color:#F2F2F2;">
            <center>
                <table style="width:700px; border:1px solid #345e8f; background-color:#FFFFFF;">
                    <tr>
                        <td colspan="2" style="background-color:#345e8f; color:#FFFFFF;">
                            <center><h3 style="font-family:Lucida Sans, Sans serif; color:#ffffff; margin:0;">MENSAJE DE SOLICITUD</h3></center>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;">
                            <table>
                                <tr>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;"><strong style="font-size:9pt;">NOMBRE</strong></td>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;"><strong style="font-size:9pt;">:</strong></td>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;text-transform:uppercase;">{{$nombre}}</td>
                                </tr>
                                <tr>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;"><strong style="font-size:9pt;">EMAIL</strong></td>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;"><strong style="font-size:9pt;">:</strong></td>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;text-transform:uppercase;">{{$email}}</td>
                                </tr>
                                <tr>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;"><strong style="font-size:9pt;">MENSAJE</strong></td>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;"><strong style="font-size:9pt;">:</strong></td>
                                    <td style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;text-transform:uppercase;">{{$mensaje}}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:400px;">
                            {{HTML::image('images/logo.png', '', array('style' => 'width:200px;'))}}
                        </td>
                        <td style="width:300px; font-family:Lucida Sans, Sans serif; font-size:7pt; text-transform:uppercase;">
                            {{Config::get('app.direccion')}}<br/>
                            TELEFONO {{Config::get('app.telefonos')}}<br/>
                            PAGINA WEB <a href="http://{{Config::get('app.paginaWeb')}}">{{Config::get('app.paginaWeb')}}</a>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>