<table width="100%" height="100%" style="width:100%; height:100%;">
    <tr>
        <td style="background-color:#F2F2F2;">
            <center>
                <table style="width:700px; border:1px solid #345e8f; background-color:#FFFFFF;">
                    <tr>
                        <td colspan="2" style="background-color:#345e8f; color:#FFFFFF;">
                            <center><h3 style="font-family:Lucida Sans, Sans serif; color:#ffffff; margin:0;">MENSAJE DE CONFIRMACION</h3></center>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-family:Lucida Sans, Sans serif; font-size:12pt; color:#345e8f; padding:10px;">
                            {{$nombre}}...
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-family:Lucida Sans, Sans serif; padding-left:10px; padding-right:10px; font-size:9pt;">
                            GRACIAS POR COMUNICARTE CON NOSOTROS, SI RECIBISTE ESTE MENSAJE SIGNIFICA QUE TU MENSAJE HA SIDO ENVIADO CORRECTAMENTE, Y SERA RESPONDIDO A LA BREVEDAD.<p/>
                            SALUDOS CORDIALES.<p/>
                            &nbsp;&nbsp;&nbsp;&nbsp;<font style="text-transform:uppercase;">{{Config::get('app.empresa')}}</font>
                            <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:400px;">
                            {{HTML::image('images/logo.png', '', array('style' => 'width:200px;'))}}
                        </td>
                        <td style="width:300px; font-family:Lucida Sans, Sans serif; font-size:7pt; text-transform:uppercase;">
                            {{Config::get('app.direccion')}}<br/>
                            TELEFONO {{Config::get('app.telefonos')}}<br/>
                            PAGINA WEB <a href="http://{{Config::get('app.paginaWeb')}}">{{Config::get('app.paginaWeb')}}</a>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>