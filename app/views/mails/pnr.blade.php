<center>
	<strong>El c&oacute;digo de esta reserva es <font style="color:#ff3300;">{{Session::get('pnr')->codigo_pnr}}</font></strong>
	<br/><br/>
	<table style="width: 900px; font-family: Courier New; line-height: 1; background-color: #FFF; border: 2px solid #888; font-size:10pt;" >
		<tr style="border-bottom: 1px solid #888;">
			<td style="width: 300px; vertical-align: top;">
				{{Config::get('app.direccion')}}
			</td>
			<td style="font-size: 16pt; font-weight: bold; vertical-align: top; text-align: center;">
				
				@if(Session::get('pnr')->emitido)
                    BOLETO ELECTR&Oacute;NICO
                @else
                    RESERVA
                @endif

			</td>
			<td style="width: 150px;">
				{{HTML::image('images/logo.png', '', array('style' => 'width: 150px; float:right;'))}}
			</td>
		</tr>
		<tr>
			<td colspan="3" class="cell-pax">
				<hr/>
				<strong style="font-size: 14pt;">PASAJEROS</strong>
				<ul style="list-style: none; margin-top:5px;">
					@foreach(Session::get('pnr')->pasajeros as $i => $pax)
						<li><strong>{{$i+1}}.</strong> {{$pax->nombre}} {{$pax->apellido}}</li>
					@endforeach
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<strong style="font-size: 14pt;">ITINERARIO</strong>

				@if(count(Session::get('pnr')->itinerario) > 0)

					<table style="margin-left: 10px; margin-bottom: 15px; width: 98%; border-collapse: collapse; border-spacing: 0; font-size:10pt; font-family: Courier New; line-height: 1;">  
						<tr>
							<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">NRO<br>SEG</th>
					        <th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">LINEA AEREA</th>
							<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">VUELO</th>
							<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">FECHA <br>SALIDA</th>
							<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">ORIGEN</th>
					        <th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">SALE</th>
							<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">DESTINO</th>
							<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">LLEGA</th>
							<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">TIEMPO<br>VUELO</th>
					        <th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">ESTADO</th>
						</tr>
						@foreach(Session::get('pnr')->itinerario as $i => $segmento)
							<tr>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$i+1}}</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$segmento->nombre_mark_air}}({{$segmento->mark_air}})</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$segmento->nro_vuelo}}</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{date('d/m/y', strtotime($segmento->fecha_salida))}}</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">
									{{$segmento->nombre_origen}}({{$segmento->origen}})
									<br/>
	                                <span class="dias-transc">{{$segmento->nombre_aepto_origen}}</span>
								</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$segmento->hora_salida}}</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">
									{{$segmento->nombre_destino}}({{$segmento->destino}})
									<br/>
	                                <span class="dias-transc">{{$segmento->nombre_aepto_destino}}</span>
								</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">
									{{$segmento->hora_llegada}}
									@if($segmento->dias_despues > 0)
	                                    @if($segmento->dias_despues == 1)
	                                        <span class="dias-transc">+1d&iacute;a</span>
	                                    @else
	                                        <span class="dias-transc">+{{$segmento->dias_despues}}d&iacute;as</span>
	                                    @endif
	                                @endif
								</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$segmento->t_vuelo}}</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$segmento->estado}}</td>
							</tr>
						@endforeach
					</table>  
					<br/>
					<table style="margin-left: 10px; margin-bottom: 15px; width: 98%; border-collapse: collapse; border-spacing: 0; font-size:10pt; width: 80%; font-family: Courier New; line-height: 1;">
						<tr>
							<th rowspan="2" style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">PASAJERO</th>
							<th colspan="{{count(Session::get('pnr')->itinerario)}}" style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">ASIENTOS POR SEG</th>
							<th rowspan="2" style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">COD VIAJERO<br/>FRECUENTE</th>
						</tr>
						<tr>
							@foreach(Session::get('pnr')->itinerario as $i => $seg)
								<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">{{$i+1}}</th>
							@endforeach
						</tr>
						@foreach(Session::get('pnr')->pasajeros as $i => $pax)
							<tr>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$i+1}}. {{$pax->nombre}} {{$pax->apellido}}</td>
								@foreach(Session::get('pnr')->itinerario as $i => $seg)
									<td style="border: 1px solid #888; line-height: 1; padding: 2px; text-align:center;">
										@if(property_exists($pax, 'asientos'))
											@if(array_key_exists($i+1, $pax->asientos))
												{{$pax->asientos[$i+1]}}
											@else
												N/A
											@endif
										@else
											N/A
										@endif
									</td>
								@endforeach
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;">
									@if(property_exists($pax, 'viajero_frecuente'))
										@foreach($pax->viajero_frecuente as $ff)
											{{$ff->codigo}}({{$ff->nombre_programa}})&nbsp;&nbsp;
										@endforeach
									@else
										Viajero frecuente no ingresado
									@endif
								</td>
							</tr>
						@endforeach
					</table>

				@else

					@if(Session::get('pnr')->emitido)
                        <ul style="list-style: disc inside;">
							<li style="line-height:1.2;">LOS VUELOS HAN SIDO CONCRETADOS.</li>
						</ul>
                    @else
                        <ul style="list-style: disc inside;">
							<li style="line-height:1.2;">EL ITINERARIO HA SIDO CANCELADO.</li>
						</ul>
                    @endif

				@endif
				<br/>
			</td>
		</tr>
		@if(Input::has('mostrar_precio'))
			<tr>
				<td colspan="3">
					<strong style="font-size:14pt;">PRECIOS</strong>

					@if(count($pnrBd->precios) > 0)
						
						<table style="margin-left: 10px; margin-bottom: 15px; width: 98%; border-collapse: collapse; border-spacing: 0; font-size:10pt; width: 60%; font-family: Courier New; line-height: 1;">
							<tr>
								<th rowspan="2" style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">PASAJEROS</th>
								<th colspan="2" style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">USD</th>
								<th colspan="2" style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">BOB</th>
							</tr>
							<tr>
								<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">PAX</th>
								<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">TOTAL</th>
								<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">PAX</th>
								<th style="color: #3785dd; font-weight: 900; border: 1px solid #333; text-align: center; line-height: 1; padding: 2px;">TOTAL</th>
							</tr>

							@foreach($pnrBd->precios as $precio) 
								
								@if($precio->moneda == 'USD')
									<tr>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$precio->cantidad_pax}} {{$precio->n_tipo_pax_p}}</td>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px; text-align: right;">{{ceil($precio->fee + $precio->precio)}}</td>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px; text-align: right;">{{ceil($precio->subtotal)}}</td>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px;"></td>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px;"></td>
									</tr>
								@elseif($precio->moneda == 'BOB')
									<tr>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px;">{{$precio->cantidad_pax}} {{$precio->n_tipo_pax_p}}</td>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px;"></td>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px;"></td>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px; text-align: right;">{{ceil($precio->fee + $precio->precio)}}</td>
										<td style="border: 1px solid #888; line-height: 1; padding: 2px; text-align: right;">{{ceil($precio->subtotal)}}</td>
									</tr>
								@endif

							@endforeach

							<tr>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;"></td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;"><strong>TOTAL USD</strong></td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px; text-align: right;">
									<strong>
										@if(property_exists($pnrBd->totales, 'USD'))
                                            {{ceil($pnrBd->totales->USD->monto)}}
                                        @endif
									</strong>
								</td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px;"><strong>TOTAL BOB</strong></td>
								<td style="border: 1px solid #888; line-height: 1; padding: 2px; text-align: right;">
									<strong>
										@if(property_exists($pnrBd->totales, 'BOB'))
                                            {{ceil($pnrBd->totales->BOB->monto)}}
                                        @endif
									</strong>
								</td>
							</tr>
						</table>

					@else

						<ul style="list-style: disc inside;">
							<li style="line-height:1.2;">EN ESTE MOMENTO NO HAY TARIFAS.</li>
						</ul>

					@endif

					<br/>
				</td>
			</tr>
		@endif
		<tr>
			<td colspan="3">
				<strong style="font-size:14pt; color: #FF3300;">CONDICIONES DE TARIFA</strong>

				@foreach(Session::get('pnr')->condiciones as $key => $condicion)
					<ul style="list-style: none;">

						<li style="font-weight:bold; list-style: none; line-height:1.2;"><strong>{{strtoupper($condicion->nombre_carrier)}} ({{$key}}) INDICA:</strong></li>

						@if(property_exists($condicion, 'max_equip'))
							<li style="list-style: disc inside; line-height:1.2;">MAXIMO DE EQUIPAJE PERMITIDO..</li>

							@foreach($condicion->max_equip as $equip)
								<li style="margin-left:20px; list-style: none; line-height:1.2;">{{$equip->equipaje}} EN VUELO {{$equip->n_vuelo}}</li>
							@endforeach
						@endif
						
						@if(property_exists($condicion, 'cond_gral'))
							<li style="list-style: disc inside; line-height:1.2;">{{$condicion->cond_gral}}</li>
						@endif

					</ul>
				@endforeach

					<ul style="list-style: none;">
						<li style="font-weight:bold; list-style: none; line-height:1.2;">
							<strong>FECHA L&Iacute;MITE DE PAGO Y EMISI&Oacute;N:</strong>
						</li>
						<li style="list-style: disc inside; font-weight:bold; color #FF3300; line-height:1.2;">
							{{strtoupper(Session::get('pnr')->time_limit_oficina_descripcion)}}
						</li>
					</ul>

			</td>
		</tr>
		<tr>
			<td colspan="3">
				<strong style="font-size:14pt; color: #FF3300;">IMPORTANTE</strong>
				<ul style="list-style: disc inside;">
					<li style="line-height:1.2;">{{Config::get('app.empresa')}} AGRADECE SU PREFERENCIA.</li>
					<li style="line-height:1.2;">PARA EMISI&Oacute;N, CONT&Aacute;CTENOS O VISITE NUESTRAS OFICINAS.</li>
					<li style="line-height:1.2;">TODAS LAS TARIFAS SON GARANTIZADAS CON LA EMISI&Oacute;N DEL BOLETO POR LO QUE SE RECOMIENDA CONCRETAR SU COMPRA A LA BREVEDAD.</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<strong style="font-size:14pt; color: #FF3300;">REQUISITOS DE VIAJE</strong>
				<ul style="list-style: disc inside;">
					<li style="line-height:1.2;">
						NUESTRO PERSONAL EST&Aacute; DISPUESTO A ASESORARLO SOBRE REQUISITOS PARA EL DESTINO ELEGIDO, O CONSULTE EN L&Iacute;NEA LOS MISMOS EN LA DIRECCI&Oacute;N <a href="https://www.klm.com/travel/ar_es/prepare_for_travel/travel_planning/travel_clinic/visaform.htm" target="_blank">https://www.klm.com/travel/ar_es/prepare_for_travel/travel_planning/travel_clinic/visaform.htm</a>.
					</li>
				</ul>
			</td>
		</tr>
	</table>
</center>
		