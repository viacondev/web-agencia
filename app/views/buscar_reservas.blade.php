@extends('layouts.master')

@section('content')

    <!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">
                    
                    <div class="col-md-12">

                        <h4 class="classic-title"><span>B&uacute;squeda de Reservas</span></h4>

                        @if(Auth::user()->rol == 0)

                            <div class="panel-pax">

                                {{Form::open(array('url' => 'pnr_pasajero'))}}
                                    
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-2">{{Form::label('', 'Apellido')}}</div>
                                            <div class="col-sm-4">{{Form::text('apellido', Input::get('apellido'))}}</div>
                                            <div class="col-sm-2">{{Form::label('', 'Nombre')}}</div>
                                            <div class="col-sm-4">{{Form::text('nombre', Input::get('nombre'))}}</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-2">
                                        {{Form::submit('Buscar')}}
                                    </div>
                                    
                                {{Form::close()}}

                            </div>

                            <div class="hidden-separator"></div>

                            @if(isset($pnrs))

                                <div class="pax-separator"></div>

                                <div class="header-pnrs hidden-xs">
                                    <div class="col-sm-1">Nro</div>
                                    <div class="col-sm-1">C&oacute;digo</div>
                                    <div class="col-sm-3">Ruta</div>
                                    <div class="col-sm-2">Pasajeros</div>
                                    <div class="col-sm-2">Cliente</div>
                                    <div class="col-sm-2">Creado</div>
                                    <div class="col-sm-1"></div>
                                </div>
                                
                                <div class="pax-separator"></div>
                                
                                @foreach($pnrs as $key => $pnr)
                                    
                                    <div class="item-pax">
                                        <div class="col-sm-1"><strong class="num-seg">{{$key+1}}.-</strong></div>
                                        <div class="col-sm-1">{{$pnr->codigo}}</div>
                                        <div class="col-sm-3">{{$pnr->ruta}}</div>
                                        <div class="col-sm-2">
                                            @foreach($pnr->pasajeros as $pasajero)
                                                {{$pasajero->nombre}} {{$pasajero->primer_apellido}}<br/>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-2">
                                            {{$pnr->usuario->cliente->nombre}} ({{$pnr->usuario->nombre}})
                                        </div>
                                        <div class="col-sm-2">
                                            {{date('d/M H:i', strtotime($pnr->fecha))}} 
                                            @if($pnr->estado == 1)
                                                (Reserva)
                                            @elseif($pnr->estado == 2)
                                                (Emitido)
                                            @elseif($pnr->estado == 3)
                                                (Anulado)
                                            @endif
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="pnr_admin/{{$pnr->codigo}}">
                                               <input type="submit" value="Ver" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="pax-separator"></div>

                                @endforeach

                            @endif

                        @else

                            No tiene acceso a esta b&uacute;squeda.

                        @endif

                    </div>

                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop