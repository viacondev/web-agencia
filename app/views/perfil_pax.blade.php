@extends('.layouts.master')

@section('content')

    <!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="classic-title"><span>Perfil de Pasajero</span></h4>

                        <div class="blog-post standard-post">
                            <!-- Post Content -->
                            <div class="post-content">
                                <div class="post-type">
                                    @if($pax->sexo == 'M')
                                        <i class="fa fa-male"></i>
                                    @else
                                        <i class="fa fa-female"></i>
                                    @endif
                                </div>
                                <h2><a href="#">{{$pax->nombre}} {{$pax->primer_apellido}} {{$pax->segundo_apellido}}</a></h2>
                                <ul class="post-meta">
                                    <li>
                                        @if($pax->sexo == 'M')
                                            Var&oacute;n
                                        @else
                                            Mujer
                                        @endif
                                    </li>
                                    <li>Nacido el {{$pax->fecha_nacimiento_descripcion}}</li>
                                    <li>{{$pax->edad}} a&ntilde;os de edad</li>
                                </ul> 

                                <div class="row info-pax">

                                    @if($pax->nit != '')
                                        <div class="col-sm-3"><h4>NIT:</h4></div>
                                        <div class="col-sm-9">{{$pax->nit}}</div>
                                    @endif

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Documentos:</h4></div>

                                    @if(count($pax->docs) > 0)
                                        <div class="col-sm-9">

                                            @foreach($pax->docs as $doc)
                                                {{$doc->numero}} - {{$doc->tipo_documento}} - {{$doc->paisdoc->nombre}}<br/>
                                            @endforeach

                                        </div>
                                    @else
                                        <div class="col-sm-9">No tiene ning&uacute;n documento registrado.</div>
                                    @endif

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Tel&eacute;fonos:</h4></div>

                                    @if(count($pax->telfs) > 0)
                                        <div class="col-sm-9">

                                            @foreach($pax->telfs as $telf)
                                                {{$telf->numero}} - {{$telf->ciudadtel->nombre}}<br/>
                                            @endforeach

                                        </div>
                                    @else
                                        <div class="col-sm-9">No tiene ning&uacute;n tel&eacute;fono registrado.</div>
                                    @endif

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Mails:</h4></div>
                                    <div class="col-sm-9">
                                        
                                        @if($pax->mails != '')
                                            {{nl2br($pax->mails)}}
                                        @else
                                            No tiene correo registrado.
                                        @endif

                                    </div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Direcci&oacute;n:</h4></div>
                                    <div class="col-sm-9">
                                        {{nl2br($pax->direccion)}}                                        
                                    </div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Viajero Frecuente:</h4></div>
                                    <div class="col-sm-9">

                                        @if(count($pax->ffs) > 0)
                                            <div class="col-sm-9">

                                                @foreach($pax->ffs as $ff)
                                                    {{$ff->codigo}} - {{$ff->airff->programa_millas}} ({{$ff->nombre_registrado}} {{$ff->apellido_registrado}})<br/>
                                                @endforeach

                                            </div>
                                        @else
                                            <div class="col-sm-9">No tiene ning&uacute;n c&oacute;digo FrequentFlyer registrado.</div>
                                        @endif
                                        
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="hidden-separator"></div>

                    </div>

                    <div class="col-sm-6"></div>
                    <div class="col-sm-6">
                        {{Form::open(array('url' => 'eliminar_pasajero', 'id' => 'form-delete'))}}
                            {{Form::hidden('id', $pax->id)}}
                            {{Form::submit('Eliminar Pasajero')}}
                        {{Form::close()}}
                    </div>

                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop

@section('code_js')

    {{HTML::script('js/perfil_pax.js')}}

@stop