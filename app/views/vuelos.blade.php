@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">
                    
                    <div class="col-md-7">
                        
                        <!-- Classic Heading -->
                        <!--<h4 class="classic-title"><span>Reservar</span></h4>-->
                        
                        <div class="tabs-section form-booking">
                            
                            <!-- Nav Tabs -->
                            <ul class="nav nav-tabs">
                                <li>
                                	<a href="#tab-1" data-toggle="tab">
                                		<i class="fa fa-long-arrow-right hidden-xs"></i>Ida
                                	</a>
                                </li>
                                <li class="active">
                                	<a href="#tab-2" data-toggle="tab">
                                		<i class="fa fa-exchange hidden-xs"></i>Ida-Vuelta
                                	</a>
                                </li>
                                <li>
                                	<a href="#tab-3" data-toggle="tab">
                                		<i class="fa fa-bars hidden-xs"></i>Multidestino
                                	</a>
                                </li>
                            </ul>
                            
                            <!-- Tab panels -->
                            <div class="tab-content purple">
                                <!-- Tab Content (Solo Ida) -->
                                <div class="tab-pane fade" id="tab-1">
                                    
                                    {{Form::open(array('url' => 'consulta', 'class' => 'submit-booking', 'id' => '1'))}}

                                        <div class="col-sm-4">
                                            {{Form::label('', 'Origen')}}
                                            {{Form::text('origen', 'Santa Cruz de la Sierra, Bolivia (SRZ)', array('id' => 'origen1', 'autocomplete' => 'off'))}}
				                            {{Form::hidden('codigo_origen', 'SRZ', array('id' => 'codigo_origen1'))}}
                                        </div>
                                        <div class="col-sm-4">
                                            {{Form::label('', 'Destino')}}
                                            {{Form::text('destino', '', array('id' => 'destino1', 'autocomplete' => 'off'))}}
				                            {{Form::hidden('codigo_destino', '', array('id' => 'codigo_destino1'))}}
                                        </div>
                                        <div class="col-sm-4">
                                            {{Form::label('', 'Salida')}}
                                            {{Form::text('fecha_salida', '', array('id' => 'fecha_salida1', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                        </div>
                                        
                                        <div class="separator"></div>

                                        <!-- Datos Generales -->

                                            <div class="col-sm-2">
                                                {{Form::label('', 'Adulto(12+)', array('class' => 'mini-label'))}}
                                                {{Form::select('adultos', array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9'), '1')}}
                                            </div>
                                            <div class="col-sm-2">
                                                {{Form::label('', 'Menor(2-12)', array('class' => 'mini-label'))}}
                                                {{Form::select('menores', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), '0')}}
                                            </div>
                                            <div class="col-sm-2">
                                                {{Form::label('', 'Infante(0-2)', array('class' => 'mini-label'))}}
                                                {{Form::select('infantes', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), '0')}}
                                            </div>
                                            <div class="col-sm-6">
                                                <br/>
                                                {{Form::checkbox('fecha_flexible', '1', false, array('class' => 'hidden fecha_flexible'))}}
                                                <i class="fa fa-square-o check-flex"></i>
                                                <span class="text-flex">Buscar 3 d&iacute;as antes y despu&eacute;s</span>
                                            </div>

                                            <div class="clear"></div>

                                            <div class="col-sm-3">
                                                {{Form::label('', 'Escalas', array('class' => 'mini-label'))}}
                                                {{Form::select('max_conexiones', array('n' => 'Cualquiera', '3' => '3','2' => '2','1' => '1','0' => 'Directo'), 'n')}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::label('', 'Cabina', array('class' => 'mini-label'))}}
                                                {{Form::select('cabina', array('Y' => 'Turista', 'S' => 'Económica Superior','C' => 'Ejecutiva','F' => 'Primera'), 'Y')}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::label('', 'Aerolinea', array('class' => 'mini-label'))}}
                                                {{Form::select('aerolinea',  array('' => 'Cualquiera', 'AA' => 'American Airlines','AR' => 'Aerolíneas Argentinas','AF' => 'AirFrance','AM'=>'AeroMéxico','AV'=>'Avianca','AZ'=>'Alitalia','A4'=>'Aerocon','CM'=>'CopaAirlines','G3'=>'GrupoGol','H2'=>'Sky','IB'=>'Iberia','KE'=>'KoreanAir','LA'=>'LAN','LH'=>'Lufthansa','PZ'=>'TAM Mercosur','UX'=>'AirEuropa','Z8'=>'Amaszonas'), '')}}
                                            </div>
                                            <div class="col-sm-3">
                                                <br/>
                                                {{Form::hidden('modo_vuelo', 1)}}
                                                {{Form::submit('Buscar', array('class' => 'submit-air'))}}
                                            </div>

                                        <!-- /Datos Generales -->

                                    {{Form::close()}}

                                    <div class="clear"></div>

                                </div>
                                <!-- Tab Content 2 -->
                                <div class="tab-pane fade in active" id="tab-2">

                                    {{Form::open(array('url' => 'consulta', 'class' => 'submit-booking', 'id' => '2'))}}
                                    
                                        <div class="col-sm-6">
                                            {{Form::label('', 'Origen')}}
                                            {{Form::text('origen', 'Santa Cruz de la Sierra, Bolivia (SRZ)', array('id' => 'origen2', 'autocomplete' => 'off'))}}
				                            {{Form::hidden('codigo_origen', 'SRZ', array('id' => 'codigo_origen2'))}}
                                        </div>
                                        <div class="col-sm-6">
                                            {{Form::label('', 'Destino')}}
                                            {{Form::text('destino', '', array('id' => 'destino2', 'autocomplete' => 'off'))}}
				                            {{Form::hidden('codigo_destino', '', array('id' => 'codigo_destino2'))}}
                                        </div>
                                        <div class="col-sm-6">
                                            {{Form::label('', 'Salida')}}
                                            {{Form::text('fecha_salida', '', array('id' => 'fecha_salida2', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                        </div>
                                        <div class="col-sm-6">
                                            {{Form::label('', 'Retorno')}}
                                            {{Form::text('fecha_retorno', '', array('id' => 'fecha_retorno', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                        </div>
                                        
                                        <div class="separator"></div>

                                        <!-- Datos Generales -->

                                            <div class="col-sm-2">
                                                {{Form::label('', 'Adulto(12+)', array('class' => 'mini-label'))}}
                                                {{Form::select('adultos', array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9'), '1')}}
                                            </div>
                                            <div class="col-sm-2">
                                                {{Form::label('', 'Menor(2-12)', array('class' => 'mini-label'))}}
                                                {{Form::select('menores', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), '0')}}
                                            </div>
                                            <div class="col-sm-2">
                                                {{Form::label('', 'Infante(0-2)', array('class' => 'mini-label'))}}
                                                {{Form::select('infantes', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), '0')}}
                                            </div>
                                            <div class="col-sm-6">
                                                <br/>
                                                {{Form::checkbox('fecha_flexible', '1', false, array('class' => 'hidden fecha_flexible'))}}
                                                <i class="fa fa-square-o check-flex"></i>
                                                <span class="text-flex">Buscar 3 d&iacute;as antes y despu&eacute;s</span>
                                            </div>

                                            <div class="clear"></div>

                                            <div class="col-sm-3">
                                                {{Form::label('', 'Escalas', array('class' => 'mini-label'))}}
                                                {{Form::select('max_conexiones', array('n' => 'Cualquiera', '3' => '3','2' => '2','1' => '1','0' => 'Directo'), 'n')}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::label('', 'Cabina', array('class' => 'mini-label'))}}
                                                {{Form::select('cabina', array('Y' => 'Turista', 'S' => 'Económica Superior','C' => 'Ejecutiva','F' => 'Primera'), 'Y')}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::label('', 'Aerolinea', array('class' => 'mini-label'))}}
                                                {{Form::select('aerolinea',  array('' => 'Cualquiera', 'AA' => 'American Airlines','AR' => 'Aerolíneas Argentinas','AF' => 'AirFrance','AM'=>'AeroMéxico','AV'=>'Avianca','AZ'=>'Alitalia','A4'=>'Aerocon','CM'=>'CopaAirlines','G3'=>'GrupoGol','H2'=>'Sky','IB'=>'Iberia','KE'=>'KoreanAir','LA'=>'LAN','LH'=>'Lufthansa','PZ'=>'TAM Mercosur','UX'=>'AirEuropa','Z8'=>'Amaszonas'), '')}}
                                            </div>
                                            <div class="col-sm-3">
                                                <br/>
                                                {{Form::hidden('modo_vuelo', 2)}}
                                                {{Form::submit('Buscar', array('class' => 'submit-air'))}}
                                            </div>

                                        <!-- /Datos Generales -->

                                    {{Form::close()}}

                                    <div class="clear"></div>

                                </div>
                                <!-- Tab Content 3 -->
                                <div class="tab-pane fade" id="tab-3">

                                    {{Form::open(array('url' => 'consulta', 'class' => 'submit-booking', 'id' => '3'))}}

                                        <div id="multidestino-container">
                                            <div id="fly-1">
                                                <div class="col-sm-1">
                                                    <br/>
                                                    {{Form::label('', '1:', array('class' => 'title-mutidestino hidden-xs'))}}
                                                    {{Form::label('', 'Vuelo 1:', array('class' => 'title-mutidestino hidden-lg hidden-md hidden-sm'))}}
                                                </div>
                                                <div class="col-sm-4">
                                                    {{Form::label('', 'Origen')}}
                                                    {{Form::text('origen_1', 'Santa Cruz de la Sierra, Bolivia (SRZ)', array('id' => 'origen_1', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_origen_1', 'SRZ', array('id' => 'codigo_origen_1'))}}
                                                </div>
                                                <div class="col-sm-4">
                                                    {{Form::label('', 'Destino')}}
                                                    {{Form::text('destino_1', '', array('id' => 'destino_1', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_destino_1', '', array('id' => 'codigo_destino_1'))}}
                                                </div>
                                                <div class="col-sm-3">
                                                    {{Form::label('', 'Salida')}}
                                                    {{Form::text('fecha_salida_1', '', array('id' => 'fecha_salida_1', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                                </div>
                                            </div>
                                            <div id="fly-2">
                                                <div class="col-sm-1">
                                                    <br/>
                                                    {{Form::label('', '2:', array('class' => 'title-mutidestino hidden-xs'))}}
                                                    {{Form::label('', 'Vuelo 2:', array('class' => 'title-mutidestino hidden-lg hidden-md hidden-sm'))}}
                                                </div>
                                                <div class="col-sm-4">
                                                    {{Form::label('', 'Origen')}}
                                                    {{Form::text('origen_2', '', array('id' => 'origen_2', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_origen_2', '', array('id' => 'codigo_origen_2'))}}
                                                </div>
                                                <div class="col-sm-4">
                                                    {{Form::label('', 'Destino')}}
                                                    {{Form::text('destino_2', '', array('id' => 'destino_2', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_destino_2', '', array('id' => 'codigo_destino_2'))}}
                                                </div>
                                                <div class="col-sm-3">
                                                    {{Form::label('', 'Salida')}}
                                                    {{Form::text('fecha_salida_2', '', array('id' => 'fecha_salida_2', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="multidestino-template" class="hidden">
                                        	<div id="fly-_x_">
                                                <div class="col-sm-1">
                                                    <br/>
                                                    {{Form::label('', '_x_:', array('class' => 'title-mutidestino hidden-xs'))}}
                                                    {{Form::label('', 'Vuelo _x_:', array('class' => 'title-mutidestino hidden-lg hidden-md hidden-sm'))}}
                                                </div>
                                                <div class="col-sm-4">
                                                    {{Form::label('', 'Origen')}}
                                                    {{Form::text('origen__x_', '', array('id' => 'origen__x_', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_origen__x_', '', array('id' => 'codigo_origen__x_'))}}
                                                </div>
                                                <div class="col-sm-4">
                                                    {{Form::label('', 'Destino')}}
                                                    {{Form::text('destino__x_', '', array('id' => 'destino__x_', 'autocomplete' => 'off'))}}
                                                    {{Form::hidden('codigo_destino__x_', '', array('id' => 'codigo_destino__x_'))}}
                                                </div>
                                                <div class="col-sm-3">
                                                    {{Form::label('', 'Salida')}}
                                                    {{Form::text('fecha_salida__x_', '', array('id' => 'fecha_salida__x_', 'autocomplete' => 'off', 'placeholder' => 'dd/mm/aaaa', 'size' => '10'))}}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                        	{{Form::hidden('cant_vuelos', 0, array('id' => 'vuelos'))}}
                                            <a id="delete-fly">- Quitar Vuelo</a>
                                            <a id="add-fly">+ Agregar Vuelo</a>
                                        </div>
                                        
                                        <div class="separator"></div>

                                        <!-- Datos Generales -->

                                            <div class="col-sm-2">
                                                {{Form::label('', 'Adulto(12+)', array('class' => 'mini-label'))}}
                                                {{Form::select('adultos', array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9'), '1')}}
                                            </div>
                                            <div class="col-sm-2">
                                                {{Form::label('', 'Menor(2-12)', array('class' => 'mini-label'))}}
                                                {{Form::select('menores', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), '0')}}
                                            </div>
                                            <div class="col-sm-2">
                                                {{Form::label('', 'Infante(0-2)', array('class' => 'mini-label'))}}
                                                {{Form::select('infantes', array('0' => '0', '1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'), '0')}}
                                            </div>

                                            <div class="clear"></div>

                                            <div class="col-sm-3">
                                                {{Form::label('', 'Escalas', array('class' => 'mini-label'))}}
                                                {{Form::select('max_conexiones', array('n' => 'Cualquiera', '3' => '3','2' => '2','1' => '1','0' => 'Directo'), 'n')}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::label('', 'Cabina', array('class' => 'mini-label'))}}
                                                {{Form::select('cabina', array('Y' => 'Turista', 'S' => 'Económica Superior','C' => 'Ejecutiva','F' => 'Primera'), 'Y')}}
                                            </div>
                                            <div class="col-sm-3">
                                                {{Form::label('', 'Aerolinea', array('class' => 'mini-label'))}}
                                                {{Form::select('aerolinea',  array('' => 'Cualquiera', 'AA' => 'American Airlines','AR' => 'Aerolíneas Argentinas','AF' => 'AirFrance','AM'=>'AeroMéxico','AV'=>'Avianca','AZ'=>'Alitalia','A4'=>'Aerocon','CM'=>'CopaAirlines','G3'=>'GrupoGol','H2'=>'Sky','IB'=>'Iberia','KE'=>'KoreanAir','LA'=>'LAN','LH'=>'Lufthansa','PZ'=>'TAM Mercosur','UX'=>'AirEuropa','Z8'=>'Amaszonas'), '')}}
                                            </div>
                                            <div class="col-sm-3">
                                                <br/>
                                                {{Form::hidden('modo_vuelo', 3)}}
                                                {{Form::submit('Buscar', array('class' => 'submit-air'))}}
                                            </div>

                                        <!-- /Datos Generales -->

                                    {{Form::close()}}

                                    <div class="clear"></div>

                                </div>
                            </div>
                            <!-- End Tab Panels -->
                            
                        </div>

                        <div class="hidden-separator"></div>
                        
                    </div>
                    
                    <div class="col-md-5">
                        
                        @foreach($solicitados as $key => $vuelo)

                        	<!-- Start Service Icon 1 -->
	                        <div class="col-md-12 service-box service-icon-left-more">
	                            <div class="service-content">
	                                <h4 class="link-busqueda" id="{{$key}}">{{$vuelo->nombre_origen}} - {{$vuelo->nombre_destino}} <br class="hidden-lg hidden-md hidden-sm" /><strong class="accent-color fright">{{$monedas[$vuelo->moneda] . $vuelo->precio_final}}</strong></h4>
	                                {{Form::open(array('url' => 'consulta', 'class' => 'hidden buscar' . $key))}}
                                        
                                        {{Form::hidden('origen', $vuelo->nombre_origen . '(' . $vuelo->segmentos[0]->origen . ')')}}
                                        {{Form::hidden('codigo_origen', $vuelo->segmentos[0]->origen)}}
                                        {{Form::hidden('destino', $vuelo->nombre_destino . '(' . $vuelo->segmentos[0]->destino . ')')}}
                                        {{Form::hidden('codigo_destino', $vuelo->segmentos[0]->destino)}}

                                        {{Form::hidden('fecha_salida', date('d/m/Y', strtotime($vuelo->segmentos[0]->fecha)))}}

                                        @if($vuelo->tipo_busqueda == 2)
                                            {{Form::hidden('fecha_retorno', date('d/m/Y', strtotime($vuelo->segmentos[1]->fecha)))}}
                                        @endif

                                        {{Form::hidden('modo_vuelo', $vuelo->tipo_busqueda)}}
                                        {{Form::hidden('adultos', 1)}}
                                        {{Form::hidden('menores', 0)}}
                                        {{Form::hidden('infantes', 0)}}
                                        {{Form::hidden('max_conexiones', 'n')}}
                                        {{Form::hidden('cabina', 'Y')}}
                                        {{Form::hidden('aerolinea', '')}}
										
                                        {{Form::hidden('text', json_encode($vuelo))}}

									{{Form::close()}}
	                            </div>
	                        </div>
	                        <!-- End Service Icon 1 -->
	                        <div class="clear"></div>

                        @endforeach

                    </div>
                    
                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop

@section('code_js')

    {{HTML::script('http://viacontours.com/air-ws/public/js/aerolineas_aeropuertos.js?201508101225')}}
    {{HTML::script('js/busqueda.js?201510211532')}}

@stop

