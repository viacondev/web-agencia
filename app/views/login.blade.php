<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

    <!-- Basic -->
    <title>BoliviaBooking | Login</title>

    <!-- Define Charset -->
    <meta charset="utf-8">

    <!-- Responsive Metatag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Page Description and Author -->
    <meta name="description" content="Margo - Responsive HTML5 Template">
    <meta name="author" content="iThemesLab">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.png" />

    <!-- Bootstrap CSS  -->
    <link rel="stylesheet" href="asset/css/bootstrap.min.css" type="text/css" media="screen">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="screen">

    <!-- Margo CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen">

    <!-- Responsive CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">

    <!-- Css3 Transitions Styles  -->
    <link rel="stylesheet" type="text/css" href="css/animate.css" media="screen">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="css/colors/red.css" title="red" media="screen" />


    <!-- Margo JS  -->
    <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.migrate.js"></script>
    <script type="text/javascript" src="js/modernizrr.js"></script>
    <script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/nivo-lightbox.min.js"></script>
    <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="js/jquery.appear.js"></script>
    <script type="text/javascript" src="js/count-to.js"></script>
    <script type="text/javascript" src="js/jquery.textillate.js"></script>
    <script type="text/javascript" src="js/jquery.lettering.js"></script>
    <script type="text/javascript" src="js/jquery.easypiechart.min.js"></script>
    <script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/mediaelement-and-player.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/login.js"></script>

    <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>

<body>

    <!-- Full Body Container -->
    <div id="container" class="boxed-page">
        
        
        <!-- Start Header Section --> 
        <div class="hidden-header"></div>
        <header class="clearfix" style="position:fixed;">
            
            <!-- Start Top Bar -->
            <div class="top-bar" id="no-slide">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <!-- Start Contact Info -->
                            <ul class="contact-details">
                                <li><a href="#"><i class="fa fa-map-marker"></i> Velasco y Su&aacute;rez de Figueroa, Santa Cruz, BO</a>
                                </li>
                                <li><a href="#"><i class="fa fa-envelope-o"></i> info@boliviabooking.com</a>
                                </li>
                                <li><a href="#"><i class="fa fa-phone"></i> +591 336 3610</a>
                                </li>
                            </ul>
                            <!-- End Contact Info -->
                        </div><!-- .col-md-6 -->
                        <div class="col-md-5">
                            <!-- Start Social Links -->
                            <ul class="social-list">
                                <li>
                                    <a class="user itl-tooltip" data-placement="bottom" title="" href="#" data-original-title="Ingresar" data-toggle="modal" data-target=".bs-example-modal-md">
                                        <span class="label-login">Iniciar Sesi&oacute;n</span><i class="fa fa-user icon-login"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="facebook itl-tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a class="skype itl-tooltip" data-placement="bottom" title="Skype" href="#"><i class="fa fa-skype"></i></a>
                                </li>
                            </ul>
                            <!-- End Social Links -->
                        </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .top-bar -->
            <!-- End Top Bar -->
            
        </header> 
        <!-- End Header Section -->

        <!-- Ventana modal para el formulario del login -->
        <div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <!-- Start Contact Form -->
                        <div id="contact-form" class="contatct-form form-login">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <div class="loader"></div>
                            <div class="row">
                                <div class="col-md-12 center">
                                    <img alt="" src="images/boliviabooking1.png" class="img-login" />
                                </div>
                            </div>
                            
                            {{Form::open(array('url' => 'login', 'class' => 'contactForm loginForm'))}}
                                <div class="row">
                                    <div class="panel-login">
                                        <div class="col-md-12">
                                            <label for="name">Usuario</label>
                                            {{Form::text('nombre', '', array('id' => 'nombre', 'size' => '30'))}}
                                        </div>
                                        <div class="col-md-12">
                                            <label for="e-mail">Contrase&ntilde;a</label>
                                            {{Form::password('password', array('id' => 'password', 'size' => '30'))}}
                                        </div>
                                    </div>
                                    <div class="col-sm-12 unauth-login">
                                        El usuario o contrase&ntilde;a no es v&aacute;lido.
                                    </div>
                                    <div class="col-sm-12 error-login">
                                        Ocurri&oacute; un error, verifique su conexi&oacute;n a Internet.
                                    </div>
                                    <div class="col-sm-12 login-load center">
                                        {{HTML::image('images/cargando.gif', '', array('class' => 'img-loading'))}}
                                        <br/>
                                        Autenticando...
                                    </div>
                                    <div class="col-md-12">
                                        {{Form::submit('Ingresar')}}
                                    </div>
                                </div>
                            {{Form::close()}}

                        </div>
                    <!-- End Contact Form -->
                </div>
            </div>
        </div>        
        <!-- Fin ventana modal -->
        
        <!-- Start Home Page Slider -->
        <section id="home">
            <!-- Carousel -->
            <div id="main-slide" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#main-slide" data-slide-to="0" class="active"></li>
                    <li data-target="#main-slide" data-slide-to="1"></li>
                    <li data-target="#main-slide" data-slide-to="2"></li>
                </ol>
                <!--/ Indicators end-->

                <!-- Carousel inner -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img class="img-responsive" src="images/slider/bg1.jpg" alt="slider">
                        <div class="slider-content">
                            <div class="col-md-12 text-center">
                                <h2 class="animated2 white">
                        		  <span>Bienvenido a <strong>BoliviaBooking</strong></span>
                        	    </h2>
                                <h3 class="animated3 white">
                            		<span>Tu buscador favorito</span>
                            	</h3>
                            </div>
                        </div>
                    </div>
                    <!--/ Carousel item end -->
                    <div class="item">
                        <img class="img-responsive" src="images/slider/bg2.jpg" alt="slider">
                        <div class="slider-content">
                            <div class="col-md-12 text-center">
                                <h2 class="animated4 white">
                                    <span>Una <strong>Herramienta</strong> muy &uacute;til</span>
                                </h2>
                                <h3 class="animated5 white">
                                	<span>F&aacute;cil y R&aacute;pida</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--/ Carousel item end -->
                    <div class="item">
                        <img class="img-responsive" src="images/slider/bg3.jpg" alt="slider">
                        <div class="slider-content">
                            <div class="col-md-12 text-center">
                                <h2 class="animated7 white">
                                    <span>Las mejores <strong>tarifas</strong></span>
                                </h2>
                                <h3 class="animated8 white">
                                	<span>que estabas buscando</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--/ Carousel item end -->
                </div>
                <!-- Carousel inner end-->

                <!-- Controls -->
                <a class="left carousel-control" href="#main-slide" data-slide="prev">
                    <span><i class="fa fa-angle-left"></i></span>
                </a>
                <a class="right carousel-control" href="#main-slide" data-slide="next">
                    <span><i class="fa fa-angle-right"></i></span>
                </a>
            </div>
            <!-- /carousel -->
        </section>
        <!-- End Home Page Slider -->
        
        
        <!-- Start Services Section -->
        <div class="section service">
            <div class="container">
                <div class="row">
                    
                    <!-- Start Service Icon 4 -->
                    <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="04">
                        <div class="service-icon">
                            <i class="fa fa-search icon-large"></i>
                        </div>
                        <div class="service-content">
                            <h4>B&uacute;squeda f&aacute;cil</h4>
                            <p>Encuentra las mejores tarifas y haz tus reservas de forma sencilla, amigable y rápida.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 4 -->

                    <!-- Start Service Icon 1 -->
                    <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
                        <div class="service-icon">
                            <i class="fa fa-book icon-large"></i>
                        </div>
                        <div class="service-content">
                            <h4>Control de tus pasajeros</h4>
                            <p>Ten total control de los datos de tus pasajeros, viajero frecuente, y m&aacute;s.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 1 -->

                    <!-- Start Service Icon 2 -->
                    <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
                        <div class="service-icon">
                            <i class="fa fa-tablet icon-large"></i>
                        </div>
                        <div class="service-content">
                            <h4>Alta Disponibilidad</h4>
                            <p>Accede a nuestra aplicaci&oacute;n desde cualquier lugar y a trav&eacute;s de cualquier dispositivo inteligente.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 2 -->

                    <!-- Start Service Icon 3 -->
                    <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
                        <div class="service-icon">
                            <i class="fa fa-archive icon-large"></i>
                        </div>
                        <div class="service-content">
                            <h4>Control de tus reservas</h4>
                            <p>Cuenta con datos actualizados de tus reservas y configura tus comisiones para tus clientes.</p>
                        </div>
                    </div>
                    <!-- End Service Icon 3 -->

                </div><!-- .row -->
            </div><!-- .container -->
        </div>
        <!-- End Services Section --> 
        
        <!-- Start Footer Section -->
        <footer>
            <div class="container">

                <!-- Start Copyright -->
                <div class="copyright-section">
                    <div class="row">
                        <div class="col-md-6">
                            <p>&copy; 2014 BoliviaBooking -  Todos los Derechos Reservados</p>
                        </div><!-- .col-md-6 -->
                        <div class="col-md-6">
                            <ul class="footer-nav">
                                <li>
                                    <a href="#">Contacto</a>
                                </li>
                            </ul>
                        </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                </div>
                <!-- End Copyright -->

            </div>
        </footer>
        <!-- End Footer Section -->
        
        
    </div>
    <!-- End Full Body Container -->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

    <div id="loader">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>

    

</body>

</html>