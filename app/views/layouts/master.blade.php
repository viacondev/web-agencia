<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

    <!-- Basic -->
    <title>{{Config::get('app.empresa')}}</title>

    <!-- Define Charset -->
    <meta charset="utf-8">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />

    <!-- Responsive Metatag -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- JQueryUI CSS -->
    {{HTML::style('css/jquery-ui-1.10.4.custom.css')}}

    <!-- Bootstrap CSS  -->
    {{HTML::style('asset/css/bootstrap.min.css')}}

    <!-- Font Awesome CSS -->
    {{HTML::style('css/font-awesome.min.css')}}

    <!-- Margo CSS Styles  -->
    {{HTML::style('css/style.css?201601051011')}}

    <!-- Responsive CSS Styles  -->
    {{HTML::style('css/responsive.css')}}

    <!-- Css3 Transitions Styles  -->
    {{HTML::style('css/animate.css')}}

    <!-- Css3 DatePick Styles  -->
    {{HTML::style('css/jquery.datepick.css')}}

    <!-- Color CSS Styles  -->
    {{HTML::style('css/colors/naranja.css?201508101224', array('title' => 'naranja'))}}

    @yield('styles')

</head>

<body>

    <!-- Full Body Container -->
    <div id="container" class="boxed-page">
        
        <!-- Start Header Section --> 
        <div class="hidden-header"></div>
        <header class="clearfix">
            
            <!-- Start Top Bar -->
            <div class="top-bar hidden-lg hidden-md hidden-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Start Social Links -->
                            <ul class="social-list">
                                @if(Config::get('app.facebook') != '')
                                    <li>
                                        <a class="facebook itl-tooltip" data-placement="bottom" title="Facebook" href="https://www.facebook.com/{{Config::get('app.facebook')}}" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                @endif
                                @if(Config::get('app.skype') != '')
                                    <li>
                                        <a class="skype itl-tooltip" data-placement="bottom" title="Skype" href="skype:{{Config::get('app.skype')}}"><i class="fa fa-skype"></i></a>
                                    </li>
                                @endif
                                @if(Config::get('app.mail') != '')
                                    <li>
                                        <a class="mail itl-tooltip" data-placement="bottom" title="E-mail" href="mailto:{{Config::get('app.mail')}}"><i class="fa fa-envelope"></i></a>
                                    </li>
                                @endif
                            </ul>
                            <!-- End Social Links -->
                        </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .top-bar -->
            <!-- End Top Bar -->
            
            
            <!-- Start  Logo & Naviagtion  -->
            <div class="navbar navbar-default navbar-top">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="{{URL::to('/')}}">
                            {{HTML::image('images/logo.png')}}
                        </a>
                    </div>
                    <div class="social-icons-circle hidden-xs">
                        @if(Config::get('app.facebook') != '')
                            <div class="service-box service-icon-left">
                                <a href="https://www.facebook.com/{{Config::get('app.facebook')}}" class="service-icon" target="_blank">
                                    <i class="fa fa-facebook icon-mini-effect icon-effect-1 facebook-icon"></i>
                                </a>
                            </div>
                        @endif
                        @if(Config::get('app.skype') != '')
                            <div class="service-box service-icon-left">
                                <a href="skype:{{Config::get('app.skype')}}" class="service-icon" target="_blank">
                                    <i class="fa fa-skype icon-mini-effect icon-effect-1 skype-icon"></i>
                                </a>
                            </div>
                        @endif
                        @if(Config::get('app.mail') != '')
                            <div class="service-box service-icon-left">
                                <a href="mailto:{{Config::get('app.mail')}}" class="service-icon" target="_blank">
                                    <i class="fa fa-envelope icon-mini-effect icon-effect-1 mail-icon"></i>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="navbar-collapse collapse">

                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav">
                            <li>
                                <a class="active" href="{{URL::to('aereo')}}">Reservar</a>
                            </li>
                            <li>
                                <a href="{{URL::to('contacto')}}">Contacto</a>
                            </li>
                        </ul>
                        <!-- End Navigation List -->

                        <div class="contactbar hidden-sm hidden-xs">
                            <i class="fa fa-phone"></i>
                            <h4>Ll&aacute;manos</h4>
                            <h5><h5><a href="tel:{{Config::get('app.telfPrincipal')}}">{{Config::get('app.telfPrincipal')}}</a></h5>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End Header Logo & Naviagtion -->
            
        </header> 
        <!-- End Header Section -->

        <!-- CONTENIDO PÁGINA WEB -->
                
                @yield('content')
                
        <!-- /termina contenido -->
        
        <!-- Start Footer Section -->
        <footer>
            <div class="container">

                <div class="row footer-widgets">
                    
                    <!-- Start Subscribe & Social Links Widget -->
                    <div class="col-md-6">
                        <div class="footer-widget social-widget">
                            <div class="fleft">
                                <h4>S&iacute;guenos en :&nbsp;&nbsp;&nbsp;&nbsp;</h4>
                            </div>
                            <div class="fleft">
                                <ul class="social-icons">
                                    @if(Config::get('app.facebook') != '')
                                        <li>
                                            <a class="facebook" href="https://www.facebook.com/{{Config::get('app.facebook')}}" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </li>
                                    @endif
                                    @if(Config::get('app.skype') != '')
                                        <li>
                                            <a class="skype" href="skype:{{Config::get('app.skype')}}"><i class="fa fa-skype"></i></a>
                                        </li>
                                    @endif
                                    @if(Config::get('app.mail') != '')
                                        <li>
                                            <a class="mail" href="mailto:{{Config::get('app.mail')}}"><i class="fa fa-envelope"></i></a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div><!-- .col-md-3 -->
                    <!-- End Subscribe & Social Links Widget -->

                    <!-- Start Flickr Widget -->
                    <div class="col-md-6">
                        <div class="footer-widget">
                            <div class="fleft"><h4>Tel&eacute;fono :&nbsp;&nbsp;&nbsp;&nbsp;</h4></div>
                            <div class="fleft"><ul><li><a href="tel:{{Config::get('app.telefonos')}}">{{Config::get('app.telfPrincipal')}}</a></li></ul></div>
                            <div class="clear"></div>
                            <div class="fleft"><h4>Whatsapp :&nbsp;&nbsp;&nbsp;&nbsp;</h4></div>
                            <div class="fleft"><ul><li><a href="tel:{{Config::get('app.whatsapp')}}">{{Config::get('app.whatsapp')}}</a></li></ul></div>
                            <div class="clear"></div>
                            <div class="fleft"><h4>Email :&nbsp;&nbsp;&nbsp;&nbsp;</h4></div>
                            <div class="fleft"><ul><li><a href="mailto:{{Config::get('app.mail')}}" target="_blank">{{Config::get('app.mail')}}</a></li></ul></div>
                        </div>
                        <!--<div class="clear"></div>
                        <div class="footer-widget">
                            
                        </div>
                        <div class="clear"></div>
                        <div class="footer-widget">
                            
                        </div>-->
                    </div><!-- .col-md-3 -->
                    <!-- End Flickr Widget -->
                
                </div><!-- .row -->

                <!-- Start Copyright -->
                <div class="copyright-section">
                    <div class="row">
                        <div class="col-md-12 center">
                            <p>&copy; {{date('Y')}} {{Config::get('app.empresa')}} -  Todos los Derechos Reservados</p>
                        </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                </div>
                <!-- End Copyright -->

            </div>
        </footer>
        <!-- End Footer Section -->
        
        
    </div>
    <!-- End Full Body Container -->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

    <div id="loader">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>

    {{HTML::script('js/jquery-2.1.1.min.js')}}
    {{HTML::script('js/jquery-ui-1.10.1.custom.min.js')}}
    {{HTML::script('js/jquery.migrate.js')}}
    {{HTML::script('js/modernizrr.js')}}
    {{HTML::script('asset/js/bootstrap.min.js')}}
    {{HTML::script('js/jquery.datepick.min.js')}}
    {{HTML::script('js/jquery.fitvids.js')}}
    {{HTML::script('js/owl.carousel.min.js')}}
    {{HTML::script('js/nivo-lightbox.min.js')}}
    {{HTML::script('js/jquery.isotope.min.js')}}
    {{HTML::script('js/jquery.appear.js')}}
    {{HTML::script('js/count-to.js')}}
    {{HTML::script('js/jquery.textillate.js')}}
    {{HTML::script('js/jquery.lettering.js')}}
    {{HTML::script('js/jquery.easypiechart.min.js')}}
    <!--{{HTML::script('js/jquery.nicescroll.min.js')}}-->
    {{HTML::script('js/jquery.parallax.js')}}
    {{HTML::script('js/mediaelement-and-player.js')}}
    {{HTML::script('js/script.js?201508101137')}}

    @yield('code_js')

    <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</body>

</html>