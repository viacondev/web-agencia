<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Viacontours</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

			<!-- SECCIÓN DE ESTILOS -->

				<!--{{HTML::style('css/bootstrap.min.css')}}-->
				<!--{{HTML::style('css/bootstrap-theme.min.css')}}-->
				{{HTML::style('css/layout.css')}}
				<!--{{HTML::style('css/responsive.css')}}-->
				{{HTML::style('css/user_change.css')}}
				{{HTML::style('css/jquery-ui-1.10.4.custom.min.css')}}
				{{HTML::style('css/jquery_printarea.css')}}

				@yield('styles')

			<!-- /TERMINA SECCIÓN CSS -->

			<!-- SECCIÓN ÍCONOS -->

				<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144-precomposed.png" />
			    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/apple-touch-icon-114-precomposed.png" />
			    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/apple-touch-icon-72-precomposed.png" />
			    <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon-57-precomposed.png" />
			    <link rel="shortcut icon" href="images/favicon.png" />

			<!-- /ÍCONOS -->

	</head>
	<body>

		<!-- HEADER Y MENU ================================================== -->
			
			<?php
				// header obtenido desde la página de wordpress 
				//include_once('../../wp-config.php');
				//get_header();
			?>

			<header>
				<div class="container">
					<div class="row"> 
						<div class="col-sm-4">
							<div id="logo">
		                        <a href="http://viacontours.com"   title="Viacon Tours" rel="home">
		                        	<img src="http://viacontours.com/images/logo_260x68.png" alt="Travel Booking" />
		                        </a>  
		                    </div> 
						</div><!-- /col -->
		                    
		                <div class="col-sm-offset-1 col-sm-3 hidden-xs"> 
		                        <div class="callUsTop">
		                            <h4 >LL&#193;MENOS AHORA</h4>
		                            <span>Telefono: <a href="tel: 591-3363610" title="<?php echo $numeroTelefono;?>">+591 - 3 36 3610</a></span>
		                        </div><!-- /callUsTop -->
		                  </div><!-- /col -->
		                    
							<div class="col-md-4 clearfix container-social-top hidden-sm hidden-xs">
								<div class="fRight pRev">
									<div class="social clearfix">
		                                   <ul>
		                                    <li>
		                                    	<a class="cliente" href="http://www.viacontours.com/clientes" target="_blank" title="Sistema Cliente">Cliente</a>
		                                    </li>
		                                    <h4>Clientes</h4>
										</ul>
									</div><!-- /topSocial -->
									<div class="social clearfix">
										<ul>
											<li>
		                                    	<a class="facebook" href="http://facebook.com/viacon.tours" target="_blank" title="Facebook">Facebook</a>
		                                    </li>
		                                    <li>
		                                    	<a class="skype" target="_blank" title="Skype">Skype</a>
		                                    </li>
		                                    <li>
		                                    	<a class="email" href="mailto:info@viacontours.com" target="_blank" title="Contactanos por  email.">Cliente</a>
		                                    </li> 
		                                   </ul>
		                                   <h4>Redes Sociales</h4>
		                                </div> 
								</div><!-- /fRight -->
							</div><!-- /col -->
						
					</div><!-- /row -->
				</div><!-- /container -->
			</header><!-- /header -->

			<div class="mainMenuContainer">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="mainMenu clearfix">
								<em class="mobDropdown"> -- MENU 	--</em>
		                        <ul id="menu-principal" class="">
		                        	<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18 first"><a href="http://viacontours.com/">Inicio</a></li>
									<li id="menu-item-69" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69"><a href="http://viacontours.com/nosotros/">Nosotros</a></li>
									<li id="menu-item-80" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-80"><a href="http://www.viacontours.com/air/public/aereo">Vuelos</a></li>
									<li id="menu-item-19" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-19"><a href="http://www.viacontours.com/travel/paquetes.php">Paquetes Vacacionales</a></li>
									<li id="menu-item-21" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-21"><a href="http://www.viacontours.com/travel/promociones.php">Promociones</a></li>
									<li id="menu-item-89" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-89"><a href="http://viacontours.com/otros-servicios/">Otros Servicios</a></li>
									<li id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-70"><a href="http://viacontours.com/noticias/">Noticias</a></li>
									<li id="menu-item-68" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-68 last"><a href="http://viacontours.com/contactenos/">Contáctanos</a></li>
								</ul>
							</div><!-- /mainMenu -->
						</div><!-- /col -->
					</div><!-- /row -->
				</div><!-- /container -->
			</div><!-- /mainMenuContainer -->

		<!-- /header y menu -->

		<!-- CONTENIDO PÁGINA WEB -->
				
				@yield('content')
				
		<!-- /termina contenido -->

		<!-- FOOTER ================================================== -->
			
			<?php 
				// footer obtenido desde la página de wordpress
				//get_footer();
			?>
			<div id="footer">
				<div class="footerTop">
					<div class="container">
						<div class="row">
		                	<div class="col-sm-3">
								<h5>Contacto <span>Información</span></h5>
								<p>Calle Velasco Esq. Suarez de Figueroa  <br>
								Edif. Don Luis 1er Piso Nro 232</p>
								<p>Llámanos: <a href="tel:+591 – 3 36 3610 " title="Llamanos">+591 – 3 36 3610 </a></p>
								<p>Email: <a href="mailto:info@viacontours.com" title="Email us">info@viacontours.com</a></p>
		                        <br>
		                        <div class="social clearfix">
		                            <ul>
		                                <li class="first">
		                                    <a class="facebook" href="http://www.facebook.com/viacon.tours" target="_blank" title="Facebook">Facebook</a>
		                                </li>
		                                <li>
		                                    <a class="skype" href="" target="_blank" title="Sistema Cliente">Cliente</a>
		                                </li>
		                                <li class="last">
		                                    <a class="email" href="mailto:info@viacontours.com" target="_blank" title="Contactanos por  email.">Cliente</a>
		                                </li> 
		                            </ul>
								</div>
							</div>
							<div class="col-sm-3">
								<h5>Nuestros <span>Servicios</span></h5>
								<div class="footerLinkList">
									<div class="menu-servicios-container">
										<ul id="menu-servicios" class="">
											<li id="menu-item-122" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-122 first"><a href="http://www.viacontours.com/air/public/aereo">Vuelos</a></li>
											<li id="menu-item-123" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-123"><a href="http://www.viacontours.com/travel/paquetes.php">Paquetes Vacacionales</a></li>
											<li id="menu-item-124" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-124"><a href="http://www.viacontours.com/travel/promociones.php">Promociones</a></li>
											<li id="menu-item-131" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-131 last"><a href="http://viacontours.com/otros-servicios/">Otros Servicios</a></li>
										</ul>
									</div> 	
								</div>
							</div>
							<div class="col-sm-3">
								<h5>Soporte al <span>Cliente</span></h5>
								<div class="footerLinkList">
									<div class="menu-menu-superior-container">
										<ul id="menu-menu-superior" class="">
											<li id="menu-item-130" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130 first"><a href="http://viacontours.com/">Inicio</a></li>
											<li id="menu-item-126" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-126"><a href="http://viacontours.com/nosotros/">La Empresa</a></li>
											<li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="http://viacontours.com/contactenos/">Contáctanos</a></li>
											<li id="menu-item-13" class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-13 last"><a href="http://viacontours.com/noticias/">Noticias</a></li>
										</ul>
									</div> 	
								</div>
							</div>
							<div class="col-sm-3">
								<h5>Subscríbete <span></span></h5>
		                        <div class="newslwtter">
		                        <aside id="text-3" class="widget widget_text">			<div class="textwidget"><div class="wpcf7" id="wpcf7-f117-o1" lang="es-ES" dir="ltr">
									<div class="screen-reader-response"></div>
									<form name="" action="/air/public/aereo#wpcf7-f117-o1" method="post" class="wpcf7-form" novalidate="novalidate">
									<div style="display: none;">
									<input type="hidden" name="_wpcf7" value="117">
									<input type="hidden" name="_wpcf7_version" value="4.0.1">
									<input type="hidden" name="_wpcf7_locale" value="es_ES">
									<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f117-o1">
									<input type="hidden" name="_wpnonce" value="4bc4159029">
									</div>
									<div class="generalForm"><span> <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Tu Nombre Completo"></span> </span><br>
									<span> <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Tu Email"></span> </span><br>
									<span><input type="submit" value="SUSCRIBIRME" class="wpcf7-form-control wpcf7-submit"><img class="ajax-loader" src="http://viacontours.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Enviando..." style="visibility: hidden;"></span></div>
									<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div>
											</aside> 
		                        </div>
								
							</div>
						</div>
					</div>
				</div>
				<div class="footerBottom">
					<div class="container">
						<div class="row">
							<div class="col-sm-8">
								<div class="footerMenu clearfix">
		                        	<?php   wp_nav_menu( array( 'theme_location' => 'footer2', 'menu_class' => '' ) ); ?> 								 
								</div><!-- /footerMenu -->
							</div>
							<div class="col-sm-4">
		                    	<div class="copy" >Copyright &copy; <?php echo date('Y');?>. ViaconTours<!-- /copy -->						
							</div>
						</div><!-- /row -->
					</div><!-- /container -->
				</div><!-- /footerBottom -->
			</div>

		<!-- /footer -->

		<!-- SECCIÓN DE CÓDIGO JAVASCRIPT -->

			{{HTML::script('js/jquery-1.8.2.min.js')}}
			{{HTML::script('js/bootstrap.min.js')}}
			{{HTML::script('js/respond.min.js')}}
			{{HTML::script('js/jquery.easing.1.3.js')}}
			{{HTML::script('js/jquery.touchSwipe.min.js')}}
			{{HTML::script('js/responsiveslides.min.js')}}
			{{HTML::script('js/easyResponsiveTabs.js')}}
			{{HTML::script('js/jquery-validation.js')}}
			{{HTML::script('js/jquery.datepick.min.js')}}
			{{HTML::script('js/jquery.customSelect.min.js')}}
			{{HTML::script('js/jquery.simplemodal.js')}}
			{{HTML::script('js/jRating.jquery.js')}}
			{{HTML::script('js/jquery.screwdefaultbuttonsV2.min.js')}}
			{{HTML::script('js/global.js')}}
			{{HTML::script('js/jquery-ui-1.10.1.custom.min.js')}}
			{{HTML::script('js/jquery-tinyNav.js')}}
			{{HTML::script('js/owl.carousel.min.js')}}

			@yield('code_js')

		<!-- /TERMINA SECCIÓN JS -->

	</body>

</html>
