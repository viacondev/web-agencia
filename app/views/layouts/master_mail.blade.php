<!-- SECCIÓN DE ESTILOS -->
	<!-- Algunos css y script fueron comentados pero se debe habilitar para que el aéreo funcione -->
	{{HTML::style('css/bootstrap.min.css')}}
	{{HTML::style('css/bootstrap-theme.min.css')}}
	{{HTML::style('css/layout.css')}}
	{{HTML::style('css/responsive.css')}}
	{{HTML::style('css/user_change.css')}}
	{{HTML::style('css/jquery-ui-1.10.4.custom.min.css')}}
	{{HTML::style('css/jquery_printarea.css')}}

	@yield('styles')

<!-- /TERMINA SECCIÓN CSS -->

<!-- CONTENIDO MAIL -->
		
		@yield('content')
		
<!-- /termina contenido -->