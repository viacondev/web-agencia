@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">
                    
                    <div class="col-md-12">

                        <h4 class="classic-title"><span>Mis Reservas</span></h4>

                        <div class="panel-pax">

                            {{Form::open(array('url' => 'seach_res', 'id' => 'form-pax', 'class'=> 'form-horizontal','id'=>'search_res'))}}
                                
                                <div class="col-sm-9">
                                    {{Form::text('txt_reserva_name', Input::get('txt_reserva_name'), array('placeholder'=>'Ingresa nombre o apellido'))}}
                                </div>
                                <div class="col-sm-3">
                                    {{Form::submit('Buscar')}}
                                </div>
                                
                            {{Form::close()}}

                        </div>

                        <div class="hidden-separator"></div>

                        <div class="tabs-section">
                            
                            <!-- Nav Tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab-1" data-toggle="tab">
                                        <i class="fa fa-thumbs-up hidden-xs"></i>Reservados
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab-2" data-toggle="tab">
                                        <i class="fa fa-check-circle hidden-xs"></i>Emitidos
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab-3" data-toggle="tab">
                                        <i class="fa fa-thumbs-down hidden-xs"></i>Anulados
                                    </a>
                                </li>
                            </ul>
                            
                            <!-- Tab panels -->
                            <div class="tab-content">
                                <!-- Tab Content (Solo Ida) -->
                                <div class="tab-pane fade in active" id="tab-1">
                                    
                                    <div class="pax-separator"></div>
                                    <div class="header-pnrs hidden-xs">
                                        <div class="col-sm-1">Nro</div>
                                        <div class="col-sm-1">C&oacute;digo</div>
                                        <div class="col-sm-3">Ruta</div>
                                        <div class="col-sm-2">Pasajeros</div>
                                        <div class="col-sm-2">Precio</div>
                                        <div class="col-sm-2">Creado</div>
                                        <div class="col-sm-1"></div>
                                    </div>
                                    <div class="pax-separator"></div>
                                    
                                    @foreach($reservados as $key => $pnr)
                                        
                                        <div class="item-pax">
                                            <div class="col-sm-1"><strong class="num-seg">{{$key+1}}.-</strong></div>
                                            <div class="col-sm-1">{{$pnr->codigo}}</div>
                                            <div class="col-sm-3">{{$pnr->ruta}}</div>
                                            <div class="col-sm-2">
                                                @foreach($pnr->pasajeros as $pasajero)
                                                    {{$pasajero->nombre}} {{$pasajero->primer_apellido}}<br/>
                                                @endforeach
                                            </div>
                                            <div class="col-sm-2">
                                                @foreach($pnr->totales as $moneda => $total)
                                                    {{$moneda}} {{ceil($total)}}<br/>
                                                @endforeach
                                            </div>
                                            <div class="col-sm-2">{{date('d/M H:i', strtotime($pnr->fecha))}}</div>
                                            <div class="col-sm-1">
                                                <a href="pnr/{{$pnr->codigo}}">
                                                   <input type="submit" value="Ver" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="pax-separator"></div>

                                    @endforeach

                                </div>
                                <!-- Tab Content 2 -->
                                <div class="tab-pane fade" id="tab-2">

                                    <div class="pax-separator"></div>
                                    <div class="header-pnrs hidden-xs">
                                        <div class="col-sm-1">Nro</div>
                                        <div class="col-sm-1">C&oacute;digo</div>
                                        <div class="col-sm-3">Ruta</div>
                                        <div class="col-sm-2">Pasajeros</div>
                                        <div class="col-sm-2">Precio</div>
                                        <div class="col-sm-2">Creado</div>
                                        <div class="col-sm-1"></div>
                                    </div>
                                    <div class="pax-separator"></div>
                                    
                                    @foreach($emitidos as $key => $pnr)
                                        
                                        <div class="item-pax">
                                            <div class="col-sm-1"><strong class="num-seg">{{$key+1}}.-</strong></div>
                                            <div class="col-sm-1">{{$pnr->codigo}}</div>
                                            <div class="col-sm-3">{{$pnr->ruta}}</div>
                                            <div class="col-sm-2">
                                                @foreach($pnr->pasajeros as $pasajero)
                                                    {{$pasajero->nombre}} {{$pasajero->primer_apellido}}<br/>
                                                @endforeach
                                            </div>
                                            <div class="col-sm-2">
                                                @foreach($pnr->totales as $moneda => $total)
                                                    {{$moneda}} {{ceil($total)}}<br/>
                                                @endforeach
                                            </div>
                                            <div class="col-sm-2">{{date('d/M H:i', strtotime($pnr->fecha))}}</div>
                                            <div class="col-sm-1">
                                                <a href="pnr/{{$pnr->codigo}}">
                                                   <input type="submit" value="Ver" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="pax-separator"></div>

                                    @endforeach

                                </div>
                                <!-- Tab Content 3 -->
                                <div class="tab-pane fade" id="tab-3">

                                    <div class="pax-separator"></div>
                                    <div class="header-pnrs hidden-xs">
                                        <div class="col-sm-1">Nro</div>
                                        <div class="col-sm-1">C&oacute;digo</div>
                                        <div class="col-sm-3">Ruta</div>
                                        <div class="col-sm-2">Pasajeros</div>
                                        <div class="col-sm-2">Precio</div>
                                        <div class="col-sm-2">Creado</div>
                                        <div class="col-sm-1"></div>
                                    </div>
                                    <div class="pax-separator"></div>
                                    
                                    @foreach($anulados as $key => $pnr)
                                        
                                        <div class="item-pax">
                                            <div class="col-sm-1"><strong class="num-seg">{{$key+1}}.-</strong></div>
                                            <div class="col-sm-1">{{$pnr->codigo}}</div>
                                            <div class="col-sm-3">{{$pnr->ruta}}</div>
                                            <div class="col-sm-2">
                                                @foreach($pnr->pasajeros as $pasajero)
                                                    {{$pasajero->nombre}} {{$pasajero->primer_apellido}}<br/>
                                                @endforeach
                                            </div>
                                            <div class="col-sm-2">
                                                @foreach($pnr->totales as $moneda => $total)
                                                    {{$moneda}} {{ceil($total)}}<br/>
                                                @endforeach
                                            </div>
                                            <div class="col-sm-2">{{date('d/M H:i', strtotime($pnr->fecha))}}</div>
                                            <div class="col-sm-1">
                                                <a href="pnr/{{$pnr->codigo}}">
                                                   <input type="submit" value="Ver" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="pax-separator"></div>

                                    @endforeach

                                </div>
                            </div>
                            <!-- End Tab Panels -->
                            
                        </div>

                    </div>

                </div>
                
            </div>
        </div>
    <!-- End content -->

@stop

@section('code_js')

@stop