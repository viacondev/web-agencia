@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="classic-title"><span>Nuevo Cliente</span></h4>

                        <div class="panel-pax">
                        	
                            {{Form::open(array('url' => 'crearcliente', 'id' => 'form-pax'))}}
                            
                                <div class="col-sm-4">
                                    {{Form::label('', 'Nombre')}}
                                    {{Form::text('nombre', Input::get('nombre', ''), array('class' => 'field-required'))}}
                                </div>
                                <div class="col-sm-2">
                                    {{Form::label('', 'Cod Cliente')}}
                                    {{Form::text('idcliente', Input::get('idcliente', ''), array('class' => 'field-required'))}}
                                </div>
                                <div class="col-sm-3">
                                    {{Form::label('', 'Ciudad')}}
                                    <select class="field-required" name="ciudad">
                                        @foreach ($ciudades as $key => $ciudad)
                                            
                                            <option value="{{$ciudad->id}}" @if($ciudad->codigo_iata == 'SRZ') selected @endif >{{$ciudad->nombre}}</option>
                                            
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    {{Form::label('', 'Emisión')}}
                                    <select class="field-required" name="autorizado_emision_aereo">
                                        <option value="0" selected >No autorizado</option>
                                        <option value="1" >Autorizado</option>
                                    </select>
                                </div>

                                <div class="clear"></div>

                                <div class="col-sm-4">
                                    {{Form::label('', 'Direccion')}}
                                    {{Form::textarea('direccion', Input::get('direccion', ''), array('class' => 'field-required', 'rows' => 3))}}
                                </div>

                                <div class="col-sm-4">
                                    {{Form::label('', 'Telefono')}}
                                    {{Form::textarea('telefono', Input::get('telefono', ''), array('class' => 'field-required', 'rows' => 3))}}
                                </div>

                                <div class="col-sm-4">
                                    {{Form::label('', 'Mail')}}
                                    {{Form::textarea('mail', Input::get('mail', ''), array('class' => 'field-required', 'rows' => 3))}}
                                </div>

                                <div class="col-sm-12">
                                    {{Form::submit('Guardar')}}
                                </div>

                            {{Form::close()}}

                        </div>
                    </div>

                </div>
            </div>
        </div>
@stop
