@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="classic-title"><span>Informaci&oacute;n de Agencia</span></h4>

                        <div class="blog-post standard-post">
                            <!-- Post Content -->
                            <div class="post-content col-md-6">
                                <div class="post-type"><i class="fa fa-home"></i></div>
                                <h2><a href="#">{{Auth::user()->cliente->nombre}}</a></h2>
                                <ul class="post-meta">
                                    <li>{{$ciudad->nombre}} - {{$pais->nombre}}</li>
                                </ul> 

                                <div class="row info-pax">

                                    <div class="col-sm-5"><h4>Mails:</h4></div>
                                    <div class="col-sm-7">
                                        {{nl2br(Auth::user()->cliente->mails)}}
                                    </div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-5"><h4>Direcci&oacute;n:</h4></div>
                                    <div class="col-sm-7">
                                        {{nl2br(Auth::user()->cliente->direccion)}}
                                    </div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-5"><h4>Tel&eacute;fonos:</h4></div>
                                    <div class="col-sm-7">
                                        {{nl2br(Auth::user()->cliente->telefonos)}}
                                    </div>

                                </div>

                            </div>
                            <div class="post-content col-md-6">
                                <div class="post-type"><i class="fa fa-user"></i></div>
                                <h2><a href="#">{{Auth::user()->nombre_completo}}</a></h2>
                                <ul class="post-meta">
                                        <li>Usuario Actual</li>
                                </ul> 
                                <div class="row info-pax">

                                    <div class="col-sm-5">Mails</div>
                                    <div class="col-sm-7">{{Auth::user()->mails}}</div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-5">Telefonos</div>
                                    <div class="col-sm-7">{{Auth::user()->telefonos}}</div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-6">
                                    	<input type="button" class="change-password" data-toggle="modal" data-target=".bs-example-modal-sm" value="Cambiar Contrase&ntilde;a" />
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(Auth::user()->rol == 1 ||  Auth::user()->rol == 0)
                            
                            <div class="hidden-separator"></div>

                            <div class="blog-post standard-post">
                                <!-- Post Content -->
                                <div class="post-content">
                                    <div class="post-type"><i class="fa fa-users"></i></div>
                                    <h2><a href="#">Usuarios</a></h2>
                                    <ul class="post-meta">
                                        <li>{{count($usuarios)}} Usuarios en Total</li>
                                    </ul> 

                                    <div class="row info-pax">

    	                                @foreach($usuarios as $usuario)

    	                                    <div class="info-user">
    	                                        <div class="col-sm-3">
    	                                        	<h4>
    	                                        		{{$usuario->nombre}}
    	                                        		(
    	                                        			@if($usuario->rol == 1)
    	                                        				admin
    	                                        			@else
    	                                        				normal
    	                                        			@endif
    	                                        		)
    	                                        	</h4>
    	                                        </div>
    	                                        <div class="col-sm-3">
    	                                            {{$usuario->nombre_completo}}
    	                                        </div>
    	                                        <div class="col-sm-3">
    	                                            {{nl2br($usuario->telefonos)}}
    	                                        </div>
    	                                        <div class="col-sm-3">
    	                                            {{nl2br($usuario->mails)}}
    	                                        </div>
    	                                    </div>

    	                                    <div class="hidden-separator"></div>
    	                                @endforeach

                                    </div>

                                </div>
                            </div>

                            <div class="hidden-separator"></div>

                            <div class="blog-post standard-post">
                                <!-- Post Content -->
                                <div class="post-content">
                                    <div class="post-type"><i class="fa fa-cogs"></i></div>
                                    <h2><a href="#">Configuraci&oacute;n</a></h2>

                                    <div class="hidden-separator"></div>

                                    <div class="row info-pax">

                                        <div class="info-user" id="config-gral">
                                            <div class="col-sm-3">
                                                <h4>Fee Predeterminado</h4>
                                            </div>
                                            <div class="col-sm-5">
                                            	{{Form::open(array('url' => 'editar_airconfig', 'id' => 'form_airconfig'))}}
    	                                            <input type="text" name="porcentaje_fee" value="{{$config->porcentaje_fee}}" />
    	                                            <div class="mini-hidden-separator"></div>
    	                                            <div class="row hidden">
    	                                            	<div class="col-sm-6">
    	                                            		<input type="radio" name="mostrar_fee" value="1" @if($config->mostrar_fee == 1) checked @endif /> Quiero ver precio mas fee desde la b&uacute;squeda
    	                                            	</div>
    	                                            	<div class="col-sm-6">
    	                                            		<input type="radio" name="mostrar_fee" value="2" @if($config->mostrar_fee == 2) checked @endif /> Quiero ver precio mas fee s&oacute;lo como sugerencia
    	                                            	</div>
    	                                            	<div class="mini-hidden-separator"></div>
    	                                            </div>
                                                {{Form::close()}}
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="button" value="Guardar" id="airconfig" />
                                                <div class="config-gral-sucess ms-sucess hidden">Guardado Correctamente !!</div>
                                                <div class="config-gral-error ms-error hidden">Ocurrió un error, intente nuevamente.</div>
                                            </div>
                                        </div>
                                        <div id="loading-config-gral" class="hidden">
                                        	<div class="col-sm-12">
                                        		{{HTML::image('images/cargando.gif', '')}}
                                        		<br/>
                                        		Espere por favor ...
                                        	</div>
                                        </div>

                                        <div class="hidden-separator"></div>
                                        <div class="separator"></div>
                                        <div class="hidden-separator"></div>

                                        <div class="info-fee" id="config-fee">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <h4>Fee Por Aerolinea</h4>
                                                    </div>
                                                    <div class="col-sm-6">
                                                    	{{Form::open(array('url' => 'editar_fee', 'id' => 'editar_fee'))}}
    	                                                	
    	                                                	<div class="row panel-fee-config">
    	                                                		
    	                                                		@foreach($fees as $key => $fee)
    		                                                	
    		                                                		<div class="fee{{$key+1}}">
    			                                                		<div class="col-sm-5">
    			                                                			{{Form::text('porcentaje_fee' . ($key+1), $fee->porcentaje_fee)}}
    			                                                			{{Form::hidden('idfee' . ($key+1), $fee->id, array('id' => 'idfee' . ($key+1)))}}
    			                                                		</div>
    			                                                		<div class="col-sm-7">
    			                                                			<select name="idaerolinea{{$key+1}}">
    			                                                				
    			                                                				@foreach($aerolineas as $aerolinea)
    			                                                					
    			                                                					<option value="{{$aerolinea->id}}" 
    				                                                					@if($aerolinea->id == $fee->idaerolinea)
    				                                                						selected
    				                                                					@endif
    			                                                					>
    			                                                						{{$aerolinea->nombre}}
    			                                                					</option>

    			                                                				@endforeach

    			                                                			</select>
    			                                                		</div>
    		                                                		</div>

    		                                          				<div class="clear"></div>

    	                                                		@endforeach
    	                                                	</div>
    	                                                	<div class="template-fee hidden">
    	                                                		<div class="fee_x_">
    		                                                		<div class="col-sm-5">
    		                                                			{{Form::text('porcentaje_fee_x_', 0)}}
    		                                                		</div>
    		                                                		<div class="col-sm-7">
    		                                                			<select name="idaerolinea_x_">
    			                                                				
    		                                                				@foreach($aerolineas as $aerolinea)
    		                                                					
    		                                                					<option value="{{$aerolinea->id}}" >
    		                                                						{{$aerolinea->nombre}}
    		                                                					</option>

    		                                                				@endforeach

    		                                                			</select>
    		                                                		</div>
    		                                                		<div class="clear"></div>
    	                                                		</div>
    	                                                	</div>

    	                                                	{{Form::hidden('cant_fee', count($fees), array('id' => 'cant_fee'))}}
    	                                                	{{Form::hidden('fees_deleted', '', array('id' => 'fees_deleted'))}}

                                                    	{{Form::close()}}
                                                        <div class="mini-hidden-separator"></div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <i class="fa fa-plus" id="add-fee"></i>
                                                        <i class="fa fa-minus" id="del-fee"></i>
                                                    </div>    
                                                    <div class="col-sm-2">
                                                    	<input type="button" value="Guardar" id="guardar_fee" />
                                                    	<div class="config-fee-sucess ms-sucess hidden">Guardado Correctamente !!</div>
                                                		<div class="config-fee-error ms-error hidden">Ocurrió un error, intente nuevamente.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <div id="loading-config-fee" class="hidden">
                                        	<div class="col-sm-12">
                                        		{{HTML::image('images/cargando.gif', '')}}
                                        		<br/>
                                        		Espere por favor ...
                                        	</div>
                                        </div>

                                        <div class="hidden-separator"></div>
                                        <div class="separator"></div>
                                        <div class="hidden-separator"></div>

                                        <div class="info-user" id="config-logo">
                                        	<div class="col-sm-3">
                                                <h4>Logo de la Agencia</h4>
                                            </div>
                                            <div class="col-sm-3">
                                            	<input type="text" id="url-archivo" readonly />
                                            </div>
                                            <div class="col-sm-3">
    		                                    <label class="cargar">
    												Subir Logo
    												<span>
    													{{Form::open(array('url' => 'subir_logo', 'id' => 'subir-logo', 'class' => 'hidden'))}}
    													{{Form::close()}}
    													<input type="file" id="archivo" name="archivo" class="hidden" />
    												</span>
    											</label>
    											<div class="config-logo-sucess ms-sucess hidden">Guardado Correctamente !!</div>
                                                <div class="config-logo-error ms-error hidden">Ocurrió un error, intente nuevamente.</div>
    										</div>
    										<div class="col-sm-3 logo-agencia">
    											@if($config->logo != '')
    												{{HTML::image('logos/' . $config->logo, '', array('id' => 'el-logo'))}}
    											@else
    												No Ingresado
    											@endif
    										</div>
    									</div>
    									<div id="loading-config-logo" class="hidden">
                                        	<div class="col-sm-12">
                                        		{{HTML::image('images/cargando.gif', '')}}
                                        		<br/>
                                        		Espere por favor ...
                                        	</div>
                                        </div>

    									<div class="hidden-separator"></div>             

                                    </div>

                                </div>
                            </div>

                        @endif

                    </div>

                </div>
                
            </div>
        </div>
    <!-- End content -->

    <!-- Modal para cambio de contraseña por parte del mismo usuario -->
        <div class="modal fade bs-example-modal-sm" style="top:25%" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          	<div class="modal-dialog modal-xs">
            	<div class="modal-content">
                	<div id="contact-form" class="contatct-form form-login">
                    	<div class="modal-header">
                        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        		<span aria-hidden="true">&times;</span>
                        	</button>
                        	<h4 class="modal-title" id="myModalLabel"></h4>
                    	</div>
                    	<div class="modal-body">
                    		<div class="loader"></div>
                    		<div class="hidden-separator"></div>

		                        {{Form::open(array('class' => 'form-password', 'url' => 'change_password', 'id'=>"form-password"))}}

                                    <div class="row">

                                        <div class="col-md-12">
                                            {{Form::label('', 'Contraseña Actual')}}
                                            {{Form::password('password_old', array('id' => 'password_old'))}}
                                        </div>
                                       	<div class="col-md-12">
                                            {{Form::label('', 'Nueva Contraseña')}}
                                            {{Form::password('password_new', array('class' => 'password_new','id'=>'password_new'))}}
                                        </div>
                                        <div class="col-md-12">
                                            {{Form::label('', 'Repita la Contraseña Nueva')}}
                                            {{Form::password('password_repeat', array('id' => 'password_repeat'))}}
                                            <div id="confirm"></div>
                                            <div class="col-sm-12 loading ocultar" style="display:none">
                                                <div class="clear"></div>
                                                {{HTML::image('images/cargando.gif', '')}}
                                                <div class="clearb"></div>
                                            </div>
                                        </div>
                                        <div class="hidden-separator"></div>
                                        <div class="col-md-12">
                                            <input type="hidden" id="idusuario" name="idusuario" value="{{Auth::user()->id}}">
                                            {{Form::submit('Actualizar', array('id'=>'actualizar'))}}
                                        </div>
                                    </div>

		                        {{Form::close()}}
                        
                    	</div>
                	</div>
            	</div>
          	</div>
        </div>
    <!-- End modal password -->

@stop

@section('code_js')

    {{HTML::script('js/mi_cuenta.js')}}

@stop