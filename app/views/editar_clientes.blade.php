@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="classic-title"><span>Edici&oacute;n de Cliente</span></h4>

                        <div class="panel-pax">

                        	{{Form::open(array('url' => 'updateCliente', 'id' => 'form-pax'))}}
                            
                                <div class="col-sm-4">
                                    {{Form::label('', 'Nombre')}}
                                    {{Form::text('nombre', $cliente->nombre, array('class' => 'field-required'))}}
                                </div>

                                <div class="col-sm-2">
                                    {{Form::label('', 'Cod Cliente')}}
                                    {{Form::text('idcliente', $cliente->idcliente, array('class' => 'field-required'))}}
                                </div>

                                <div class="col-sm-3">
                                    {{Form::label('', 'Ciudad')}}
                                    <select class="field-required" name="ciudad">

                                        @foreach ($ciudades as $key => $value)
                                            @if($value->id == $cliente->idciudad)
                                   
                                                <option value="{{$value->id}}" selected><? echo $value->nombre ?></option>
                                            
                                            @else
                                    
                                                <option value="{{$value->id}}"><? echo $value->nombre ?></option>

                                            @endif  
                                        @endforeach

                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    {{Form::label('', 'Emisión')}}
                                    <select class="field-required" name="autorizado_emision_aereo">
                                        <option value="0" 
                                            @if($cliente->autorizado_emision_aereo == 0)  
                                                selected 
                                            @endif >No autorizado</option>
                                        <option value="1" 
                                            @if($cliente->autorizado_emision_aereo == 1)  
                                                selected 
                                            @endif >Autorizado</option>
                                    </select>
                                </div>

                                <div class="clear"></div>
                                
                                <div class="col-sm-4">
                                    {{Form::label('', 'Direccion')}}
                                    {{Form::textarea('direccion', $cliente->direccion, array('class' => 'field-required','rows'=>'3'))}}
                                </div>

                                <div class="col-sm-4">
                                    {{Form::label('', 'Telefono')}}
                                    {{Form::textarea('telefono', $cliente->telefonos, array('class' => 'field-required','rows'=>'3'))}}
                                </div>

                                <div class="col-sm-4">
                                    {{Form::label('', 'Mail')}}
                                    {{Form::textarea('mail', $cliente->mails, array('class' => 'field-required','rows'=>'3'))}}
                                </div>
                                
                                <div class="col-sm-12">
                                    {{Form::hidden('id', $cliente->id)}}
                                    {{Form::submit('Actualizar')}}
                                </div>

                            {{Form::close()}}

                        </div>
                    </div>

                </div>
            </div>
        </div>
@stop
