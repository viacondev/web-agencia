@extends('layouts.master')

@section('content')

	<!-- Start Content -->
        <div id="content">
            <div class="container">
                
                <div class="row">

                    <div class="col-sm-12">

                        <h4 class="classic-title"><span>Informaci&oacute;n de Agencia</span></h4>

                        <div class="blog-post standard-post">
                            <!-- Post Content -->
                            <div class="post-content">
                                <div class="post-type"><i class="fa fa-home"></i></div>
                                <h2><a href="#">{{$cliente->nombre}}</a></h2>
                                <ul class="post-meta">
                                    <li>{{$cliente->ciudad->nombre}} - {{$cliente->ciudad->pais->nombre}}</li>
                                </ul> 

                                <div class="row info-pax">

                                    <div class="col-sm-3"><h4>Mails:</h4></div>
                                    <div class="col-sm-9">
                                        {{nl2br($cliente->mails)}}
                                    </div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Direcci&oacute;n:</h4></div>
                                    <div class="col-sm-9">
                                        {{nl2br($cliente->direccion)}}
                                    </div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Tel&eacute;fonos:</h4></div>
                                    <div class="col-sm-9">
                                        {{nl2br($cliente->telefonos)}}
                                    </div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Emisi&oacute;n:</h4></div>
                                    <div class="col-sm-9">
                                        @if($cliente->autorizado_emision_aereo == 1)
                                            Autorizado
                                        @else
                                            No Autorizado
                                        @endif
                                    </div>

                                    <div class="hidden-separator"></div>

                                    <div class="col-sm-3"><h4>Logo</h4></div>
                                    <div class="col-sm-3">
                                        {{HTML::image('logos/' . $cliente->configuracion->logo, '', array('id' => 'el-logo'))}}
                                    </div>
                                    <div class="col-sm-6">
                                    </div>

                                </div>

                            </div>
                        </div>

                            
                        <div class="hidden-separator"></div>

                        <div class="blog-post standard-post">
                            <!-- Post Content -->
                            <div class="post-content">
                                <div class="post-type"><i class="fa fa-users"></i></div>
                                <h2><a href="#">Usuarios</a></h2>
                                <ul class="post-meta">
                                    <li>{{count($cliente->usuarios)}} Usuarios en Total</li>
                                </ul> 

                                <div class="row info-pax">

	                                @foreach($cliente->usuarios as $usuario)

	                                    <div class="info-user">
	                                        <div class="col-sm-2">
	                                        	<h4>
	                                        		{{$usuario->nombre}}
	                                        		(
	                                        			@if($usuario->rol == 1)
	                                        				admin
	                                        			@else
	                                        				normal
	                                        			@endif
	                                        		)
	                                        	</h4>
	                                        </div>
	                                        <div class="col-sm-2">
	                                            {{$usuario->nombre_completo}}
	                                        </div>
	                                        <div class="col-sm-1">
	                                            {{nl2br($usuario->telefonos)}}
	                                        </div>
	                                        <div class="col-sm-3">
	                                            {{nl2br($usuario->mails)}}
	                                        </div>
	                                        <div class="col-sm-2">
	                                        	{{Form::hidden('id_usuario', $usuario->id)}}
                                                <a href="{{URL::to('ver_usuario/' . $usuario->id)}}">
                                                	<input type="button" class="change-password" value="Editar">
                                                </a>
                                            </div>
                                            <div class="col-sm-2">
                                                <a href="#" id="contraseña" data-toggle="modal" data-target=".bs-example-modal-sm" sectionId="{{$usuario->id}}">
                                                	<input type="button" value="Editar Password" class="change-password" />
                                                </a>
                                            </div>
	                                    </div>

	                                    <div class="hidden-separator"></div>
	                                @endforeach

                                </div>

                            </div>
                        </div>

                        <div class="col-sm-6">
                            <a href="{{URL::to('editar_cliente/' . $cliente->id)}}">
                                {{Form::submit('Editar Cliente')}}
                            </a>
                        </div>
                        <div class="col-sm-6">
                            {{Form::submit('Crear Usuario', array('class' => 'crear-usuario', 'data-original-title' => "Ingresar", 'data-toggle' => "modal", 'data-target' => ".bs-example-modal-lg"))}}
                        </div>

                    </div>

                </div>
                
            </div>
        </div>
    <!-- End content -->

    <!-- Ventana modal para crear un nuevo usuario -->
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content modal-fee">
                    <!-- Start Contact Form -->
                        <div id="contact-form" class="contatct-form form-login">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <div class="loader"></div>
                            <div class="hidden-separator"></div>
                            {{Form::open(array('class' => 'form-fee', 'url' => 'crear_usuario'))}}
                                <div class="row">

                                    <div class="col-md-6">
                                        {{Form::label('', 'Usuario')}}
                                        {{Form::text('nombre', '')}}
                                    </div>
                                    <div class="col-md-6">
                                        {{Form::label('', 'Contraseña')}}
                                        {{Form::password('password', '')}}
                                    </div>

                                    <div class="col-md-6">
                                        {{Form::label('', 'Nombre Completo')}}
                                        {{Form::text('nombre_completo', '')}}
                                    </div>
                                    <div class="col-md-6">
                                        {{Form::label('', 'Rol')}}
                                        {{Form::select('rol', array('1' => 'Administrador', '2' => 'Normal'), '1')}}
                                    </div>
                                    <div class="col-md-6">
                                        {{Form::label('', 'Mails')}}
                                        {{Form::textarea('mails', '', array('rows' => 3))}}
                                    </div>
                                    <div class="col-md-6">
                                        {{Form::label('', 'Teléfonos')}}
                                        {{Form::textarea('telefonos', '', array('rows' => 3))}}
                                    </div>
                                        
                                    <div class="hidden-separator"></div>

                                    <div class="col-md-12">
                                        {{Form::hidden('idcliente', $cliente->id)}}
                                        {{Form::submit('Crear Usuario')}}
                                    </div>

                                </div>
                            {{Form::close()}}

                        </div>
                    <!-- End Contact Form -->
                </div>
            </div>
        </div>        
    <!-- Fin ventana modal -->  

    <!-- ventana modal para cambiar contraseña de otro usuario -->
        <div class="modal fade bs-example-modal-sm" style="top:25%" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xs">
            <div class="modal-content">
                <div id="contact-form" class="contatct-form form-login">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                    <div class="loader"></div>
                    <div class="hidden-separator"></div>
                        {{Form::open(array('class' => 'form-fee', 'url' => 'update_password'))}}
                            
                            <div class="row">

                               <div class="col-md-12">
                                    {{Form::label('', 'Nueva Contraseña')}}
                                    {{Form::password('password', '', array('rows' => 3))}}
                                </div>
                                    
                                <div class="hidden-separator"></div>

                                <div class="col-md-12">
                                	{{Form::hidden('idusuario', '', array('id' => 'idusuario'))}}
                                    {{Form::submit('Actualizar')}}
                                </div>

                            </div>

                        {{Form::close()}}
                    </div>
                </div>
            </div>
          </div>
        </div>
    <!-- Fin de ventana modal -->

@stop

@section('code_js')

    {{HTML::script('js/mi_cuenta.js')}}

@stop