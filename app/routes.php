<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('test_login', 'LoginController@testLogin');

Route::get('/', function()
{
	//return View::make('login');
	return Redirect::to('aereo');
});
/*Route::get('login', function()
{
	return View::make('login');
});*/
/*Route::post('login', 'LoginController@autenticar');

Route::get('logout', 'LoginController@cerrarSesion');*/

/*Route::group(array('before'=>'guest'), function() {*/

	// página de inicio
		Route::get('aereo', 'InicioController@indexVuelos');
		Route::post('aereo', 'InicioController@indexVuelos');

	// sección para hacer reservas

		// búsqueda de vuelos

			// transforma los valores ingresados en un objeto para enviarlos al motor de reservas
				Route::post('consulta', 'BusquedaController@transformarConsulta');
				Route::get('consulta', 'BusquedaController@transformarConsultaAtras');

			// realiza la consulta al motor
				//Route::get('consulta', 'BusquedaController@realizarConsulta');
				Route::get('consulta_atras', 'BusquedaController@transformarConsultaAtras');

			// páginas en que se muestran los resultados
				Route::get('resultados', function() {
					return View::make('resultados');
				});
				Route::get('resultado', 'BusquedaController@realizarConsulta');

			// muestra otros precios encontrados, en la página de inicio y de resultados
				Route::post('consulta_anterior', 'BusquedaController@transformarConsultaLink');

			// va a una página con más resultados
				Route::get('ir_pagina/{pagina}', 'BusquedaController@irPagina');

			// filtra tramos entre los resultados
				Route::get('select_tramo/{seleccion}', 'BusquedaController@filtrarPorTramos');

			// quita un filtro de búsqueda por tramos reestablece los resultados
				Route::get('quitar_tramo/{seleccion}', 'BusquedaController@quitarFiltro');

			// quita todos los filtros de búsqueda
				Route::get('quitar_filtros', 'BusquedaController@quitarTodosLosFiltros');

			// filtra por aerolinea y escalas entre los resultados
				Route::get('select_aer_esc/{aerolinea}/{escalas}', 'BusquedaController@filtrarPorAerolineaEscalas');

			// selecciona un itinerario
				Route::get('select_itinerario/{seleccion}', 'VerificacionController@confirmarDisponibilidad');

		// llenado de datos de pasajeros

			// página de formulario para datos de pasajeros
				Route::get('pasajeros', 'DatosPasajerosController@mostrarFormularioPasajeros');
			// búsqueda de datos de pasajeros
				//Route::get('buscar_datos_pax/{nombre}/{apellido}/{id}', 'DatosPasajerosController@buscarDatosPasajeros');

		// cierre de la reserva

			// realiza el cierre de una reserva
				Route::post('cierre_reserva', 'CierreReservaController@cerrarReserva');
			// en un intento fallido de cerrar reserva, deswcarta y realiza otra búsqueda
				Route::get('descartar', 'CierreReservaController@descartarSeleccion');

		// mostrar la reserva

			// muestra la reserva ya cerrada
				Route::get('reserva', 'DisplayReservaController@mostrarReserva');
			// muestra un pnr creado
				Route::post('ver_reserva', 'DisplayReservaController@mostrarPnrPorCodigo');


	// sección de pasajeros
		// nuevo pasajero
			/*Route::get('nuevo_pasajero', 'PasajeroController@nuevoPasajero');
			Route::post('nuevo_pasajero', 'PasajeroController@registrarPasajero');*/

		// registra pasajero
			//Route::get('perfil_pasajero/{idpax}', 'PasajeroController@mostrarPasajero');

		// edita pasajero
			/*Route::get('editar_pasajero/{idpax}', 'PasajeroController@editarPasajero');
			Route::post('editar_pasajero', 'PasajeroController@guardarPasajeroEditado');*/

		// elimina pasajero
			Route::post('eliminar_pasajero', 'PasajeroController@eliminarPasajero');

		// lista de pasajeros pertenecientes al cliente
			/*Route::get('mis_pasajeros', 'PasajeroController@mostrarPasajeros');
			Route::post('mis_pasajeros', 'PasajeroController@BuscarPasajeros');*/

		// reservas del cliente
			/*Route::get('mis_reservas', 'ReservaController@mostrarReservas');		// obtienen un listado de reservas*/
			Route::get('pnr/{codigo_pnr}', 'ReservaController@obtenerReserva');		// obtiene una reserva a partir del código
			/*Route::post('editar_precio', 'ReservaController@editarPrecio');			// edita los precios finales
			Route::post('anular_pnr', 'ReservaController@anularReserva');*/			// borra el itinerario del pnr
			Route::post('enviar_mail_pnr', 'ReservaController@enviarMailPnr');		// envia por mail una reserva
			/*Route::post('seach_res', 'ReservaController@buscarReserva'); 			// busca según el pasajero
			Route::post('mover_a_emision', 'ReservaController@moverPnrAEmision'); 	// mueve un PNR a un queue para emisión automática*/

		// información de cuenta y configuraciones
			/*Route::get('mi_cuenta', 'ConfiguracionController@informacionPerfil');
			Route::post('editar_airconfig', 'ConfiguracionController@editarConfiguracionGeneral');
			Route::post('editar_fee', 'ConfiguracionController@editarConfiguracionFeePorAerolinea');
			Route::post('subir_logo', 'ConfiguracionController@subirLogo');
			Route::post('change_password' , 'ConfiguracionController@changePassword');*/

		// información de contacto
			Route::get('contacto', 'ContactoController@mostrarContacto');
			Route::post('send_mail_contacto', 'ContactoController@enviarMail');

		// seccion Admin
			// clientes
				/*Route::get('clientes', 'ClienteController@cargarCliente');
				Route::get('mostrarClientes', 'ClienteController@mostrarClientes');
				Route::get('editar_cliente/{id}', 'ClienteController@editClientes');
				Route::post('updateCliente', 'ClienteController@updateCliente');
				Route::post('crearcliente', 'ClienteController@createCliente');
				Route::get('ver_cliente/{id}', 'ClienteController@verCliente');*/
			// gestión de usuarios
				/*Route::post('crear_usuario', 'ClienteController@crearUsuario');
				Route::get('ver_usuario/{id}', 'ClienteController@editarUsuario');
				Route::post('update_usuario', 'ClienteController@updateUsuario');
				// cambia el password desde el mismo usuario
				Route::post('update_password', 'ClienteController@updatePassword');
				// busca el cliente a través de su nombre o apellido
				Route::post('search_cliente', 'ClienteController@buscarCliente');*/
			// búsqueda de reservas de cualquier Agencia
				/*Route::get('pnr_admin/{codigo_pnr}', 'ReservaController@obtenerReservaAdmin'); 	// obtiene una reserva para que pueda verla el administrador
				Route::post('pnr_codigo', 'ReservaController@buscarPnrPorCodigo');				// busca una reserva que se encuentre en la BD de boliviabooking
				Route::post('pnr_pasajero', 'ReservaController@buscarPnrPorPasajeros'); 		// busca reservas a partir del nombre y/o apellido
				Route::get('pnr_pasajero', function() { return View::make('buscar_reservas'); }); 		// busca reservas a partir del nombre y/o apellido*/

		//Route::get('prueba', function() { return Redirect::to('perfil_pasajero/1');});

/*});*/



// pruebas a realizar
	//Route::get('mipruebajhon', function() { return Config::get('app.direccion'); });










//Route::get('error', function(){ return View::make('error'); });