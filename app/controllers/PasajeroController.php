<?php
/**
* 
*/
class PasajeroController extends BaseController
{
	public function nuevoPasajero()
	{
		// obtenemos las aerolineas, ciudades y los paises para los telefonos, documentos y viajero frecuente
		$programas 	= Aerolinea::where('estado', '=', '1')->orderBy('programa_millas', 'asc')->get();
		$ciudades 	= Ciudad::orderBy('nombre', 'asc')->get();
		$paises 	= Pais::orderBy('nombre', 'asc')->get();

		return View::make('nuevo_pax', array('programas' => $programas, 'ciudades' => $ciudades, 'paises' => $paises));
	}

	public function registrarPasajero()
	{
		// transformamos la fecha de nacimiento
			$a_fecha_nac 		= explode('/', Input::get('fecha_nacimiento'));
			$fecha_nacimiento 	= $a_fecha_nac[2] . '-' . $a_fecha_nac[1] . '-' . $a_fecha_nac[0];

		// registramos al pasajero
			$pasajero 	= 	Pasajero::create(array('nombre' 			=> strtoupper(MiString::sanearStringSoloLetrasNumeros(Input::get('nombre'))), 
													'primer_apellido' 	=> strtoupper(MiString::sanearStringSoloLetrasNumeros(Input::get('primer_apellido'))), 
													'segundo_apellido' 	=> strtoupper(MiString::sanearStringSoloLetrasNumeros(Input::get('segundo_apellido'))), 
													'fecha_nacimiento' 	=> $fecha_nacimiento, 
													'sexo' 				=> Input::get('sexo'), 
													'nit' 				=> strtoupper(MiString::sanearStringSoloLetrasNumeros(Input::get('nit'))), 
													'mails' 			=> strtoupper(Input::get('mails')), 
													'direccion' 		=> strtoupper(Input::get('direccion')),
													'idcliente' 		=> Auth::user()->cliente->id));

		// registramos los telefonos y los asociamos al pasajero
			$telefonos = array();
			for ($i=1; Input::has('numero_telf' . $i); $i++)
			{ 
				$telefonos[] 	= 	new Telefono(array('numero' 	=> MiString::sanearStringSoloNumeros(Input::get('numero_telf' . $i)), 
														'idciudad' 	=> Input::get('ciudad_telf' . $i)));
			}
			$pasajero->telefonos()->saveMany($telefonos);

		// registramos los documentos y los asociamos al pasajero
			$documentos = array();
			for ($i=1; Input::has('numero_doc' . $i); $i++) 
			{ 
				$documentos[]	= 	new Documento(array('numero' 	=> MiString::sanearStringSoloNumeros(Input::get('numero_doc' . $i)), 
														'tipo_doc' 	=> Input::get('tipo_doc' . $i), 
														'idpais' 	=> Input::get('pais_doc' . $i)));
			}
			$pasajero->documentos()->saveMany($documentos);

		// registramos los viajero frecuente
			$viaj_frecuentes = array();
			for ($i=1; Input::has('codigo_ff' . $i); $i++) 
			{ 
				$viaj_frecuentes[] 	= 	new FrequentFly(array('codigo' 					=> MiString::sanearStringSoloLetrasNumeros(Input::get('codigo_ff' . $i)), 
																'nombre_registrado' 	=> MiString::sanearStringSoloLetrasNumeros(Input::get('nombre_ff' . $i)), 
																'apellido_registrado' 	=> MiString::sanearStringSoloLetrasNumeros(Input::get('apellido_ff' . $i)), 
																'idaerolinea' 			=> Input::get('aerolinea_ff' . $i)));
			}
			$pasajero->frequentFly()->saveMany($viaj_frecuentes);

		if($pasajero)
			return Redirect::to('perfil_pasajero/' . $pasajero->id);
		else
			return View::make('nuevo_pax');
	}

	public function mostrarPasajero($idpax)
	{
		$pasajero = Pasajero::find($idpax);

		$pasajero->fecha_nacimiento_descripcion = Fecha::fechaEspaniol_dMY($pasajero->fecha_nacimiento);  // transforma fecha a formato '08 de Junio de 1992'
		$pasajero->edad 						= Fecha::edad($pasajero->fecha_nacimiento);

		$pasajero->telfs 	= $pasajero->telefonos;
		foreach ($pasajero->telfs as $key => $tel) 
		{
			$pasajero->telfs[$key]->ciudadtel = $tel->ciudad;	// obtenemos la ciudad a la que pertenece el teléfono para mostrar
		}

		$pasajero->docs 	= $pasajero->documentos;
		foreach ($pasajero->docs as $key => $doc) 
		{
			switch ($doc->tipo_doc) 
			{	// definimos la descripción para mostrar en el tipo de documento
				case 'CI':	$pasajero->docs[$key]->tipo_documento = 'Carnet';		break;
				case 'PP':	$pasajero->docs[$key]->tipo_documento = 'Pasaporte';	break;
				case 'DN':	$pasajero->docs[$key]->tipo_documento = 'DNI';			break;
				default:	$pasajero->docs[$key]->tipo_documento = 'Desconocido';	break;
			}
			$pasajero->docs[$key]->paisdoc = $doc->pais; // obtenemos el país al que pertenece el documento
		}

		$pasajero->ffs 		= $pasajero->frequentFly;
		foreach ($pasajero->ffs as $key => $ff) 
		{
			$pasajero->ffs[$key]->airff = $ff->aerolinea; // obtiene los datos de la aerolinea a la que pertenece el viajero frecuente
		}

		return View::make('perfil_pax', array('pax' => $pasajero));
	}

	public function editarPasajero($idpax)
	{
		$pasajero = Pasajero::find($idpax);

		if($pasajero)
		{
			$pasajero->nacimiento 	= Fecha::fechadmYConSlash($pasajero->fecha_nacimiento);  // transforma fecha a formato '08 de Junio de 1992'

			$pasajero->telfs 		= $pasajero->telefonos;

			$pasajero->docs 		= $pasajero->documentos;

			$pasajero->ffs 			= $pasajero->frequentFly;

			// obtenemos las aerolineas, ciudades y los paises para los telefonos, documentos y viajero frecuente
			$programas 	= Aerolinea::where('estado', '=', '1')->orderBy('programa_millas', 'asc')->get();
			$ciudades 	= Ciudad::orderBy('nombre', 'asc')->get();
			$paises 	= Pais::orderBy('nombre', 'asc')->get();

			return View::make('editar_pax', array('programas' => $programas, 'ciudades' => $ciudades, 'paises' => $paises, 'pasajero' => $pasajero));
		}
		else
		{
			Redirect::to('mis_pasajeros');
		}		
	}

	public function guardarPasajeroEditado()
	{
		$pasajero = Pasajero::find(Input::get('id'));

		// transformamos la fecha de nacimiento
			$a_fecha_nac 		= explode('/', Input::get('fecha_nacimiento'));
			$fecha_nacimiento 	= $a_fecha_nac[2] . '-' . $a_fecha_nac[1] . '-' . $a_fecha_nac[0];

		// actualizamos datos del pasajero
			$pasajero->nombre = strtoupper(MiString::sanearStringSoloLetrasNumeros(Input::get('nombre')));
			$pasajero->primer_apellido = strtoupper(MiString::sanearStringSoloLetrasNumeros(Input::get('primer_apellido')));
			$pasajero->segundo_apellido = strtoupper(MiString::sanearStringSoloLetrasNumeros(Input::get('segundo_apellido')));
			$pasajero->fecha_nacimiento = $fecha_nacimiento;
			$pasajero->sexo = Input::get('sexo');
			$pasajero->nit = strtoupper(MiString::sanearStringSoloLetrasNumeros(Input::get('nit')));
			$pasajero->mails = strtoupper(Input::get('mails'));
			$pasajero->direccion = strtoupper(Input::get('direccion'));

			$pasajero->save();

		// registramos y guardamos los documentos
			// eliminamos los documentos que se quitaron
				$a_docs_eliminados = explode(';', trim(Input::get('docs_deleted'), ';'));
				Documento::destroy($a_docs_eliminados);
			// actualizamos aquellos que se modificaron y registramos los nuevos
				$documentos = array();
				for ($i=1; Input::has('numero_doc' . $i); $i++) 
				{ 
					if(Input::has('iddoc' . $i))
					{	// si existe entonces se debe actualizar el registro
						$doc = Documento::find(Input::get('iddoc' . $i));

						$doc->numero 	= MiString::sanearStringSoloNumeros(Input::get('numero_doc' . $i));
						$doc->tipo_doc 	= Input::get('tipo_doc' . $i);
						$doc->idpais 	= Input::get('pais_doc' . $i);

						$doc->save();
					}
					else
					{	// sino es porque se debe insertar uno nuevo
						$documentos[]	= 	new Documento(array('numero' 	=> MiString::sanearStringSoloNumeros(Input::get('numero_doc' . $i)), 
																'tipo_doc' 	=> Input::get('tipo_doc' . $i), 
																'idpais' 	=> Input::get('pais_doc' . $i)));
					}
				}
				$pasajero->documentos()->saveMany($documentos);

		// registramos y guardamos los teléfonos
			// eliminamos los teléfonos que se quitaron
				$a_telfs_eliminados = explode(';', trim(Input::get('telfs_deleted'), ';'));
				Telefono::destroy($a_telfs_eliminados);
			// actualizamos aquellos que se modificaron y registramos los nuevos
				$telefonos = array();
				for ($i=1; Input::has('numero_telf' . $i); $i++)
				{ 
					if(Input::has('idtelf' . $i))
					{	// si existe entonces se debe actualizar el registro
						$telf = Telefono::find(Input::get('idtelf' . $i));

						$telf->numero 		= MiString::sanearStringSoloNumeros(Input::get('numero_telf' . $i));
						$telf->idciudad 	= Input::get('ciudad_telf' . $i);

						$telf->save();
					}
					else
					{	// sino es porque se debe insertar uno nuevo
						$telefonos[] 	= 	new Telefono(array('numero' 	=> MiString::sanearStringSoloNumeros(Input::get('numero_telf' . $i)), 
																'idciudad' 	=> Input::get('ciudad_telf' . $i)));
					}
				}
				$pasajero->telefonos()->saveMany($telefonos);

		// registramos y guardamos los viajero frecuente
			// eliminamos los viajero frecuente que se quitaron
				$a_ff_eliminados = explode(';', trim(Input::get('ffs_deleted'), ';'));
				FrequentFly::destroy($a_ff_eliminados);
			// actualizamos aquellos que se modificaron y registramos los nuevos
				$viaj_frecuentes = array();
				for ($i=1; Input::has('codigo_ff' . $i); $i++) 
				{ 
					if(Input::has('idff' . $i))
					{	// si existe entonces se debe actualizar el registro
						$ff = FrequentFly::find(Input::get('idff' . $i));

						$ff->codigo 				= MiString::sanearStringSoloLetrasNumeros(Input::get('codigo_ff' . $i));
						$ff->nombre_registrado 		= MiString::sanearStringSoloLetrasNumeros(Input::get('nombre_ff' . $i));
						$ff->apellido_registrado 	= MiString::sanearStringSoloLetrasNumeros(Input::get('apellido_ff' . $i));
						$ff->idaerolinea 			= Input::get('aerolinea_ff' . $i);

						$ff->save();
					}
					else
					{	// sino es porque se debe insertar uno nuevo
						$viaj_frecuentes[] 	= 	new FrequentFly(array('codigo' 					=> MiString::sanearStringSoloLetrasNumeros(Input::get('codigo_ff' . $i)), 
																		'nombre_registrado' 	=> MiString::sanearStringSoloLetrasNumeros(Input::get('nombre_ff' . $i)), 
																		'apellido_registrado' 	=> MiString::sanearStringSoloLetrasNumeros(Input::get('apellido_ff' . $i)), 
																		'idaerolinea' 			=> Input::get('aerolinea_ff' . $i)));
					}
				}
				$pasajero->frequentFly()->saveMany($viaj_frecuentes);

		return Redirect::to('perfil_pasajero/' . Input::get('id', '1'));
	}

	public function eliminarPasajero()
	{
		$pasajero = Pasajero::find(Input::get('id'));

		// eliminamos los documentos
		$pasajero->documentos()->delete();

		// eliminamos los teléfonos
		$pasajero->telefonos()->delete();

		// eliminamos viajero frecuente
		$pasajero->frequentFly()->delete();

		$pasajero->delete();

		return Redirect::to('mis_pasajeros');
	}

	public function mostrarPasajeros()
	{
		return View::make('mis_pasajeros');
	}

	public function buscarPasajeros()
	{
		$criterio 	= Input::get('criterio', '');

		$pasajeros 	= Auth::user()->cliente->pasajeros()->where(function($query) use ($criterio) {
																	$query->orWhere('nombre', 'LIKE', '%' . $criterio . '%')
																			->orWhere('primer_apellido', 'LIKE', '%' . $criterio . '%')
																			->orWhere('segundo_apellido', 'LIKE', '%' . $criterio . '%');
																})->get();

		// guardamos temporalmente la lista de los pasajeros
			Session::put('mis_pasajeros', $pasajeros);

		$cantidad 	= count($pasajeros);

		$paginas 	= ceil($cantidad / 15);

		$display = array();
		for($i=0; $i<15 && $i<count($pasajeros); $i++)
		{
			$pax = $pasajeros[$i];
			$pax->edad 		= Fecha::edad($pax->fecha_nacimiento);
			$pax->reservas 	= $pax->pnrs()->count();

			$display[] = $pax;
		}

		return View::make('mis_pasajeros', array('pasajeros' => $display, 'pagina' => 1, 'paginas' => $paginas));
	}

	public function irAPaginaBusqueda($pagina)
	{
		if(Session::has('mis_pasajeros'))
		{
			$pasajeros 	= Session::get('mis_pasajeros', array());

			$cantidad 	= count($pasajeros);

			$paginas 	= ceil($cantidad / 15);

			$inicio 	= ($pagina - 1) * 15;
			$fin 		= $pagina * 15;

			$display = array();
			for($i=$inicio; $i<$fin && $i<count($pasajeros); $i++)
			{
				$pax = $pasajeros[$i];
				$pax->edad 		= Fecha::edad($pax->fecha_nacimiento);
				$pax->reservas 	= $pax->pnrs()->count();

				$display[] = $pax;
			}

			return View::make('mis_pasajeros', array('pasajeros' => $display, 'pagina' => $pagina, 'paginas' => $paginas));
		}
		else
			return Redirect::to('mis_pasajeros');
	}
}

?>