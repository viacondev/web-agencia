<?php
/**
* 
*/
class ReservaController extends BaseController
{
	/*
	* Obtiene un listado de reservas
	*/
	public function mostrarReservas()
	{
		// obtenemos los pnrs que están reservados, emitidos y anulados
		$reservados = Auth::user()->cliente->pnrsPorCliente(1);
		$emitidos 	= Auth::user()->cliente->pnrsPorCliente(2);
		$anulados 	= Auth::user()->cliente->pnrsPorCliente(3);

		return View::make('mis_reservas', array('reservados' => $reservados, 'emitidos' => $emitidos, 'anulados' => $anulados));
	}

	/*
	* Obtiene la reserva
	*/
	public function obtenerReserva($codigo_pnr)
	{		
		$response = ConsolidadorConexion::consultar('getpnr', array('codigo_pnr' => $codigo_pnr, 'cliente' => Config::get('app.cliente')));
		if(property_exists($response, 'codigo_pnr'))
		{
			Session::put('pnr', $response);
			/*echo "<pre>"; print_r($response); echo "</pre>";
			return 0;*/
			return View::make('vista_reserva');
		}
		else
			return 'NO SE ENCONTRO EL PNR';
	}

	/*
	* Edita el precio final, o fee aplicado por el usuario del sistema
	*/
	public function editarPrecio()
	{
		for ($i=0; Input::has('fee' . $i); $i++) 
		{ 
			$precio = Precio::find(Input::get('idprecio' . $i));

			$precio->fee = Input::get('fee' . $i);

			$precio->save();
		}

		$pnr = Pnr::find(Input::get('idpnr'));

		return Redirect::to('pnr/' . $pnr->codigo);
	}

	/*
	* Anula una reserva que esta en boliviabooking
	*/
	public function anularReserva()
	{
		$consulta 	= (object)array('codigo_pnr' 	=> Input::get('codigo_pnr'), 
									'idpnr' 		=> Input::get('idpnr'), 
									'received_from' => 'ANUL BOLBOOKING ' . substr(Auth::user()->cliente->nombre, 0, 15) . ' US ' . substr(Auth::user()->nombre, 0, 10));

		// Obtenemos pnr del Motor de Reservas
			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($consulta)
			    )
			);

			$response = AirConexion::consultar('anular_pnr', $postdata);

			if(property_exists($response, 'error'))
				return Redirect::to('pnr/' . Input::get('codigo_pnr'))->with('show_error', 'La anulación no pudo completarse, comunicarse por favor con su agente de viajes. <br/>Detalle: ' . $response->error);
			else
			{
				$pnr 			= Pnr::find(Input::get('idpnrlocal'));
				$pnr->estado 	= 3;

				$pnr->save();

				return Redirect::to('pnr/' . Input::get('codigo_pnr'))->with('show_success', 'La reserva ha sido anulada correctamente.');
			}
	}

	/*
	* Busca reservas según el nombre o apellido de pasajero
	*/
	public function buscarReserva()
	{	
		$reservados = Auth::user()->cliente->pnrsPorCliente(1, Input::get('txt_reserva_name'));
		$emitidos 	= Auth::user()->cliente->pnrsPorCliente(2, Input::get('txt_reserva_name'));
		$anulados 	= Auth::user()->cliente->pnrsPorCliente(3, Input::get('txt_reserva_name'));

		return View::make('mis_reservas', array('reservados' => $reservados, 'emitidos' => $emitidos, 'anulados' => $anulados));
	}

	/*
	* Envía por correo el pnr en el formato de tabla
	*/
	public function enviarMailPnr()
	{
		if(Request::ajax())
		{
			$mail_receptor 	= Input::get('email');
			$data = array();

			Mail::send('mails.pnr', $data, function($message) use ($mail_receptor)
			{
			    $message->to($mail_receptor)->subject('Reserva de Boleto Aéreo');
			});

			return 'CORRECTO';
		}
		else
			return 'NO PERMITIDO';
	}

	/*
	* Mueve a emision el PNR
	*/
	public function moverPnrAEmision()
	{
		// Envio del Pnr al Queue de Emision por WebServices
			// Se realiza el envío al queue
			$data 					= new stdClass;
			if(Input::get('moneda') == 'USD')
				$data->numero_queue 	= 410;
			else
				$data->numero_queue 	= 415;
			$data->cod_instruccion 		= 11;
			$data->pcc 					= '9M6B';
			$data->codigo_pnr 			= Input::get('codigo_pnr');
			$data->ver_est_cuentas 		= 1;
			$data->descripcion 			= 'BOLBOOKING US ' . Auth::user()->nombre . ' CLI ' . Auth::user()->cliente->nombre;
			$data->remarks_historico 	= array('EMIS BOLBOOKING US ' . Auth::user()->nombre . ' CLI ' . Auth::user()->cliente->nombre . ' ' . date('dMy Hi'));
			$data->notificar_email 		= array('ricardo.pareja@viacontours.com');

			// Realizamos la consulta al motor para editar la reserva					
			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($data)
			    )
			);
			$response = AirConexion::consultar('queue_mover', $postdata);

		// agregamos automáticamente la observación al realizar el envío 
			if($response->resultado == 'CORRECTO')
			{
				$emision 	= 	Emision::create(array('pendientes' 		=> Input::get('cant_boletos'), 
														'estado' 		=> 0, 
														'idpnr' 		=> Input::get('id'), 
														'idusuario'		=> Auth::user()->id));

				return json_encode((object)array('correcto' => 'La reserva se ha enviado a emisión correctamente y se está procesando. Por favor, espere unos segundos.', 'tiempo_estimado' => $response->tiempo_estimado));
			}
			else
				return json_encode((object)array('errores' => 'La emisión no pudo completarse, comunicarse por favor con su agente de viajes. <br/>Detalle: ' . $response->error));
	}

	// SECCION DE ADMINISTRADOR

		/*
		* Muestra un pnr a partir del código sin restricciones de cliente
		*/
		public function obtenerReservaAdmin($codigo_pnr)
		{
			$codigo 	= $codigo_pnr;
			$miPnr 		= Pnr::where('codigo', '=', $codigo)->first();	// obtiene la reserva registrada en la base de datos local

			if($miPnr)
			{
				$consulta 	= (object)array('codigo_pnr' => $codigo);

				// Obtenemos pnr del Motor de Reservas
					$postdata = http_build_query(
					    array(
					        'consulta' => json_encode($consulta)
					    )
					);

					$response = AirConexion::consultar('obtener', $postdata);

					if(property_exists($response, 'codigo_pnr'))
					{
						// indicador de una vista normal, y no una vista luego de cerrar una reserva
						$response->get = true;
						Session::put('pnr', $response);
					}
					else
					{
						Session::put('error', $codigo);
					}

				$config = $miPnr->usuario->cliente->configuracion;

				// transformamos algunos datos para la vista de la reserva
					if(Session::has('pnr'))
					{
						$pnr = Session::get('pnr');

						// actualizamos el estado de la reserva en la base de datos local
						if($pnr->emitido)
							$miPnr->estado = 2;
						else if($pnr->anulado)
							$miPnr->estado = 3;

						$miPnr->save();		// guardamos el cambio y el timestamp

						if(!property_exists($pnr, 'parseado'))
						{
							if(property_exists($pnr, 'itinerario'))
							{
								// transformamos las fechas y tiempos de vuelo
								foreach ($pnr->itinerario as $i => $segmento) 
								{
									$pnr->itinerario[$i]->fecha_literal 	= Fecha::fechaEspaniol_WdM($segmento->fecha_salida);
									$pnr->itinerario[$i]->t_vuelo_literal 	= Fecha::tiempoDescripcionConPunto($segmento->t_vuelo);
								}	
							}
							
							if(property_exists($pnr, 'precios'))
							{
								// transformamos el tipo de pasajero y moneda
								foreach($pnr->precios as $i => $precio)
								{
									$pnr->precios[$i]->n_moneda 	= str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $precio->moneda);
									$pnr->precios[$i]->n_tipo_pax_s = str_replace(array('ADT', 'CNN', 'INF'), array('Adulto', 'Menor', 'Infante'), $precio->tipo_pax);
									$pnr->precios[$i]->n_tipo_pax_p = str_replace(array('ADT', 'CNN', 'INF'), array('Adultos', 'Menores', 'Infantes'), $precio->tipo_pax);
								}
							}

							/* definir tipo de cabina desde la consulta */
							$pnr->cabina = '';
							
							if(property_exists($pnr, 'totales'))
							{
								// transformamos moneda del monto total
								foreach ($pnr->totales as $key => $total) 
								{
									$pnr->totales->$key->n_moneda 	= str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $key);
								}
							}

							if(property_exists($pnr, 'time_limit_oficina'))
							{
								// convertimos el time_limit
								$pnr->time_limit_oficina_descripcion 	= Fecha::fechaEspaniol_WdMHi($pnr->time_limit_oficina);
							}
							
							if(property_exists($pnr, 'pasajeros'))
							{
								// convertimos a array los asientos de cada pasajero
								foreach($pnr->pasajeros as $key => $pax)
								{
									$pnr->pasajeros[$key]->asientos = get_object_vars((object)$pax->asientos);
								}
							}

							// indicamos que ya fue parseado el pnr
							$pnr->parseado = true;
						}

						$precio_total 	= array(); 	// este valor acumulará los precios indexados por la moneda
						// obtenemos los precios guardados por boliviabooking para la agencia y le sugerimos el fee
						foreach($miPnr->precios as $key => $precio)
						{
							// obtenemos el fee sugerido por el sistema según configuración del cliente agencia
							$fee_sugerido = Auth::user()->cliente->feesAereo()->where('idaerolinea', '=', $precio->idaerolinea)->first();

							// transformamos los tipos de pasajero
							$miPnr->precios[$key]->n_moneda 	= str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $precio->moneda);
							$miPnr->precios[$key]->n_tipo_pax_s = str_replace(array('ADT', 'CNN', 'INF'), array('Adulto', 'Menor', 'Infante'), $precio->tipo_pax);
							$miPnr->precios[$key]->n_tipo_pax_p = str_replace(array('ADT', 'CNN', 'INF'), array('Adultos', 'Menores', 'Infantes'), $precio->tipo_pax);

							// si existe mostramos el fee sugerido por la aerolinea, caso contrario mostramos el de la configuracion general
							if($fee_sugerido)
								$miPnr->precios[$key]->fee_sugerido = $precio->precio * ($precio->porcentaje_fee/100);
							else
								$miPnr->precios[$key]->fee_sugerido = $precio->precio * ($config->porcentaje_fee/100);
									
							// calculamos el subtotal tomando en cuenta el fee
							$miPnr->precios[$key]->subtotal 		= ($precio->precio + $precio->fee) * $precio->cantidad_pax;

							if(array_key_exists($precio->moneda, $precio_total))
								$precio_total[$precio->moneda]->monto 	+= $miPnr->precios[$key]->subtotal;
							else
								$precio_total[$precio->moneda] 			= (object)array('monto' 	=> $miPnr->precios[$key]->subtotal,
																						'n_moneda' 	=> str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $precio->moneda));
						}

						$miPnr->totales = $precio_total; 
						
						$pnr->registro 	= $miPnr;

						Session::put('pnr', $pnr);
					}

				//return json_encode(Session::get('pnr'));

				return View::make('vista_reserva', array('config' => $config, 'cliente' => $miPnr->usuario->cliente));
			}
			else
				return "PNR NO VALIDO, ESTE PNR NO FUE CREADO EN BOLIVIABOOKING O NO EXISTE";		
		}

		/*
		* Obtiene cualquier reserva de cualquier cliente a través de su código de 6 letras
		*/
		public function buscarPnrPorCodigo()
		{
			$codigo_pnr = Input::get('codigo');
			$miPnr 		= Pnr::where('codigo', '=', $codigo_pnr)->orderBy('created_at', 'desc')->first();	// obtiene la reserva registrada en la base de datos local

			if($miPnr)
				return Redirect::to('pnr_admin/' . $codigo_pnr);
			else
				return "Esta Reserva no fue realizada en BoliviaBooking o no existe.";
		}

		/*
		* Realiza una búsqueda de Pnr
		*/
		public function buscarPnrPorPasajeros()
		{
			$nombre 	= Input::get('nombre');
			$apellido 	= Input::get('apellido');
			$a_pnrs 	= array();

			$ids_pnr 	= DB::select("SELECT AP.idpnr FROM bb_pasajero P, air_pnr_pasajero AP Where AP.idpasajero=P.id AND P.nombre LIKE '%" . $nombre . "%' AND (P.primer_apellido LIKE '%" . $apellido . "%' OR P.segundo_apellido LIKE '%" . $apellido . "%')");
			foreach ($ids_pnr as $id_pnr) 
			{
				$a_pnrs[] = $id_pnr->idpnr;
			}

			if(count($a_pnrs) > 0)
				$pnrs 	= Pnr::whereIn('id', $a_pnrs)->orderBy('created_at', 'desc')->take(20)->get();
			else
				$pnrs = array();

			return View::make('buscar_reservas', array('pnrs' => $pnrs));
		}
}

?>