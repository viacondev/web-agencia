<?php
/**
* 
*/
class PruebaController extends BaseController
{
	public function enviarMail()
	{
		$valor = json_decode('{"codigo_pnr":"XZRIHT","telefonos":[{"ciudad":"SRZ","numero":"76644726-H-1.1"},{"ciudad":"SRZ","numero":"3363610-A-1.1"}],"pasajeros":[{"nombre":"FELIX","apellido":"BASCOPE"}],"precios":[{"registro":"9M6B P9XG*AWT 0953\/12JAN15","comando":"WPMUSD\u0087ACM\u0087PADT\u0087KP1\u0087FCA\u0087EO\/T\/IVA USD797.70 BR6.96\u0087RQ","carrier":"CM","nombre_carrier":"COPA Airlines","moneda":"USD","neto":694,"impuesto":236.7,"total_pax":930.7,"tipo_pax":"ADT","cant_pax":1,"subtotal":930.7,"time_limit":"2015-01-15 23:59:00","fee":55.842,"total_pax_mas_fee":986.542,"subtotal_mas_fee":986.542,"cond_gral":"NON END\/REFUND RESTRICTION\/PENALTY FOR CHNGS APPLY","max_equip":[{"n_vuelo":"126","equipaje":"02P"},{"n_vuelo":"252","equipaje":"02P"},{"n_vuelo":"457","equipaje":"02P"},{"n_vuelo":"127","equipaje":"02P"}],"segmentos":[0,1,2,3],"rest_ticket":["LAST DAY TO PURCHASE 15JAN\/2359","GUARANTEED FARE APPL IF PURCHASED BEFORE 15JAN"],"n_moneda":"Usd","n_tipo_pax_s":"Adulto","n_tipo_pax_p":"Adultos"}],"totales":{"USD":{"monto":930.7,"monto_mas_fee":986.542,"n_moneda":"Usd"}},"itinerario":[{"salida":"2015-02-27T0228","fecha_salida":"2015-02-27","hora_salida":"02:28","llegada":"02-27T0614","fecha_llegada":"2015-02-27","hora_llegada":"06:14","t_vuelo":"04.46","nro_vuelo":"0126","clase_srv":"N","estado":"HK","origen":"VVI","nombre_origen":"Santa Cruz","destino":"PTY","nombre_destino":"Panama City","t_equipo":"738","mark_air":"CM","nombre_mark_air":"COPA Airlines","linea_op":"","fecha_literal":"Viernes, 27 de Febrero","t_vuelo_literal":"04hr 46min"},{"salida":"2015-02-27T0944","fecha_salida":"2015-02-27","hora_salida":"09:44","llegada":"02-27T1346","fecha_llegada":"2015-02-27","hora_llegada":"13:46","t_vuelo":"07.02","nro_vuelo":"0252","clase_srv":"N","estado":"HK","origen":"PTY","nombre_origen":"Panama City","destino":"LAS","nombre_destino":"Las Vegas","t_equipo":"738","mark_air":"CM","nombre_mark_air":"COPA Airlines","linea_op":"","fecha_literal":"Viernes, 27 de Febrero","t_vuelo_literal":"07hr 02min"},{"salida":"2015-03-05T2253","fecha_salida":"2015-03-05","hora_salida":"22:53","llegada":"03-06T0805","fecha_llegada":"2015-03-06","hora_llegada":"08:05","t_vuelo":"06.12","nro_vuelo":"0457","clase_srv":"N","estado":"HK","origen":"LAS","nombre_origen":"Las Vegas","destino":"PTY","nombre_destino":"Panama City","t_equipo":"738","mark_air":"CM","nombre_mark_air":"COPA Airlines","linea_op":"","fecha_literal":"Jueves, 05 de Marzo","t_vuelo_literal":"06hr 12min"},{"salida":"2015-03-06T1558","fecha_salida":"2015-03-06","hora_salida":"15:58","llegada":"03-06T2153","fecha_llegada":"2015-03-06","hora_llegada":"21:53","t_vuelo":"04.55","nro_vuelo":"0127","clase_srv":"N","estado":"HK","origen":"PTY","nombre_origen":"Panama City","destino":"VVI","nombre_destino":"Santa Cruz","t_equipo":"738","mark_air":"CM","nombre_mark_air":"COPA Airlines","linea_op":"","fecha_literal":"Viernes, 06 de Marzo","t_vuelo_literal":"04hr 55min"}],"ruta":"VVI\/PTY\/LAS\/PTY\/VVI","ipcc":"9M6B","aaa_ipcc":"9M6B","fecha_creacion":"2015-01-12T0753","agente":"AWT","recibido_por":"PAGINA WEB","condiciones":{"CM":{"nombre_carrier":"COPA Airlines","max_equip":[{"n_vuelo":"126","equipaje":"02P"},{"n_vuelo":"252","equipaje":"02P"},{"n_vuelo":"457","equipaje":"02P"},{"n_vuelo":"127","equipaje":"02P"}],"cond_gral":"NON END\/REFUND RESTRICTION\/PENALTY FOR CHNGS APPLY"}},"time_limit":"2015-01-15 23:59:00","time_limit_oficina":"2015-01-15 18:00:00","cabina":"Turista","time_limit_oficina_descripcion":"Jueves, 15 de Enero a horas 18:00","parseado":true}');	

		Session::put('pnr', $valor);

		$data = array();

		$envio_mail = Mail::send('mails.pnr', $data, function($message)
						{
						    $message->to('jhon.aguilar@viacontours.com')->cc(array('jhon_8692_aguilar@hotmail.com' => 'Jhon', 'sistemas@viacontours.com' => 'Sistemas'))->subject('Reserva de Boleto Aéreo');
						});

		if($envio_mail)
			return 'Resultado del envio satisfactorio: ' . $envio_mail;
		else
			return 'Resultado del envio no realizado: ' . $envio_mail;
	}

	public function mostrarPnr()
	{
		$valor = json_decode('{"codigo_pnr":"XZRIHT","telefonos":[{"ciudad":"SRZ","numero":"76644726-H-1.1"},{"ciudad":"SRZ","numero":"3363610-A-1.1"}],"pasajeros":[{"nombre":"FELIX","apellido":"BASCOPE"}],"precios":[{"registro":"9M6B P9XG*AWT 0953\/12JAN15","comando":"WPMUSD\u0087ACM\u0087PADT\u0087KP1\u0087FCA\u0087EO\/T\/IVA USD797.70 BR6.96\u0087RQ","carrier":"CM","nombre_carrier":"COPA Airlines","moneda":"USD","neto":694,"impuesto":236.7,"total_pax":930.7,"tipo_pax":"ADT","cant_pax":1,"subtotal":930.7,"time_limit":"2015-01-15 23:59:00","fee":55.842,"total_pax_mas_fee":986.542,"subtotal_mas_fee":986.542,"cond_gral":"NON END\/REFUND RESTRICTION\/PENALTY FOR CHNGS APPLY","max_equip":[{"n_vuelo":"126","equipaje":"02P"},{"n_vuelo":"252","equipaje":"02P"},{"n_vuelo":"457","equipaje":"02P"},{"n_vuelo":"127","equipaje":"02P"}],"segmentos":[0,1,2,3],"rest_ticket":["LAST DAY TO PURCHASE 15JAN\/2359","GUARANTEED FARE APPL IF PURCHASED BEFORE 15JAN"],"n_moneda":"Usd","n_tipo_pax_s":"Adulto","n_tipo_pax_p":"Adultos"}],"totales":{"USD":{"monto":930.7,"monto_mas_fee":986.542,"n_moneda":"Usd"}},"itinerario":[{"salida":"2015-02-27T0228","fecha_salida":"2015-02-27","hora_salida":"02:28","llegada":"02-27T0614","fecha_llegada":"2015-02-27","hora_llegada":"06:14","t_vuelo":"04.46","nro_vuelo":"0126","clase_srv":"N","estado":"HK","origen":"VVI","nombre_origen":"Santa Cruz","destino":"PTY","nombre_destino":"Panama City","t_equipo":"738","mark_air":"CM","nombre_mark_air":"COPA Airlines","linea_op":"","fecha_literal":"Viernes, 27 de Febrero","t_vuelo_literal":"04hr 46min"},{"salida":"2015-02-27T0944","fecha_salida":"2015-02-27","hora_salida":"09:44","llegada":"02-27T1346","fecha_llegada":"2015-02-27","hora_llegada":"13:46","t_vuelo":"07.02","nro_vuelo":"0252","clase_srv":"N","estado":"HK","origen":"PTY","nombre_origen":"Panama City","destino":"LAS","nombre_destino":"Las Vegas","t_equipo":"738","mark_air":"CM","nombre_mark_air":"COPA Airlines","linea_op":"","fecha_literal":"Viernes, 27 de Febrero","t_vuelo_literal":"07hr 02min"},{"salida":"2015-03-05T2253","fecha_salida":"2015-03-05","hora_salida":"22:53","llegada":"03-06T0805","fecha_llegada":"2015-03-06","hora_llegada":"08:05","t_vuelo":"06.12","nro_vuelo":"0457","clase_srv":"N","estado":"HK","origen":"LAS","nombre_origen":"Las Vegas","destino":"PTY","nombre_destino":"Panama City","t_equipo":"738","mark_air":"CM","nombre_mark_air":"COPA Airlines","linea_op":"","fecha_literal":"Jueves, 05 de Marzo","t_vuelo_literal":"06hr 12min"},{"salida":"2015-03-06T1558","fecha_salida":"2015-03-06","hora_salida":"15:58","llegada":"03-06T2153","fecha_llegada":"2015-03-06","hora_llegada":"21:53","t_vuelo":"04.55","nro_vuelo":"0127","clase_srv":"N","estado":"HK","origen":"PTY","nombre_origen":"Panama City","destino":"VVI","nombre_destino":"Santa Cruz","t_equipo":"738","mark_air":"CM","nombre_mark_air":"COPA Airlines","linea_op":"","fecha_literal":"Viernes, 06 de Marzo","t_vuelo_literal":"04hr 55min"}],"ruta":"VVI\/PTY\/LAS\/PTY\/VVI","ipcc":"9M6B","aaa_ipcc":"9M6B","fecha_creacion":"2015-01-12T0753","agente":"AWT","recibido_por":"PAGINA WEB","condiciones":{"CM":{"nombre_carrier":"COPA Airlines","max_equip":[{"n_vuelo":"126","equipaje":"02P"},{"n_vuelo":"252","equipaje":"02P"},{"n_vuelo":"457","equipaje":"02P"},{"n_vuelo":"127","equipaje":"02P"}],"cond_gral":"NON END\/REFUND RESTRICTION\/PENALTY FOR CHNGS APPLY"}},"time_limit":"2015-01-15 23:59:00","time_limit_oficina":"2015-01-15 18:00:00","cabina":"Turista","time_limit_oficina_descripcion":"Jueves, 15 de Enero a horas 18:00","parseado":true}');	

		Session::put('pnr', $valor);

		return View::make('mails.pnr');
	}

	public function mostrarPaisesAerolineasCiudades()
	{
		
	}
}

?>