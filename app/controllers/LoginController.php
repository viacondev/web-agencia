<?php 

class LoginController extends BaseController
{
    
    public function autenticar()
    {
        if(Request::ajax())
        {
            //recogemos los campos del formulario y los guardamos en un array
            //para pasarselo al método Auth::attempt
            $userdata = array(
                'nombre'    => Input::get('nombre'),
                'password'  => Input::get('password')
            ); 
     
            //si los datos son correctos y existe un usuario con los mismos se inicia sesión
            //y redirigimos a la home
            if(Auth::attempt($userdata, true))
            {
                return 'SI';
            }
            else
            {
                return 'NO';
            }    
        }
        else
            return 'NO';
        
    }

    public function testLogin()
    {
        $user = new Usuario;
        $user->nombre = 'joe';
        $user->password = Hash::make('123456');
        $user->nombre_completo = 'Jhon Aguilar';
        $user->mails = 'jhon.aguilar@viacontours.com';
        $user->telefonos='12312312';
        $user->rol=1;
        $user->idcliente=1;

        if (!($user->save()))
        {
            dd('user is not being saved to database properly - this is the problem');          
        }

        if (!(Hash::check('123456', Hash::make('123456'))))
        {
            dd('hashing of password is not working correctly - this is the problem');          
        }

        if (!(Auth::attempt(array('nombre' => 'joe', 'password' => '123456'))))
        {
            dd('storage of user password is not working correctly - this is the problem');          
        }

        else
        {
            dd('everything is working when the correct data is supplied - so the problem is related to your forms and the data being passed to the function');
        }
    }

    public function cerrarSesion()
    {
        Auth::logout();

        Session::flush();

        return View::make('login');
    }

 
}