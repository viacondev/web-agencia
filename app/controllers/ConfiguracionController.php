<?php
/**
* 
*/
class ConfiguracionController extends BaseController
{
	/*
	* Obtiene toda la información de perfil y la muestra al usuario
	*/
	public function informacionPerfil()
	{
		// obtenemos toda la información respecto del cliente
		$ciudad 	= Auth::user()->cliente->ciudad;
		$pais 		= $ciudad->pais;
		$usuarios 	= Auth::user()->cliente->usuarios;
		$config 	= Auth::user()->cliente->configuracion;
		$fees 		= Auth::user()->cliente->feesAereo;
		$aerolineas = Aerolinea::where('estado', '=', '1')->orderBy('nombre', 'asc')->get();

		return View::make('mi_cuenta', array('ciudad' => $ciudad, 'pais' => $pais, 'usuarios' => $usuarios, 'config' => $config, 'fees' => $fees, 'aerolineas' => $aerolineas));
	}
	
	/*
	* Actualiza el porcentaje de fee general y la muestra de fee para la búsqueda
	*/
	public function editarConfiguracionGeneral()
	{
		$configuraciones = Auth::user()->cliente->configuracion;

		$configuraciones->porcentaje_fee = Input::get('porcentaje_fee');
		$configuraciones->mostrar_fee = Input::get('mostrar_fee');

		if($configuraciones->save())
			return 1;
		else
			return 0;
	}

	/*
	* Actualiza los fees configurados por aerolinea
	*/
	public function editarConfiguracionFeePorAerolinea()
	{
		// registramos y guardamos el fee por aerolinea
			// eliminamos los porcentajes de fee que se quitaron
				$a_fees_eliminados = explode(';', trim(Input::get('fees_deleted'), ';'));
				AirFee::destroy($a_fees_eliminados);
			// actualizamos aquellos que se modificaron y registramos los nuevos
				$fees 			= array();
				// en este arreglo se guardarán las aerolíneas que se agregaron fee, esto no permitirá que se registre más de un fee para la aerolínea
				$a_aerolineas 	= array();
				for ($i=1; Input::has('porcentaje_fee' . $i); $i++) 
				{ 
					if(!in_array(Input::get('idaerolinea' . $i), $a_aerolineas))
					{
						if(Input::has('idfee' . $i))
						{	// si existe entonces se debe actualizar el registro
							$fee = AirFee::find(Input::get('idfee' . $i));

							$fee->porcentaje_fee 	= Input::get('porcentaje_fee' . $i);
							$fee->idaerolinea 		= Input::get('idaerolinea' . $i);

							$fee->save();
						}
						else
						{	// sino es porque se debe insertar uno nuevo
							$fees[]	= 	new AirFee(array('porcentaje_fee' 	=> Input::get('porcentaje_fee' . $i),
															'idaerolinea' 	=> Input::get('idaerolinea' . $i)));
						}
						$a_aerolineas[] = Input::get('idaerolinea' . $i);
					}
				}
				// asociamos al cliente que está autenticado
				Auth::user()->cliente->feesAereo()->saveMany($fees);

			return 1;
	}

	/*
	* Carga la imagen del logo en el servidor
	*/
	public function subirLogo()
	{
		if(Request::ajax())
		{
			$file 		= Input::file('logo');

			$nombre_archivo = 'logo_' . Auth::user()->cliente->id . '.' . $file->getClientOriginalExtension();
			
			$cliente 		= Auth::user()->cliente;
			$cliente->logo 	= $nombre_archivo;
			$cliente->save();

			if($file->move('logos', $nombre_archivo))
				return 1;
			else
				return 0;
		}
		else
			return 0;
	}

	/*
	* Cambia la contraseña, esto lo hará el mismo usuario
	*/
	public function changePassword()
	{
		if(Request::ajax())
		{
			$userdata = array(
                'nombre'    => Auth::user()->nombre,
                'password'  => Input::get('password_old')
            );

            if(Hash::check(Input::get('password_old'),Auth::user()->password))
            {
                $user=Usuario::find(Input::get('idusuario'));
                $user->password = Hash::make(Input::get('password_new'));
                $user->save();

                return "si";
            }
            return "no";
		}	
			
	}

}

?>