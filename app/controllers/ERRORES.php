object(stdClass)#173 (1) {
  ["OTA_AirLowFareSearchRS"]=>
  object(stdClass)#174 (2) {
    ["@attributes"]=>
    object(stdClass)#175 (1) {
      ["Version"]=>
      string(5) "2.2.0"
    }
    ["stlApplicationResults"]=>
    object(stdClass)#176 (2) {
      ["@attributes"]=>
      object(stdClass)#177 (2) {
        ["xmlnsstl"]=>
        string(33) "http://services.sabre.com/STL/v01"
        ["status"]=>
        string(10) "Incomplete"
      }
      ["stlError"]=>
      object(stdClass)#178 (2) {
        ["@attributes"]=>
        object(stdClass)#179 (2) {
          ["type"]=>
          string(11) "Application"
          ["timeStamp"]=>
          string(26) "2014-11-20T092807.269-0600"
        }
        ["stlSystemSpecificResults"]=>
        object(stdClass)#180 (2) {
          ["stlMessage"]=>
          array(2) {
            [0]=>
            string(25) "PROCESSING ERROR DETECTED"
            [1]=>
            string(29) "No valid journey can be build"
          }
          ["stlShortText"]=>
          string(27) "ERR.SWS.ORCH.PROVIDER_ERROR"
        }
      }
    }
  }
}