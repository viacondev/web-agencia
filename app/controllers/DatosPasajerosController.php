<?php
/**
* 
*/
class DatosPasajerosController extends BaseController
{

	/*
	* Muestra el formulario para el llenado de datos de los pasajeros
	*/
	public function mostrarFormularioPasajeros()
	{
		$response 	= ConsolidadorConexion::consultar('ciudades_paises', array());
		// obtenemos las ciudades para el telefono
        $ciudades 	= $response->ciudades;

		// obtenemos los paises para el documento
		$paises 	= $response->paises;

		return View::make('datos_pasajeros', array('ciudades' => $ciudades, 'paises' => $paises));		
	}

	/*
	* Realiza la búsqueda de los pasajeros registrados para llenar el formulario de pasajeros
	*/
	public function buscarDatosPasajeros($nombre, $apellido, $id)
	{
		if(Request::ajax())
		{
			$pasajeros 	= Auth::user()->cliente->pasajeros()->Where('nombre', 'LIKE', '%' . $nombre . '%')->where(function($query) use ($apellido) {
																												$query->orWhere('primer_apellido', 'LIKE', '%' . $apellido . '%')
																														->orWhere('segundo_apellido', 'LIKE', '%' . $apellido . '%');
																											})->take(10)->get();

			foreach ($pasajeros as $key => $pasajero) 
			{
				// obtenemos los documentos mas el pais al que pertenece
				foreach ($pasajero->documentos as $i => $doc) 
					$doc->pais;

				// obtenemos  los telefonos mas la ciudad al que pertenece				
				foreach ($pasajero->telefonos as $i => $telf) 
					$telf->ciudad;

				// obtenemos los viajero frecuente mas la erolinea a la que pertenece
				foreach ($pasajero->frequentFly as $i => $ff) 
					$ff->aerolinea;
			}
			/*return json_encode($pasajeros);
return "EJECUTADO";*/
			return View::make('buscar_datos_pasajeros', array('pasajeros' => $pasajeros, 'id' => $id));
		}
		else
			return "Solicitud Inválida";
	}

}