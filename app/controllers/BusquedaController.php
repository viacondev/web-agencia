<?php
/**
* 
*/
class BusquedaController extends BaseController
{
	/*
	* Prepara los valores para mostrar resultados
	* La consulta proviene del formulario de búsqueda
	*/
	public function transformarConsulta()
	{
		// Armamos la consulta según el formato necesario para consultar el motor de reservas
			$consulta = new stdClass();

			// indicamos cual es el origen de la consulta para aplicar el fee, el código debe coincidir con el código de la tabla "air_tipo_fee"
			$consulta->consulta_origen 	= 'AV';

			$consulta->cant_adt = Input::get('adultos');
			$consulta->cant_cnn = Input::get('menores');
			$consulta->cant_inf = Input::get('infantes');

			$consulta->max_conexiones 	= Input::get('max_conexiones');
			$consulta->cabina 			= Input::get('cabina');
			$consulta->aerolineas_pref 	= Input::get('aerolinea');

			if(Input::has('tarifas_operadora'))
				$consulta->paquete 	= true;

			if(Input::has('fecha_flexible'))
			{
				$consulta->fecha_flexible 		= true;
				// almacenamos la fecha base de la salida y del retorno si hubiese para armar la matriz
				$a_fecha = explode('/', Input::get('fecha_salida'));
				$consulta->fecha_base_salida 	= $a_fecha[2] . '-' . $a_fecha[1] . '-' . $a_fecha[0];
				if(Input::has('fecha_retorno'))
				{
					$a_fecha = explode('/', Input::get('fecha_retorno'));
					$consulta->fecha_base_retorno 	= $a_fecha[2] . '-' . $a_fecha[1] . '-' . $a_fecha[0];
				}
				// almacenamos el tipo de búsqueda de vuelo
				$consulta->tipo_busqueda 	= Input::get('modo_vuelo');
				// almacenamos el nombre de los aeropuertos
				$consulta->nombre_origen 	= Input::get('origen');
				$consulta->nombre_destino 	= Input::get('destino');
			}

			switch (Input::get('modo_vuelo')) 
			{
				case 1:
					{
						$segmento = new stdClass();
						$segmento->origen 	= Input::get('codigo_origen');
						$segmento->destino 	= Input::get('codigo_destino');

						$a_fecha = explode('/', Input::get('fecha_salida'));
						$segmento->salida = $a_fecha[2] . '-' . $a_fecha[1] . '-' . $a_fecha[0] . 'T00:00:00';

						$consulta->segmentos = array('0' => $segmento);
					}
					break;

				case 2:
					{
						$segmento_ida 		= new stdClass();
						$segmento_vuelta 	= new stdClass();

						$origen 	= Input::get('codigo_origen');
						$destino 	= Input::get('codigo_destino');

						$a_fecha_salida = explode('/', Input::get('fecha_salida'));
						$fecha_salida 	= $a_fecha_salida[2] . '-' . $a_fecha_salida[1] . '-' . $a_fecha_salida[0] . 'T00:00:00';

						$a_fecha_retorno 	= explode('/', Input::get('fecha_retorno'));
						$fecha_retorno 		= $a_fecha_retorno[2] . '-' . $a_fecha_retorno[1] . '-' . $a_fecha_retorno[0] . 'T00:00:00';

						$segmento_ida->origen 	= $origen;
						$segmento_ida->destino 	= $destino;
						$segmento_ida->salida 	= $fecha_salida;

						$segmento_vuelta->origen 	= $destino;
						$segmento_vuelta->destino 	= $origen;
						$segmento_vuelta->salida 	= $fecha_retorno;

						$consulta->segmentos = array('0' => $segmento_ida, '1' => $segmento_vuelta);
					}
					break;

				case 3:
					{
						$i = 1;
						$segmentos = array();
						while(Input::has('origen_' . $i))
						{ 
							$segmento = new stdClass();
							$segmento->origen 	= Input::get('codigo_origen_' . $i);
							$segmento->destino 	= Input::get('codigo_destino_' . $i);

							$a_fecha 			= explode('/', Input::get('fecha_salida_' . $i));
							$segmento->salida 	= $a_fecha[2] . '-' . $a_fecha[1] . '-' . $a_fecha[0] . 'T00:00:00';

							$segmentos[] = $segmento;

							$i++;
						}
						$consulta->segmentos = $segmentos;
					}
					break;
				
				default:
					{}
					break;
			}

			// Almacenamos temporalmente la consulta
				Session::put('consulta', $consulta);

		// Armamos lo que se mostrará en la búsqueda realizada
			$busqueda = new stdClass();
			// Armado de los tramos
			$tramos = array();
			switch (Input::get('modo_vuelo')) 
			{
				case 1:
				{
					$tramo 			= new stdClass();
					$tramo->origen 	= explode(',', Input::get('origen'))[0];
					$tramo->destino = explode(',', Input::get('destino'))[0];
					$tramo->fecha 	= Fecha::fechaEspaniol_dM($consulta->segmentos[0]->salida);
					$tramos[] 		= $tramo;

					$busqueda->modo_vuelo = 1;
				}
				break;

				case 2:
				{
					$tramo 			= new stdClass();
					$tramo->origen 	= explode(',', Input::get('origen'))[0];
					$tramo->destino = explode(',', Input::get('destino'))[0];
					$tramo->fecha 	= Fecha::fechaEspaniol_dM($consulta->segmentos[0]->salida) . ' al ' . Fecha::fechaEspaniol_dM($consulta->segmentos[1]->salida);
					$tramos[] 		= $tramo;

					$busqueda->modo_vuelo = 2;
				}
				break;

				case 3:
				{
					$i = 1;
					$segmentos = array();
					while(Input::has('origen_' . $i))
					{ 
						$tramo 			= new stdClass();
						$tramo->origen 	= explode(',', Input::get('origen_' . $i))[0];
						$tramo->destino = explode(',', Input::get('destino_' . $i))[0];
						$tramo->fecha 	= Fecha::fechaEspaniol_dM($consulta->segmentos[$i-1]->salida);
						$tramos[] 		= $tramo;

						$i++;
					}

					$busqueda->modo_vuelo = 3;
				}
				break;
				
				default:
				{}
				break;
			}
			$busqueda->tramos = $tramos;

			// Armado de los pasajeros
			$pasajeros = array();
			if($consulta->cant_adt > 0)
				$pasajeros[] = $consulta->cant_adt . ' Adultos';
			if($consulta->cant_cnn > 0)
				$pasajeros[] = $consulta->cant_cnn . ' Menores';
			if($consulta->cant_inf > 0)
				$pasajeros[] = $consulta->cant_inf . ' Infantes';
			$busqueda->pasajeros = $pasajeros;

			// Cabina
			$a_cabinas 			= array('Y' => 'Turista', 'S' => 'Económica Superior','C' => 'Ejecutiva','F' => 'Primera');
			$busqueda->cabina 	= $a_cabinas[$consulta->cabina];

			// Almacenamos temporalmente la búsqueda realizada
			Session::put('busqueda', $busqueda);

		// Obtenemos precios anteriores del Motor de Reservas
			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($consulta)
			    )
			);

			$result = AirConexion::consultar('cached', $postdata);

			// aqui guardamos la configuracion de fee
				$feeConfig = Config::get('app.fee');
				$feeMinimo = (array_key_exists('minimo', $feeConfig)) ? $feeConfig['minimo'] : 0;
			foreach ($result as $i => $tramo) 
			{
				$result[$i]->time = Fecha::diferenciaHorasMinutos($tramo->busqueda->date);
				$a_fechas 	= array();
				foreach ($tramo->segmentos as $j => $segmento) 
				{
					$a_fechas[] = Fecha::fechaEspaniolDM($segmento->fecha);
				}
				$result[$i]->fechas = implode(' - ', $a_fechas);
				// agregamos el fee del cliente
				$factorFee 					= array_key_exists($tramo->carrier, $feeConfig) ? 1 + (floatval($feeConfig[$tramo->carrier])/100) : 1 + (floatval($feeConfig['default'])/100);
				if(ceil($tramo->precio * $factorFee) - ceil($tramo->precio) < $feeMinimo)
					$result[$i]->precio_final = ceil($tramo->precio + $feeMinimo);
				else	
					$result[$i]->precio_final 	= ceil($tramo->precio * $factorFee);
			}

			$monedas = array('BOB' => 'Bs', 'USD' => 'Usd');

			Session::put('buscar_vuelos', 1);

		return View::make('resultados', array('busqueda' => $busqueda, 'precios_anteriores' => $result, 'monedas' => $monedas));
	}
	/*
	* Función que se ejecuta al presionar el botón "Volver"
	* el botón se encuentra en el formulario de pasajeros
	*/
	public function transformarConsultaAtras()
	{
		// Se obtienen los valores nuevamente
			$busqueda = Session::get('busqueda');
			$consulta = Session::get('consulta');

		// Obtenemos precios anteriores del Motor de Reservas
			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($consulta)
			    )
			);

			$result = AirConexion::consultar('cached', $postdata);

			// aqui guardamos la configuracion de fee
				$feeConfig = Config::get('app.fee');
				$feeMinimo = (array_key_exists('minimo', $feeConfig)) ? $feeConfig['minimo'] : 0;
			foreach ($result as $i => $tramo) 
			{
				$result[$i]->time = Fecha::diferenciaHorasMinutos($tramo->busqueda->date);
				$a_fechas 	= array();
				foreach ($tramo->segmentos as $j => $segmento) 
				{
					$a_fechas[] = Fecha::fechaEspaniolDM($segmento->fecha);
				}
				$result[$i]->fechas = implode(' - ', $a_fechas);
				// agregamos el fee del cliente
				$factorFee 					= array_key_exists($tramo->carrier, $feeConfig) ? 1 + (floatval($feeConfig[$tramo->carrier])/100) : 1 + (floatval($feeConfig['default'])/100);
				if(ceil($tramo->precio * $factorFee) - ceil($tramo->precio) < $feeMinimo)
					$result[$i]->precio_final 	= ceil($tramo->precio + $feeMinimo);
				else
					$result[$i]->precio_final 	= ceil($tramo->precio * $factorFee);
			}

			$monedas = array('BOB' => 'Bs', 'USD' => 'Usd');

			Session::put('buscar_vuelos', 1);

		return View::make('resultados', array('busqueda' => $busqueda, 'precios_anteriores' => $result, 'monedas' => $monedas));
	}
	/*
	* Prepara los valores para mostrar resultados
	* La consulta será desde a través de un link de precios anteriores
	*/
	public function transformarConsultaLink()
	{
		$request 	= json_decode(Input::get('consulta'));

		// Armamos la consulta según el formato necesario para consultar el motor de reservas
			$consulta 	= (object)array('cant_adt' => 1,
										'cant_cnn' => 0,
										'cant_inf' => 0,
										'max_conexiones' => 'n',
										'cabina' => 'Y',
										'aerolineas_pref' => '',
										'consulta_origen' => 'AV');

			// pasamos al formato para la consulta de segmentos
			$segmentos = $request->segmentos;
			$consulta->segmentos = array();
			foreach ($segmentos as $seg) 
			{
				$consulta->segmentos[] 	= (object)array('origen' => $seg->origen, 
														'destino' => $seg->destino, 
														'salida' => explode(' ', $seg->fecha)[0] . 'T00:00:00');
			}

			// Almacenamos temporalmente la consulta
				Session::put('consulta', $consulta);

		// Armamos lo que se mostrará en la búsqueda realizada
			$busqueda 				= new stdClass();
			$busqueda->modo_vuelo 	= $request->tipo_busqueda;
			$busqueda->pasajeros 	= array('01 Adulto');
			$busqueda->cabina 		= 'Turista';
			$busqueda->tramos 		= array();
			// Armado de los tramos
			foreach ($segmentos as $seg) 
			{
				$busqueda->tramos[] 	= (object)array('origen' => $seg->nombre_origen . '(' . $seg->origen .')',
														'destino' => $seg->nombre_destino . '(' . $seg->destino .')',
														'fecha' => Fecha::fechaEspaniol_dM(explode(' ', $seg->fecha)[0]));
			}
			// Almacenamos temporalmente la búsqueda realizada
			Session::put('busqueda', $busqueda);

			// Valores ingresados y utilizados para mostrar en el formulario de búsqueda
			$input = array();

			$input['solo_ida'] 		= false;
			$input['p_solo_ida'] 	= ' ocultar';
			$input['ida_vuelta'] 	= false;
			$input['p_ida_vuelta'] 	= ' ocultar';
			$input['multidestino'] 	= false;
			$input['p_multidestino']= ' ocultar';
			$input['adultos'] 		= 1;
			$input['menores'] 		= 0;
			$input['infantes'] 		= 0;
			$input['max_conexiones']= 'n';
			$input['cabina']		= 'Y';
			$input['aerolinea']		= '';
			$input['modo_vuelo'] 	= $request->tipo_busqueda;
			// valores predeterminados
			$input['origen'] 			= '';
			$input['codigo_origen'] 	= '';
			$input['destino'] 			= '';
			$input['codigo_destino'] 	= '';
			$input['fecha_salida'] 		= '';
			$input['fecha_retorno'] 	= '';
			$input['origen_1'] 			= '';
			$input['codigo_origen_1'] 	= '';
			$input['destino_1'] 		= '';
			$input['codigo_destino_1'] 	= '';
			$input['fecha_salida_1'] 	= '';
			$input['origen_2'] 			= '';
			$input['codigo_origen_2'] 	= '';
			$input['destino_2'] 		= '';
			$input['codigo_destino_2'] 	= '';
			$input['fecha_salida_2'] 	= '';

			switch ($input['modo_vuelo']) 
			{
				case 1:
					{
						$input['solo_ida'] 		= true;
						$input['p_solo_ida'] 	= '';
						$input['origen'] 		= $request->segmentos[0]->nombre_origen . '(' . $request->segmentos[0]->origen . ')';
						$input['codigo_origen'] = $request->segmentos[0]->origen;
						$input['destino'] 		= $request->segmentos[0]->nombre_destino . '(' . $request->segmentos[0]->destino . ')';
						$input['codigo_destino'] 	= $request->segmentos[0]->destino;
						$input['fecha_salida'] 	= date('d/m/Y', strtotime($request->segmentos[0]->fecha));
					}
					break;

				case 2:
					{
						$input['ida_vuelta'] 	= true;	
						$input['p_solo_ida'] 	= '';
						$input['p_ida_vuelta'] 	= '';
						$input['origen'] 		= $request->segmentos[0]->nombre_origen . '(' . $request->segmentos[0]->origen . ')';
						$input['codigo_origen'] = $request->segmentos[0]->origen;
						$input['destino'] 		= $request->segmentos[0]->nombre_destino . '(' . $request->segmentos[0]->destino . ')';
						$input['codigo_destino'] 	= $request->segmentos[0]->destino;
						$input['fecha_salida'] 	= date('d/m/Y', strtotime($request->segmentos[0]->fecha));
						$input['fecha_retorno'] = date('d/m/Y', strtotime($request->segmentos[1]->fecha));
					}
					break;

				case 3:
					{
						$input['multidestino'] 	= true;
						$input['p_multidestino']= '';
						foreach ($segmentos as $key => $seg) 
						{
							$input['origen_' . ($key+1)] 		=  $seg->nombre_origen . '(' . $seg->origen . ')';
							$input['codigo_origen_' . ($key+1)] = $request->segmentos[0]->origen;
							$input['destino_' . ($key+1)] 		=  $seg->nombre_destino . '(' . $seg->destino . ')';
							$input['codigo_destino_' . ($key+1)] = $request->segmentos[0]->destino;
							$input['fecha_salida_' . ($key+1)] =  date('d/m/Y', strtotime($seg->fecha));
						}
					}
					break;
				
				default:
					$input['ida_vuelta'] 	= true;
					break;
			}

			// Almacenamos temporalmente los valores ingresados en la búsqueda
			Session::put('input', $input);

			// Obtenemos precios anteriores del Motor de Reservas
			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($consulta)
			    )
			);

			$result = AirConexion::consultar('cached', $postdata);

			// aqui guardamos la configuracion de fee
				$feeConfig = Config::get('app.fee');
				$feeMinimo = (array_key_exists('minimo', $feeConfig)) ? $feeConfig['minimo'] : 0;
			foreach ($result as $i => $tramo) 
			{
				$result[$i]->time = Fecha::diferenciaHorasMinutos($tramo->busqueda->date);
				$a_fechas 	= array();
				foreach ($tramo->segmentos as $j => $segmento) 
				{
					$a_fechas[] = Fecha::fechaEspaniolDM($segmento->fecha);
				}
				$result[$i]->fechas = implode(' - ', $a_fechas);
				// agregamos el fee del cliente
				$factorFee 					= array_key_exists($tramo->carrier, $feeConfig) ? 1 + (floatval($feeConfig[$tramo->carrier])/100) : 1 + (floatval($feeConfig['default'])/100);
				if(ceil($tramo->precio * $factorFee) - ceil($tramo->precio) < $feeMinimo)
					$result[$i]->precio_final 	= ceil($tramo->precio + $feeMinimo);
				else
					$result[$i]->precio_final 	= ceil($tramo->precio * $factorFee);
			}

			$monedas = array('BOB' => 'Bs', 'USD' => 'Usd');

			Session::put('buscar_vuelos', 1);

		return View::make('resultados', array('busqueda' => $busqueda, 'precios_anteriores' => $result, 'monedas' => $monedas));
	}

	/*
	* realiza la consulta al motor de reservas
	*/
	public function realizarConsulta()
	{
		if(Session::has('no_hay_espacio'))
		{
			Session::forget('no_hay_espacio');
		}

		if(property_exists(Session::get('consulta'), 'fecha_flexible'))
		{
			$consulta = Session::get('consulta');

			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($consulta)
			    )
			);

			$result = AirConexion::consultar('busqueda_alternativa', $postdata);

			if($result !== false)
			{
				$resultado_motor = $result;

				$time_salida = strtotime($consulta->fecha_base_salida);

				$a_times_salida = array(
					strtotime('-3 days', $time_salida),
					strtotime('-2 days', $time_salida),
					strtotime('-1 day', $time_salida),
					$time_salida,
					strtotime('+1 day', $time_salida),
					strtotime('+2 days', $time_salida),
					strtotime('+3 days', $time_salida)
				);

				$a_fechas_salida = array(
					(object)array('time' => $a_times_salida[0], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_salida[0])),
					(object)array('time' => $a_times_salida[1], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_salida[1])),
					(object)array('time' => $a_times_salida[2], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_salida[2])),
					(object)array('time' => $a_times_salida[3], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_salida[3])),
					(object)array('time' => $a_times_salida[4], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_salida[4])),
					(object)array('time' => $a_times_salida[5], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_salida[5])),
					(object)array('time' => $a_times_salida[6], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_salida[6])),
				);

				if($consulta->tipo_busqueda == 2)
				{
					$time_retorno = strtotime($consulta->fecha_base_retorno);

					$a_times_retorno = array(
						strtotime('-3 days', $time_retorno),
						strtotime('-2 days', $time_retorno),
						strtotime('-1 day', $time_retorno),
						$time_retorno,
						strtotime('+1 day', $time_retorno),
						strtotime('+2 days', $time_retorno),
						strtotime('+3 days', $time_retorno)
					);

					$a_fechas_retorno = array(
						(object)array('time' => $a_times_retorno[0], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_retorno[0])),
						(object)array('time' => $a_times_retorno[1], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_retorno[1])),
						(object)array('time' => $a_times_retorno[2], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_retorno[2])),
						(object)array('time' => $a_times_retorno[3], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_retorno[3])),
						(object)array('time' => $a_times_retorno[4], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_retorno[4])),
						(object)array('time' => $a_times_retorno[5], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_retorno[5])),
						(object)array('time' => $a_times_retorno[6], 'descripcion' => Fecha::fechaEspaniolDiaFechaMes($a_times_retorno[6])),
					);
				}
				else
				{
					$a_fechas_retorno = array((object)array('time' => 0, 'descripcion' => (object)array('dia' => 'sólo', 'fecha' => 'ida')));
				}

				if($consulta->tipo_busqueda == 2)
					$resultado_motor->opciones = get_object_vars((object)$resultado_motor->opciones);

				// aqui guardamos la configuracion de fee
				$feeConfig = Config::get('app.fee');
				$feeMinimo = (array_key_exists('minimo', $feeConfig)) ? $feeConfig['minimo'] : 0;
				foreach ($resultado_motor->opciones as $key => $opciones) 
				{
					$resultado_motor->opciones[$key] = get_object_vars((object)$opciones);
					foreach($resultado_motor->opciones[$key] as $key2 => $optionAir)
					{
						// agregamos el fee del cliente
						$factorFee 											= array_key_exists($optionAir->carrier_valido, $feeConfig) ? 1 + (floatval($feeConfig[$option->carrier_valido])/100) : 1 + (floatval($feeConfig['default'])/100);
						if(ceil($optionAir->precio_mas_fee * $factorFee) - ceil($optionAir->precio_mas_fee) < $feeMinimo)
							$resultado_motor->opciones[$key][$key2]->precio_final = ceil($optionAir->precio_mas_fee + $feeMinimo);
						else
							$resultado_motor->opciones[$key][$key2]->precio_final = ceil($optionAir->precio_mas_fee * $factorFee);
					}
				}
			}
			else
			{
				$resultado_motor 		= new stdClass();
				$resultado_motor->error = 'Tiempo de espera superado';
				$a_fechas_retorno 		= array();
				$a_fechas_salida 		= array();
			}

			// guardamos en la sesión el resultado
				Session::put('resultado', $resultado_motor);

			return View::make('resultado', array('fecha_flexible' => true, 'fechas_salida' => $a_fechas_salida, 'fechas_retorno' => $a_fechas_retorno, 'consulta' => $consulta));

		}
		else if(!Session::has('resultado') || Session::has('buscar_vuelos'))
		{
			Session::forget('buscar_vuelos');

			$consulta = Session::get('consulta');

			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($consulta)
			    )
			);

			$result = AirConexion::consultar('busqueda', $postdata);
			$a_precios_resumen 	= array(); // precios ordenados por aerolineas

			if($result !== false)
			{
				$resultado_motor 	= $result;
				
				
				if(property_exists($resultado_motor, 'opciones'))
				{
					// aqui guardamos la configuracion de fee
						$feeConfig = Config::get('app.fee');
						$feeMinimo = (array_key_exists('minimo', $feeConfig)) ? $feeConfig['minimo'] : 0;
					// en este array guardaremos aquellos precios mas bajos por aerolinea y por escala
						$a_precios_x_aerolinea_escala 	= array();

					// hacemos algunas transformaciones adicionales
					foreach ($resultado_motor->opciones as $i => $option) 
					{
						$a_orig_dest 							= $option->orig_dest;
						// indice
						$resultado_motor->opciones[$i]->index 	= $i + 1;
						// agregamos el fee del cliente
						$factorFee 								= array_key_exists($option->carrier_valido, $feeConfig) ? 1 + (floatval($feeConfig[$option->carrier_valido])/100) : 1 + (floatval($feeConfig['default'])/100);
						$total_costo 							= $option->costo->monto + $option->costo->fee;
						$costo_avg 								= $option->costo->avg;
						$aplicarFeeMinimo 						= false;
						if(ceil($costo_avg * $factorFee) - ceil($costo_avg) < $feeMinimo)
						{
							$resultado_motor->opciones[$i]->costo->precio_final = ceil($total_costo + ($feeMinimo * $option->pasajeros));
							$aplicarFeeMinimo 									= true;
							$resultado_motor->opciones[$i]->monto_fee 			= $feeMinimo;
						}
						else
						{
							$resultado_motor->opciones[$i]->costo->precio_final = ceil($total_costo * $factorFee);
							$resultado_motor->opciones[$i]->porcentaje_fee 		= array_key_exists($option->carrier_valido, $feeConfig) ? floatval($feeConfig[$option->carrier_valido]) : floatval($feeConfig['default']);
						}

						foreach ($option->precios as $j => $price) 
						{
							// agregamos el fee del cliente
							if($aplicarFeeMinimo)
							{
								$resultado_motor->opciones[$i]->precios[$j]->precio_final_pax 		= ceil($price->total_pax + $feeMinimo);
								$resultado_motor->opciones[$i]->precios[$j]->precio_final_subtotal 	= ceil($price->subtotal + ($feeMinimo * $price->cant));
							}
							else
							{
								$resultado_motor->opciones[$i]->precios[$j]->precio_final_pax 		= ceil($price->total_pax * $factorFee);
								$resultado_motor->opciones[$i]->precios[$j]->precio_final_subtotal 	= ceil($price->subtotal * $factorFee);
							}
						}

						// aqui guardaremos la cantidad de escalas de esta opcion
							$cant_escalas = 0;

						foreach ($a_orig_dest as $j => $orig_dest) 
						{
							// revisamos el tramo
								//inicializamos la cadena de resumen del tramo
								$det_vuelos = '';

								$tramo 			= $orig_dest->tramo;
								// indice
								$resultado_motor->opciones[$i]->orig_dest[$j]->index = $j + 1;
								// transformamos la fecha a literal
								$fecha_sale 	= $tramo->fecha_sale;
								$resultado_motor->opciones[$i]->orig_dest[$j]->tramo->fecha_salida_literal = Fecha::fechaEspaniol__wdm($fecha_sale);
								// convertimos los minutos a horas y minutos
								$t_vuelo = $tramo->tiempo_vuelo;
								$resultado_motor->opciones[$i]->orig_dest[$j]->tramo->t_vuelo_literal = Fecha::tiempoDescripcion($t_vuelo);

								// revisamos los segmentos
								$segmentos 		= $orig_dest->segmentos;
								// almacenamos para usar despues la cantidad de escalas
								$cant_escalas 	+= count($segmentos) - 1;

								foreach ($segmentos as $k => $seg) 
								{
									//obtenemos el numero de vuelo mas aerolinea y aumentamos al detalle de vuelo
									$det_vuelos .= $seg->nro_vuelo . '_' . $seg->linea_op . ';';

									//indice
									$resultado_motor->opciones[$i]->orig_dest[$j]->segmentos[$k]->index = $k + 1;
									// convertimos los minutos a horas y minutos Xhr Ymin
									$t_vuelo = $seg->t_vuelo;
									$resultado_motor->opciones[$i]->orig_dest[$j]->segmentos[$k]->t_vuelo_literal = Fecha::tiempoDescripcion($t_vuelo);
								}

								$resultado_motor->opciones[$i]->orig_dest[$j]->tramo->detalle_vuelos = $det_vuelos;

								// verificamos que tipo de tramo es
								switch (Session::get('busqueda')->modo_vuelo) 
								{
									case 1:
										$resultado_motor->opciones[$i]->orig_dest[$j]->tramo->tipo_tramo = 'Sólo Ida';
										break;
									case 2:
										{
											if($j==0)
												$resultado_motor->opciones[$i]->orig_dest[$j]->tramo->tipo_tramo = 'Salida';
											else
												$resultado_motor->opciones[$i]->orig_dest[$j]->tramo->tipo_tramo = 'Retorno';
										}
										break;
									case 3:
										$resultado_motor->opciones[$i]->orig_dest[$j]->tramo->tipo_tramo = 'Tramo ' . ($j+1);
										break;
									default:
										
										break;
								}
						}

						// en esta seccion verificamos precios mas bajos por aerolinea y escalas
							$key_aer_esc = $option->carrier . '_' . $cant_escalas . '_' . str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $option->costo->moneda);
							/*echo '<h1>' . $key_aer_esc . '</h1><br/>';*/
							if(array_key_exists($key_aer_esc, $a_precios_x_aerolinea_escala))
							{
								if($a_precios_x_aerolinea_escala[$key_aer_esc] > $option->costo->precio_final)
									$a_precios_x_aerolinea_escala[$key_aer_esc] 	= $option->costo->precio_final;
							}
							else
								$a_precios_x_aerolinea_escala[$key_aer_esc] 	= $option->costo->precio_final;	
					}
					
					/*echo "PRECIOS X AEROLNEA Y ESCALA<pre>"; print_r($a_precios_x_aerolinea_escala); echo "</pre>";*/
					// ordenamos los precios por aerolineas
						asort($a_precios_x_aerolinea_escala);
						/* "PRECIOS X AEROLNEA Y ESCALA ORDENADO<pre>"; print_r($a_precios_x_aerolinea_escala); echo "</pre>";*/
						foreach ($a_precios_x_aerolinea_escala as $key => $monto) 
						{
							$a_aer_esc 	= explode('_', $key);
							$item 		= (object)array('aerolinea' => $a_aer_esc[0], 
														'escalas' =>  $a_aer_esc[1], 
														'moneda' =>  $a_aer_esc[2], 
														'precio' => $monto);
							$a_precios_resumen[] 	= $item;
						}

					// guardamos aparte lo que se mostrará
					$display 	= array_slice($resultado_motor->opciones, 0, 10, true);
					$max_pagina = ceil(count($resultado_motor->opciones) / 10);
				}
				else
				{
					$display 	= array();
					$max_pagina = 0;
				}
			}
			else
			{
				$resultado_motor 		= new stdClass();
				$resultado_motor->error = 'Tiempo de espera superado';
				$display 				= array();
				$max_pagina 			= 0;
			}

			// guardamos en la sesión el resultado
				Session::put('resultado', $resultado_motor);
				Session::put('resultado_resumen', $a_precios_resumen);
		}
		else
		{
			$display 	= array_slice(Session::get('resultado')->opciones, 0, 10, true);
			$max_pagina = ceil(count(Session::get('resultado')->opciones) / 10);
			Session::put('no_hay_espacio', 1);
		}
		
		// inicializamos los filtros
			Session::put('filtros', array());
		// eliminamos los filtrados por el momento
			if(Session::has('filtrados'))
				Session::forget('filtrados');

		return View::make('resultado', array('display' => $display, 'pagina' => 1, 'max_pagina' => $max_pagina, 'resumen_precios' => Session::get('resultado_resumen', array())));
		//return json_encode($consulta);
	}

	public function irPagina($pagina)
	{
		if(Session::has('filtrados'))
			$resultado_motor = Session::get('filtrados');
		else if(Session::has('resultado'))
			$resultado_motor = Session::get('resultado')->opciones;

		if(Session::has('resultado') || Session::has('filtrados'))
		{
			// guardamos aparte lo que se mostrará
			$max_pagina = ceil(count($resultado_motor) / 10);

			if($pagina == 1)
				$offset = 0;
			else
				$offset = ($pagina * 10) - 11;

			$display 	= array_slice($resultado_motor, $offset, 10, true);			
		}
		else
		{
			$resultado_motor 		= new stdClass();
			$resultado_motor->error = "Sesión expirada";
			Session::put('resultado', $resultado_motor);
		}

		return View::make('resultado', array('display' => $display, 'pagina' => $pagina, 'max_pagina' => $max_pagina, 'resumen_precios' => Session::get('resultado_resumen', array())));
	}

	public function filtrarPorTramos($seleccion)
	{
		// obtenemos el índice del tramo seleccionado
		$opcion 	= explode('_', $seleccion)[0] - 1;
		$tramo 		= explode('_', $seleccion)[1] - 1;
		$det_vuelos = Session::get('resultado')->opciones[$opcion]->orig_dest[$tramo]->tramo->detalle_vuelos;
		// agregamos el nuevo filtro
		$filtros 			= Session::get('filtros');
		$filtros[$tramo] 	= $det_vuelos;
		// reemplazamos el valor de los filtros
		Session::put('filtros', $filtros);

		//empezamos a aplicar el nuevo filtro a partir de los resultados, o de lo filtrado anteriormente
		if(Session::has('filtrados'))
			$filtrados 			= Session::get('filtrados');
		else
			$filtrados 			= Session::get('resultado')->opciones;
		// inicializamos el nuevo resultado del filtrado
		$filt_actualizado 	= array();

		// recorremos las opciones
		foreach ($filtrados as $i => $option) 
		{
			$a_orig_dest 			= $option->orig_dest;

			// comparamos si el detalle del vuelo es idéntico al filtro aplicado, si es cierto,
			// lo colocamos en los 'filtrados actualizado' con el índice original para no alterar resultados
			if($a_orig_dest[$tramo]->tramo->detalle_vuelos == $det_vuelos)
			{
				$filt_actualizado[$i] = $option;
			}
			
		}

		// reemplazamos el valor de los filtrados actualizados
		Session::put('filtrados', $filt_actualizado);

		// guardamos aparte lo que se mostrará
		$display 	= array_slice($filt_actualizado, 0, 10, true);
		$max_pagina = ceil(count($filt_actualizado) / 10);

		return View::make('resultado', array('display' => $display, 'pagina' => 1, 'max_pagina' => $max_pagina, 'resumen_precios' => Session::get('resultado_resumen', array())));
//return var_dump($filt_actualizado);
	}

	public function quitarFiltro($seleccion)
	{
		// obtenemos el índice del tramo seleccionado
		$tramo = explode('_', $seleccion)[1] - 1;
		// quitamos el filtro para este tramo
		$filtros 			= Session::get('filtros');
		unset($filtros[$tramo]);
		// reemplazamos el valor de los filtros
		Session::put('filtros', $filtros);

		if(count($filtros) > 0)
		{
			// aplicamos los filtros desde cero
			$filtrados 			= Session::get('resultado')->opciones;
			// inicializamos el nuevo resultado del filtrado
			$filt_actualizado 	= array();

			// recorremos las opciones
			foreach ($filtrados as $i => $option) 
			{
				$a_orig_dest 			= $option->orig_dest;

				// comparamos si el detalle del vuelo es idéntico al filtro aplicado, si es cierto,
				// lo colocamos en los 'filtrados actualizado' con el índice original para no alterar resultados
				$is_seleccionado = 1;
				foreach ($filtros as $j => $filtro) 
				{
					// si al menos uno es distinto, no forma parte de los filtrados
					if($a_orig_dest[$j]->tramo->detalle_vuelos != $filtro)
						$is_seleccionado = 0;
				}
				if($is_seleccionado == 1)
					$filt_actualizado[$i] = $option;
			}
		}
		else
		{
			// quitamos el valor de sesión de 'filtrados', porque ya no quedan filtros
			Session::forget('filtrados');
			// mostraremos todos los resultados
			$filt_actualizado = Session::get('resultado')->opciones;
		}

		// reemplazamos el valor de los filtrados actualizados
		Session::put('filtrados', $filt_actualizado);

		// guardamos aparte lo que se mostrará
		$display 	= array_slice($filt_actualizado, 0, 10, true);
		$max_pagina = ceil(count($filt_actualizado) / 10);

		return View::make('resultado', array('display' => $display, 'pagina' => 1, 'max_pagina' => $max_pagina, 'resumen_precios' => Session::get('resultado_resumen', array())));
	}

	public function filtrarPorAerolineaEscalas($aerolinea, $escalas)
	{
		// vaciamos los filtros
		Session::put('filtros', array());

		// los filtrados son todos los resultados
		$filtrados 			= Session::get('resultado')->opciones;
		// inicializamos el nuevo resultado del filtrado
		$filt_actualizado 	= array();

		// recorremos las opciones
		foreach ($filtrados as $i => $option) 
		{
			$a_orig_dest 			= $option->orig_dest;

			// comparamos si el carrier es el indicado en el filtro
			// lo colocamos en los 'filtrados actualizado' con el índice original para no alterar resultados
			if($option->carrier == $aerolinea)
			{
				// obtenemos la cantidad de escalas para comparar
				$cant_escalas = 0;
				foreach ($a_orig_dest as $j => $orig_dest) 
				{
					// almacenamos para usar despues la cantidad de escalas
					$cant_escalas 	+= count($orig_dest->segmentos) - 1;
				}

				// una vez determinado que es la aerolinea buscada comparamos las escalas
				if($cant_escalas == $escalas)
					$filt_actualizado[$i] = $option;
			}
			
		}

		// reemplazamos el valor de los filtrados actualizados
		Session::put('filtrados', $filt_actualizado);

		// guardamos aparte lo que se mostrará
		$display 	= array_slice($filt_actualizado, 0, 10, true);
		$max_pagina = ceil(count($filt_actualizado) / 10);

		return View::make('resultado', array('display' => $display, 'pagina' => 1, 'max_pagina' => $max_pagina, 'resumen_precios' => Session::get('resultado_resumen', array())));
	}

	public function quitarTodosLosFiltros()
	{
		// vaciamos los filtros
		Session::put('filtros', array());
	
		// quitamos el valor de sesión de 'filtrados', porque ya no quedan filtros
		Session::forget('filtrados');
		// mostraremos todos los resultados
		$filt_actualizado = Session::get('resultado')->opciones;

		// reemplazamos el valor de los filtrados actualizados
		Session::put('filtrados', $filt_actualizado);

		// guardamos aparte lo que se mostrará
		$display 	= array_slice($filt_actualizado, 0, 10, true);
		$max_pagina = ceil(count($filt_actualizado) / 10);

		return View::make('resultado', array('display' => $display, 'pagina' => 1, 'max_pagina' => $max_pagina, 'resumen_precios' => Session::get('resultado_resumen', array())));
	}

}
?>