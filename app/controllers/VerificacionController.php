<?php
/**
* 
*/
class VerificacionController extends BaseController
{
	
	public function confirmarDisponibilidad($seleccion)
	{
		// colocamos el número de resultado seleccionado
			Session::put('index_seleccionado', $seleccion);

		if(Session::has('resultado'))
		{
			// obtenemos el itinerario seleccionado
			$itinerario = Session::get('resultado')->opciones[$seleccion - 1];

				// modificaciones al objeto para mostrar la fecha en un formato
				foreach ($itinerario->orig_dest as $i => $tramo) 
				{
					foreach ($tramo->segmentos as $j => $segmento) 
					{
						$itinerario->orig_dest[$i]->segmentos[$j]->fecha_salida_literal = Fecha::fechaEspaniol_WdM($segmento->fecha_salida);
					}
				}

				$espacios = 0;

				// modificaciones al objeto para mostrar tipo de pasajero en singular y plural
				// contamos la cantidad de espacios necesarios para el itinerario seleccionado
				foreach ($itinerario->precios as $i => $precio) 
				{
					switch ($precio->pax) 
					{
						case 'ADT':
							{
								$itinerario->precios[$i]->tipo_pax 		= 'Adulto';
								$itinerario->precios[$i]->tipo_pax_p 	= 'Adultos';							
								$espacios += intval($precio->cant);
							}
							break;

						case 'PFA':
							{
								$itinerario->precios[$i]->tipo_pax 		= 'Adulto';
								$itinerario->precios[$i]->tipo_pax_p 	= 'Adultos';							
								$espacios += intval($precio->cant);
							}
							break;

						case 'CNN':
							{
								$itinerario->precios[$i]->tipo_pax 		= 'Menor';
								$itinerario->precios[$i]->tipo_pax_p 	= 'Menores';							
								$espacios += intval($precio->cant);
							}
							break;

						case 'INF':
							{
								$itinerario->precios[$i]->tipo_pax 		= 'Infante';
								$itinerario->precios[$i]->tipo_pax_p 	= 'Infantes';							
							}
							break;
						
						default:
							{
								$itinerario->precios[$i]->tipo_pax 		= '';
								$itinerario->precios[$i]->tipo_pax_p 	= '';							
							}
							break;
					}
				}

			// guardamos el itinerario seleccionado
			Session::put('seleccionado', $itinerario);

			/**
			*  CONSULTA PARA LA VERIFICACION
			**/
			$consulta 					= new stdClass();
			$consulta->itinerario 		= $itinerario;
			$consulta->espacios 		= $espacios;
			$consulta->consulta_origen 	= 'AV';

			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($consulta)
			    )
			);

			$response = AirConexion::consultar('verificacion', $postdata);

			/*****/
			if(property_exists($response, 'error'))
			{
				return $this->descartarSeleccion($seleccion, $response);
			}
			else if(property_exists($response, 'hay_espacio'))
			{
				if($response->hay_espacio)
				{
					if($response->cambio)
					{
						Session::put('seleccionado', $response->modificado);
					}

					Session::get('seleccionado')->cambio = $response->cambio;

					Session::put('info', $response->infoPax);
					Session::put('avisos', $response->avisos);
					
					return 'CORRECTO';
				}
				else
					return $this->descartarSeleccion($seleccion, $response);
			}
			else
			{
				return $this->descartarSeleccion($seleccion, $response);
			}
		}
		else
		{
			return $this->descartarSeleccion($seleccion, $response);
		}
	}

	function descartarSeleccion($seleccion, $vuelos)
	{
		$resultado_motor = Session::get('resultado');
		// eliminamos la opción descartada
		unset(Session::get('resultado')->opciones[$seleccion - 1]);
		
		// volvemos a mostrar los resultados
		$display 	= array_slice($resultado_motor->opciones, 0, 5, true);
		$max_pagina = ceil(count($resultado_motor->opciones) / 5);

		Session::put('filtros', array());

		// eliminamos los filtrados por el momento
			if(Session::has('filtrados'))
				Session::forget('filtrados');

		return View::make('resultado', array('display' => $display, 'pagina' => 1, 'max_pagina' => $max_pagina, 'sin_espacio' => true, 'vuelos' => $vuelos, 'resumen_precios' => Session::get('resultado_resumen')));
	}

}
?>