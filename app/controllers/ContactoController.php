<?php
/**
* 
*/
class ContactoController extends BaseController
{
	/*
	* Muestra la información de contacto
	*/
	public function mostrarContacto()
	{
		return View::make('contacto');
	}

    /*
    * Envia un mensaje de correo al cliente
    */
    public function enviarMail()
    {
        // enviamos el mail de la reserva al correo remitente con copia a dos correos
            $correo     = Config::get('app.mail');
            $data       = array('nombre' => Input::get('name'), 'email' => Input::get('email'), 'mensaje' => Input::get('message'));
            Mail::send('mails.mensaje_contacto', $data, function($message) use ($correo)
            {
                $message->to($correo)->subject('Mensaje desde la Página Web');
            });

        // enviamos confirmación
            $correo     = Input::get('email');
            $data       = array('nombre' => Input::get('name'));
            Mail::send('mails.mensaje_confirmacion', $data, function($message) use ($correo)
            {
                $message->to($correo)->subject('Confirmación ' . Config::get('app.empresa'));
            });            

        return 'CORRECTO';
    }
}

?>