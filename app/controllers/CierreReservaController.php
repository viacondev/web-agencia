<?php
/**
* 
*/
class CierreReservaController extends BaseController
{
	
	public function cerrarReserva()
	{
		// datos personales de los pasajeros

			$fee_aplicado 	= array();
			$datos_pax 		= array();
			$total_pax 		= 0;

			Session::forget('pnr');

			// aqui se guardarán los pasajeros de la BD para asociarlos al pnr que se creará
				$pasajerosBD = array();

			// inicializamos los datos de contacto
				$telefonos 	= array();
				$correos 	= array();

			$j 	= 1;		// este será el indicador de referencia de nombre
			foreach(Session::get('seleccionado')->precios as $precio)
			{
				$pasajeros = array();

				for($i = 1; $i <= $precio->cant; $i++)
				{
					$data 	= (object)array('nombre' => Input::get('nombre_' . $precio->pax . $i),
											'apellido' => Input::get('apellido_' . $precio->pax . $i));

					if(Input::has('documento_' . $precio->pax . $i))
					{
						$data->documento 	= Input::get('documento_' . $precio->pax . $i);
						$data->pais 		= Input::get('pais_' . $precio->pax . $i);
					}

					if(Input::has('nacimiento_' . $precio->pax . $i))
					{
						$a_nacimiento 			= explode('/', Input::get('nacimiento_' . $precio->pax . $i));
						$data->fecha_nacimiento = $a_nacimiento[2] . '-' . $a_nacimiento[1] . '-' . $a_nacimiento[0];
						$data->sexo 			= Input::get('sexo_' . $precio->pax . $i);
					}

					if(Input::get('ffly_' . $precio->pax . $i) != '')
					{
						$data->viajero_frecuente 	= (object)array('codigo' => Input::get('ffly_' . $precio->pax . $i),
																	'aerolinea' => Input::get('programa_aerolinea_' . $precio->pax . $i));
					}

					// si está en blanco no se ingresa el nit
					if(trim(Input::get('nit_' . $precio->pax . $i)) != '')
					{
						$data->nit 			= trim(Input::get('nit_' . $precio->pax . $i));
					}

					// si está en blanco, no se ingresa el teléfono
					if(trim(Input::get('telefono_' . $precio->pax . $i)) != '')
					{	
						$telefono 				= new stdClass;
						$telefono->numero 		= trim(Input::get('telefono_' . $precio->pax . $i));
						$telefono->cod_telefono = Input::get('cod_telefono_' . $precio->pax . $i);
						$telefono->tipo 		= 'H';
						$telefono->referencia 	= strval($j);	// hacemos referencia al pasajero que tiene el teléfono

						$telefonos[] 	= $telefono;
					}

					// si está en blanco no se ingresa el correo
					if(trim(Input::get('correo_' . $precio->pax . $i)) != '')
					{
						$correo 			= new stdClass;
						$correo->email 		= trim(Input::get('correo_' . $precio->pax . $i));
						$correo->texto 		= '';
						$correo->type 		= 'CC';
						$correo->referencia = strval($j);	// hacemos referencia al pasajero que tiene el correo

						$correos[] 	= $correo;
					}

					$pasajeros[] 	= $data;
					$j++;
				}

				$pax 	= (object)array('tipo_pax' 	=> $precio->pax,
										'cant' 		=> $precio->cant,
										'datos' 	=> $pasajeros);

				// preguntamos si el precio es tarifa privada para enviar el accounting code
				if(property_exists($precio, 'accountcode'))
					$pax->accountcode = $precio->accountcode;

				$datos_pax[] 	= $pax;

				if($precio->pax != 'INF')
					$total_pax 		+= intval($precio->cant);

				// guardamos el fee de la tarifa que se pretende guardar
				$fee_aplicado[] = $precio->total_pax - $precio->neto - $precio->imp;
			}

		// datos de contacto

			// telefono de contacto
				$telefono 				= new stdClass;
				$telefono->numero 		= Input::get('num_telefono');
				$telefono->cod_telefono = Input::get('cod_telefono');
				$telefono->tipo 		= 'H';
				$telefono->referencia 	= '1';

				$telefonos[] 	= $telefono;

				// verifica si se introdujo el teléfono de emergencia
				if(Input::get('num_telefono_em') != '')
				{
					$telf_em 				= new stdClass;
					$telf_em->numero 		= Input::get('num_telefono_em');
					$telf_em->cod_telefono 	= Input::get('cod_telefono_em');
					$telf_em->tipo 			= 'H';
					$telf_em->referencia 	= '1';		

					$telefonos[] 	= $telf_em;	
				}

				$telefonos[] = (object)array('numero' => Config::get('app.telfSabre'), 'cod_telefono' => Config::get('app.codCiudad'), 'tipo' => 'A', 'referencia' => '1',);

			// correo de contacto
				$correo 			= new stdClass;
				$correo->email 		= Config::get('app.mail');
				$correo->texto 		= '';
				$correo->type 		= 'CC';
				$correo->referencia = '1';

				$correos[] 	= $correo;

			$contacto 	= (object)array('telefonos' => $telefonos, 
										'correos' => $correos);

		// itinerario

			$segmentos 	= array();
			$ult_dest 	= '';
			$arunk 		= false;

			foreach (Session::get('seleccionado')->orig_dest as $tramo) 
			{
				foreach ($tramo->segmentos as $segmento) 
				{
					$seg 	= (object)array('origen' => $segmento->origen,
											'destino' => $segmento->destino,
											'nro_vuelo' => $segmento->nro_vuelo,
											'salida' => $segmento->fecha_salida . 'T' . $segmento->hora_salida . ':00',
											'clase_srv' => $segmento->clase_srv,
											't_equipo' => $segmento->t_equipo,
											'mark_air' => $segmento->mark_air,
											'linea_op' => $segmento->linea_op, 
											'marr_grp' => $segmento->marr_grp);

					if($ult_dest != $segmento->origen && $ult_dest != '')
					{
						$arunk = true;
					}

					$ult_dest 		= $segmento->destino;
					$segmentos[] 	= $seg;
				}
			}

		// datos para el cierre de la reserva
			$request 			= new stdClass;
			$request->segmentos = $segmentos;
			$request->carrier 	= Session::get('seleccionado')->carrier_valido;
			$request->pax 		= $datos_pax;
			$request->fee 		= $fee_aplicado;
			$request->contacto 	= $contacto;
			$request->total_pax = $total_pax;
			$request->moneda 	= Session::get('seleccionado')->costo->moneda;
			$request->rec_por 	= Config::get('app.abreviacion') . ' ' . substr(Config::get('app.empresa'), 0, 30) . ' PAG WEB';
			$request->remarks  	= array(Config::get('app.consolidador') . ' AGRADECE SU PREFERENCIA');
			$request->arunk 	= $arunk;
			
			/*if(Auth::user()->cliente->idcliente != 0)
				$request->cliente 	= Auth::user()->cliente->idcliente;*/		// se envia al cliente de viacon, que esta realizando la reserva si es distinto de cero
			
			$request->origen 	= Config::get('app.tipoFee'); // origen de la reserva, en este caso, boliviabooking 3

			// agregamos el codigo de usuario del counter
			$request->counter 	= Config::get('app.counter');

			// IMPORTANTE: se debe indicar que usuario es
			$request->usuario 	= Config::get('app.usuario');

			// IMPORTANTE: se debe indicar el porcentaje de fee aplicado
			if(property_exists(Session::get('seleccionado'), 'porcentaje_fee'))
				$request->porcentaje_fee   	= Session::get('seleccionado')->porcentaje_fee;
			elseif(property_exists(Session::get('seleccionado'), 'monto_fee'))
				$request->monto_fee   		= Session::get('seleccionado')->monto_fee;
			else
				$request->porcentaje_fee   	= 0;

			// configuramos el numero de queue al que se enviará la reserva si se genera el pnr
			$request->queue_number 		= Config::get('app.queueDestino');
			$request->queue_pseudo 		= Config::get('app.pcc');
			$request->cod_instruccion 	= Config::get('app.instruccion');

		$response = ConsolidadorConexion::consultar('cierre_reserva_from_external', array('dataair' => json_encode($request)));

		if(property_exists($response, 'codigo_pnr') && count($response->precios) > 0)
		{
			$data 			= array('codigo_pnr' => $response->codigo_pnr);
			$mail_receptor 	= Config::get('app.mail');
			Mail::send('mails.pnr_confirmacion', $data, function($message) use ($mail_receptor)
			{
			    $message->to($mail_receptor)->subject('Reserva de Boleto Aéreo');
			});

			return 'CORRECTO:' . $response->codigo_pnr;
		}
		else if(property_exists($response, 'error'))
		{
			if($response->error == 'Duplicidad')
				return 'DUPLICIDAD';
			else
			{
				Session::put('datos_pasajeros', Input::all());
				return 'DESCARTAR';	
			}
		}
		else
		{
			Session::put('datos_pasajeros', Input::all());
			return 'DESCARTAR';
		}
	}

	function descartarSeleccion()
	{
		if(Session::has('index_seleccionado'))
			$seleccion = Session::get('index_seleccionado');
		else
			$seleccion = 1;

		$resultado_motor = Session::get('resultado');
		// eliminamos la opción descartada
		unset(Session::get('resultado')->opciones[$seleccion - 1]);
		
		// volvemos a mostrar los resultados
		$display 	= array_slice($resultado_motor->opciones, 0, 5, true);
		$max_pagina = ceil(count($resultado_motor->opciones) / 5);

		Session::put('filtros', array());

		// eliminamos los filtrados por el momento
			if(Session::has('filtrados'))
				Session::forget('filtrados');

		// Obtenemos precios anteriores del Motor de Reservas
			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode(Session::get('consulta'))
			    )
			);

			$result = AirConexion::consultar('cached', $postdata);

			// aqui guardamos la configuracion de fee
				$feeConfig = Config::get('app.fee');
			foreach ($result as $i => $tramo) 
			{
				$result[$i]->time = Fecha::diferenciaHorasMinutos($tramo->busqueda->date);
				$a_fechas 	= array();
				foreach ($tramo->segmentos as $j => $segmento) 
				{
					$a_fechas[] = Fecha::fechaEspaniolDM($segmento->fecha);
				}
				$result[$i]->fechas = implode(' - ', $a_fechas);
				// agregamos el fee del cliente
				$factorFee 					= array_key_exists($tramo->carrier, $feeConfig) ? 1 + (floatval($feeConfig[$tramo->carrier])/100) : 1 + (floatval($feeConfig['default'])/100);
				$result[$i]->precio_final 	= ceil($tramo->precio * $factorFee);
			}

			$monedas 	= array('BOB' => 'Bs', 'USD' => 'Usd');

			$busqueda 	= Session::get('busqueda');

		return View::make('resultados', array('busqueda' => $busqueda, 'precios_anteriores' => $result, 'monedas' => $monedas));
	}
	
}
?>