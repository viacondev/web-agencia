<?php
/**
* 
*/
class InicioController extends BaseController
{
	
	public function indexVuelos()
	{
		//Session::flush();

		$postdata = http_build_query(
		    array(
		        'consulta_origen' => 'AV'
		    )
		);

		$results = AirConexion::consultar('anteriores', $postdata);

//		$results 	= json_decode($output);

		// aqui guardamos la configuracion de fee
			$feeConfig = Config::get('app.fee');

		foreach ($results as $i => $result) 
		{
			$a_fechas = array();
			foreach ($result->segmentos as $j => $segmento) 
			{
				$a_fechas[] = Fecha::fechaEspaniolDMSinSlash($results[$i]->segmentos[$j]->fecha);
			}
			$results[$i]->fechas = implode(' - ', $a_fechas);
			// agregamos el fee del cliente
				$factorFee 					= array_key_exists($result->carrier, $feeConfig) ? 1 + (floatval($feeConfig[$result->carrier])/100) : 1 + (floatval($feeConfig['default'])/100);
				$results[$i]->precio_final 	= ceil($result->precio * $factorFee);
		}

		// obtenemos las monedas para mostrar en la página con listas de destinos buscados en la web
		$monedas = array('BOB' => 'Bs', 'USD' => 'Usd');

		return View::make('vuelos', array('solicitados' => $results, 'monedas' => $monedas));
	}
}
?>