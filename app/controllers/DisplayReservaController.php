<?php
/**
* 
*/
class DisplayReservaController extends BaseController
{
	
	public function mostrarReserva()
	{
		if(Session::has('pnr'))
		{
			$pnr = Session::get('pnr');

			if(!property_exists($pnr, 'parseado'))
			{
				if(property_exists($pnr, 'itinerario'))
				{
					// transformamos las fechas y tiempos de vuelo
					foreach ($pnr->itinerario as $i => $segmento) 
					{
						$pnr->itinerario[$i]->fecha_literal 	= Fecha::fechaEspaniol_WdM($segmento->fecha_salida);
						$pnr->itinerario[$i]->t_vuelo_literal 	= Fecha::tiempoDescripcionConPunto($segmento->t_vuelo);
					}	
				}
				
				if(property_exists($pnr, 'precios'))
				{
					// transformamos el tipo de pasajero y moneda
					foreach($pnr->precios as $i => $precio)
					{
						$pnr->precios[$i]->n_moneda 	= str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $precio->moneda);
						$pnr->precios[$i]->n_tipo_pax_s = str_replace(array('ADT', 'PFA', 'CNN', 'INF'), array('Adulto', 'Adulto', 'Menor', 'Infante'), $precio->tipo_pax);
						$pnr->precios[$i]->n_tipo_pax_p = str_replace(array('ADT', 'PFA', 'CNN', 'INF'), array('Adultos', 'Adultos', 'Menores', 'Infantes'), $precio->tipo_pax);
					}	
				}
				
				/* definir tipo de cabina desde la consulta */
				$pnr->cabina = Session::get('busqueda')->cabina;
				//$pnr->cabina = 'Turista';

				if(property_exists($pnr, 'totales'))
				{
					// transformamos moneda del monto total
					foreach ($pnr->totales as $key => $total) 
					{
						$pnr->totales->$key->n_moneda 	= str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $key);
					}	
				}
				
				if(property_exists($pnr, 'time_limit_oficina'))
				{
					// convertimos el time_limit
					$pnr->time_limit_oficina_descripcion 	= Fecha::fechaEspaniol_WdMHi($pnr->time_limit_oficina);
				}
				
				// indicamos que ya fue parseado el pnr
				$pnr->parseado = true;
			}

			Session::flush();
			Session::put('pnr', $pnr);

			// enviamos el mail de la reserva al correo remitente con copia a dos correos
				/*$data 	= array();
				$correo = $pnr->correo;
				Mail::send('mails.pnr', $data, function($message) use ($correo)
				{
				    $message->to($correo)->cc(array('lizeth.arteaga@viacontours.com' => 'Lizeth', 'mariarene@viacontours.com' => 'MariaRene'))->subject('Reserva de Boleto Aéreo');
				});*/

			$config = Auth::user()->cliente->configuracion;

			return View::make('vista_reserva', array('config' => $config));
		}
		else
			return Redirect::to('aereo');
	}

	public function mostrarPnrPorCodigo()
	{
		Session::flush();
		$codigo 	= Input::get('codigo_pnr');

		$consulta 	= (object)array('codigo_pnr' => $codigo);

		// Obtenemos pnr del Motor de Reservas
			$postdata = http_build_query(
			    array(
			        'consulta' => json_encode($consulta)
			    )
			);

			$response = AirConexion::consultar('obtener', $postdata);

			if(property_exists($response, 'codigo_pnr'))
			{
				// indicador de una vista normal, y no una vista luego de cerrar una reserva
				$response->get = true;
				Session::put('pnr', $response);
			}
			else
			{
				Session::put('error', $codigo);
			}

		// transformamos algunos datos para la vista de la reserva
			if(Session::has('pnr'))
			{
				$pnr = Session::get('pnr');

				if(!property_exists($pnr, 'parseado'))
				{
					if(property_exists($pnr, 'itinerario'))
					{
						// transformamos las fechas y tiempos de vuelo
						foreach ($pnr->itinerario as $i => $segmento) 
						{
							$pnr->itinerario[$i]->fecha_literal 	= Fecha::fechaEspaniol_WdM($segmento->fecha_salida);
							$pnr->itinerario[$i]->t_vuelo_literal 	= Fecha::tiempoDescripcionConPunto($segmento->t_vuelo);
						}	
					}
					
					if(property_exists($pnr, 'precios'))
					{
						// transformamos el tipo de pasajero y moneda
						foreach($pnr->precios as $i => $precio)
						{
							$pnr->precios[$i]->n_moneda 	= str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $precio->moneda);
							$pnr->precios[$i]->n_tipo_pax_s = str_replace(array('ADT', 'PFA', 'CNN', 'INF'), array('Adulto', 'Adulto', 'Menor', 'Infante'), $precio->tipo_pax);
							$pnr->precios[$i]->n_tipo_pax_p = str_replace(array('ADT', 'PFA', 'CNN', 'INF'), array('Adultos', 'Adultos', 'Menores', 'Infantes'), $precio->tipo_pax);
						}
					}

					/* definir tipo de cabina desde la consulta */
					$pnr->cabina = '';
					
					if(property_exists($pnr, 'totales'))
					{
						// transformamos moneda del monto total
						foreach ($pnr->totales as $key => $total) 
						{
							$pnr->totales->$key->n_moneda 	= str_replace(array('USD', 'BOB'), array('Usd', 'Bs'), $key);
						}
					}

					if(property_exists($pnr, 'time_limit_oficina'))
					{
						// convertimos el time_limit
						$pnr->time_limit_oficina_descripcion 	= Fecha::fechaEspaniol_WdMHi($pnr->time_limit_oficina);
					}
					
					if(property_exists($pnr, 'pasajeros'))
					{
						// convertimos a array los asientos de cada pasajero
						foreach($pnr->pasajeros as $key => $pax)
						{
							$pnr->pasajeros[$key]->asientos = get_object_vars((object)$pax->asientos);
						}
					}

					// indicamos que ya fue parseado el pnr
					$pnr->parseado = true;
				}

				Session::flush();
				Session::put('pnr', $pnr);
			}

		return View::make('vista_reserva');
	}
}
?>