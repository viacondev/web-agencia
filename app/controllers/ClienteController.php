<?php
/**
* 
*/
class ClienteController extends BaseController
{
	
	public function cargarCliente()
	{
		$ciudades	= Ciudad::orderBy('nombre','asc')->get();
		return View::make('crear_clientes',array('ciudades' => $ciudades));
	}

	public function createCliente()
	{
		$cliente 	= Cliente::create(array('nombre' 					=> Input::get('nombre'), 
											'mails' 					=> Input::get('mail'), 
											'direccion' 				=> Input::get('direccion'), 
											'telefonos' 				=> Input::get('telefono'), 
											'autorizado_emision_aereo' 	=> Input::get('autorizado_emision_aereo'),
											'idcliente' 				=> Input::get('idcliente'),
											'idciudad' 					=> Input::get('ciudad')));

		$air_config = AirConfig::create(array('porcentaje_fee' 	=> 0, 
												'mostrar_fee' 	=> 1, 
												'logo' 			=> 'logo_' . $cliente->id . '.png', 
												'idcliente' 	=> $cliente->id));

		return Redirect::to('mostrarClientes');
	}

	public function mostrarClientes()
	{
		$clientes=Cliente::orderBy('id','desc')->take(20)->get();
		return View::make('mis_clientes',array('clientes' => $clientes));
	}

	public function updateCliente()
	{
		$cliente 			= Cliente::find(Input::get('id'));

		$cliente->nombre 					= Input::get('nombre');
		$cliente->mails  					= Input::get('mail');
		$cliente->direccion					= Input::get('direccion');
		$cliente->telefonos					= Input::get('telefono');
		$cliente->autorizado_emision_aereo 	= Input::get('autorizado_emision_aereo');
		$cliente->idciudad					= Input::get('ciudad');
		$cliente->idcliente					= Input::get('idcliente');
		
		$cliente->save();
		
		return Redirect::to('mostrarClientes');
	}

	public function editClientes($id){
		$cliente=Cliente::find($id);
		$ciudades=Ciudad::orderBy('nombre','asc')->get();
	   return View::make('editar_clientes',array('cliente' => $cliente,'ciudades' =>$ciudades));
	}

	public function verCliente($id) 
	{
		// obtenemos toda la información respecto del cliente
		$cliente 	= Cliente::find($id);

		return View::make('perfil_cliente', array('cliente' => $cliente));
	}

	/*
	* Inserta un nuevo usuario desde el perfil del cliente a través de su identificador
	*/
	public function crearUsuario()
	{
		if(!Usuario::where('nombre', '=', trim(Input::get('nombre')))->first())
		{
			Usuario::create(array('nombre' 				=> Input::get('nombre'), 
									'password' 			=> Hash::make(Input::get('password')), 
									'nombre_completo' 	=> Input::get('nombre_completo'), 
									'mails' 			=> Input::get('mails'), 
									'telefonos' 		=> Input::get('telefonos'), 
									'rol' 				=> Input::get('rol'), 
									'idcliente' 		=> Input::get('idcliente')));	
		}

		return Redirect::to('ver_cliente/' . Input::get('idcliente'));
	}

	/*
	* Muestra el formulario de edición de datos de un usuario
	*/
	public function editarUsuario($id)
	{
		$usuario = Usuario::find($id);

		return View::make('editar_usuario', array('usuario' => $usuario));
	}

	/*
	* Actualiza los datos del usuario editado
	*/
	public function updateUsuario()
	{
		$cliente 	= Cliente::find(Input::get('idcliente'));

		$usuario 	= Usuario::find(Input::get('id'));

		$usuario->nombre_completo 	= Input::get('nombre_completo');
		$usuario->mails 			= Input::get('mail');
		$usuario->telefonos 		= Input::get('telefono');
		$usuario->rol 				= Input::get('rol');
		$usuario->idcliente 		= Input::get('idcliente');
		$usuario->save();

		return View::make('perfil_cliente', array('cliente' => $cliente));
	}

	/*
	* Cambia la contraseña de un usuario, esto lo hace a través del perfil de cliente
	*/
	public function updatePassword()
	{
		$usuario = Usuario::find(Input::get('idusuario'));

		$usuario->password 	= Hash::make(Input::get('password'));
		$usuario->save();

		return Redirect::to('ver_cliente/' . $usuario->cliente->id);
	}

	/*
	* Busca clientes según el nombre
	*/
	public function buscarCliente()
	{
		$clientes = Cliente::where('nombre', 'LIKE', '%' . Input::get('txt_nombre') . '%')->orderBy('id','desc')->take(20)->get();

		return View::make('mis_clientes',array('clientes' => $clientes));
	}

}
?>