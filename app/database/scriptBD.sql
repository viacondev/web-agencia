/*
* Tablas generales de boliviabooking
*/

	CREATE TABLE bb_pais
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`codigo_iata`	VARCHAR(2),
		`nombre`		VARCHAR(30),
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL
	) ENGINE=InnoDB;

	CREATE TABLE bb_ciudad
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`codigo_iata`	VARCHAR(3),
		`nombre`		VARCHAR(30),
		`codigo_telf`	VARCHAR(5),
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL,
		-- FORANEAS
		`idpais` 		INT(11),
		FOREIGN KEY(`idpais`) REFERENCES bb_pais(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE bb_aerolinea
	(
		-- PRIMARY KEY
		`id` 				INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`codigo_iata`		VARCHAR(2),
		`nombre`			VARCHAR(50),
		`programa_millas`	VARCHAR(50),
		`estado`			TINYINT(1) COMMENT '1:Habilitado 0:Inactivo',
		`created_at` 		DATETIME,
		`updated_at` 		TIMESTAMP NOT NULL
	) ENGINE=InnoDB;

	CREATE TABLE bb_cliente
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`nombre`		VARCHAR(50),
		`mails`			TEXT,
		`direccion`		TEXT,
		`telefonos`		TEXT,
		`idcliente`		INT(11), 	-- es el identificador de cliente de viacon
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL,
		-- FORANEAS
		`idciudad` 		INT(11),
		FOREIGN KEY(`idciudad`) REFERENCES bb_ciudad(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE bb_usuario
	(
		-- PRIMARY KEY
		`id` 				INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`nombre`			VARCHAR(10),
		`password`			VARCHAR(50),
		`nombre_completo`	VARCHAR(200),
		`mails`				TEXT,
		`telefonos`			TEXT,
		`rol`				TINYINT(1),
		`created_at` 		DATETIME,
		`updated_at` 		TIMESTAMP NOT NULL,
		-- FORANEAS
		`idcliente` 		INT(11),
		FOREIGN KEY(`idcliente`) REFERENCES bb_cliente(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE bb_pasajero
	(
		-- PRIMARY KEY
		`id` 				INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`nombre`			VARCHAR(30),
		`primer_apellido`	VARCHAR(30),
		`segundo_apellido`	VARCHAR(30),
		`fecha_nacimiento`	DATE,
		`sexo`				VARCHAR(1),
		`nit` 				VARCHAR(60),
		`mails`				TEXT,
		`direccion`			TEXT,
		`created_at` 		DATETIME,
		`updated_at` 		TIMESTAMP NOT NULL,
		-- FORANEAS
		`idcliente`			INT(11),
		FOREIGN KEY(`idcliente`) REFERENCES bb_cliente(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE bb_frequent_fly
	(
		-- PRIMARY KEY
		`id` 					INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`codigo`				VARCHAR(20),
		`nombre_registrado`		VARCHAR(20),
		`apellido_registrado`	VARCHAR(20),
		`created_at` 			DATETIME,
		`updated_at` 			TIMESTAMP NOT NULL,
		-- FORANEAS
		`idpasajero` 			INT(11),
		FOREIGN KEY(`idpasajero`) REFERENCES bb_pasajero(`id`),
		`idaerolinea`			INT(11),
		FOREIGN KEY(`idaerolinea`) REFERENCES bb_aerolinea(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE bb_telefono
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`numero`		VARCHAR(20),
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL,
		-- FORANEAS
		`idciudad` 		INT(11),
		FOREIGN KEY(`idciudad`) REFERENCES bb_ciudad(`id`),
		`idpasajero` 	INT(11),
		FOREIGN KEY(`idpasajero`) REFERENCES bb_pasajero(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE bb_documento
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`numero`		VARCHAR(20),
		`tipo_doc`		VARCHAR(2),
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL,
		-- FORANEAS
		`idpasajero` 	INT(11),
		FOREIGN KEY(`idpasajero`) REFERENCES bb_pasajero(`id`),
		`idpais` 		INT(11),
		FOREIGN KEY(`idpais`) REFERENCES bb_pais(`id`)
	) ENGINE=InnoDB;


/*
* Tablas exclusivas de aereo en boliviabooking
*/

	CREATE TABLE air_pnr
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`codigo`		VARCHAR(6),
		`fecha`			DATETIME,
		`ruta` 			VARCHAR(100),
		`estado`		TINYINT(1) COMMENT '1:Reserva  2:Emitido 3:Anulado',
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL,
		-- FORANEAS
		`idusuario` 	INT(11),
		FOREIGN KEY(`idusuario`) REFERENCES bb_usuario(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE air_pnr_pasajero
	(
		-- PRIMARY KEY Y FORANEAS
		`idpnr` 		INT(11),
		`idpasajero` 	INT(11),
		PRIMARY KEY(`idpnr`, `idpasajero`),
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL,
		FOREIGN KEY(`idpnr`) REFERENCES air_pnr(`id`),
		FOREIGN KEY(`idpasajero`) REFERENCES bb_pasajero(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE air_precio
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`precio`		FLOAT,
		`moneda`		VARCHAR(3),
		`tipo_pax`		VARCHAR(3),
		`cantidad_pax`	INT(11),
		`time_limit`	DATETIME,
		`fee` 			FLOAT,
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL,
		-- FORANEAS
		`idpnr` 		INT(11),
		FOREIGN KEY(`idpnr`) REFERENCES air_pnr(`id`),
		`idaerolinea` 	INT(11),
		FOREIGN KEY(`idaerolinea`) REFERENCES bb_aerolinea(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE air_fee
	(
		-- PRIMARY KEY
		`id` 				INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`porcentaje_fee`	FLOAT,
		`created_at` 		DATETIME,
		`updated_at` 		TIMESTAMP NOT NULL,
		-- FORANEAS
		`idcliente` 		INT(11),
		FOREIGN KEY(`idcliente`) REFERENCES bb_cliente(`id`),
		`idaerolinea`		INT(11),
		FOREIGN KEY(`idaerolinea`) REFERENCES bb_aerolinea(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE air_emision
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`pendientes` 	TINYINT(1),
		`estado` 		TINYINT(1) COMMENT '0: EnCola   1: Procesado',	
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL,
		-- FORANEAS
		`idpnr` 		INT(11),
		`idusuario` 	INT(11),
		FOREIGN KEY(`idpnr`) REFERENCES air_pnr(`id`),
		FOREIGN KEY(`idusuario`) REFERENCES bb_usuario(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE air_config
	(
		-- PRIMARY KEY
		`id` 				INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`porcentaje_fee`	FLOAT,
		`mostrar_fee`		TINYINT(1),
		`logo`				VARCHAR(20),
		`created_at` 		DATETIME,
		`updated_at` 		TIMESTAMP NOT NULL
		`idcliente`			INT(11),
		FOREIGN KEY(`idcliente`) REFERENCES bb_cliente(`id`)
	) ENGINE=InnoDB;

	CREATE TABLE IF NOT EXISTS `bb_error_log` 
	(
		-- PRIMARY KEY
		`id` int(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
	  	`mensaje` text NOT NULL,
	  	`archivo` text NOT NULL,
	  	`linea` text NOT NULL,
	  	`pila` text NOT NULL,
	  	`created_at` datetime NOT NULL,
	  	`updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
	) ENGINE=InnoDB;

	ALTER TABLE `bb_cliente` ADD `autorizado_emision_aereo` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0: NoAutorizado 1: Autorizado' AFTER `telefonos`;

/*************************************************
* Tabla plantilla
* 
**************************************************/
	CREATE TABLE tabla
	(
		-- PRIMARY KEY
		`id` 			INT(11) AUTO_INCREMENT PRIMARY KEY,
		-- ATRIBUTOS
		`idpais`		INT(11),
		`created_at` 	DATETIME,
		`updated_at` 	TIMESTAMP NOT NULL
	) ENGINE=InnoDB;
