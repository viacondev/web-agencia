<?php
/**
* 
*/
class Pasajero extends Eloquent
{

	protected $table 	= 'bb_pasajero';
	protected $fillable = array('nombre', 'primer_apellido', 'segundo_apellido', 'fecha_nacimiento', 'sexo', 'nit', 'mails', 'direccion', 'idcliente');

	public function telefonos()
	{
		return $this->hasMany('Telefono', 'idpasajero');
	}

	public function frequentFly()
	{
		return $this->hasMany('FrequentFly', 'idpasajero');
	}

	public function documentos()
	{
		return $this->hasMany('Documento', 'idpasajero');
	}

	public function pnrs()
	{
		return $this->belongsToMany('Pnr', 'air_pnr_pasajero', 'idpasajero', 'idpnr');
	}

}
?>