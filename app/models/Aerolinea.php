<?php
/**
* 
*/
class Aerolinea extends Eloquent
{

	protected $table 	= 'bb_aerolinea';
	protected $fillable = array('codigo_iata', 'nombre', 'programa_millas', 'fee_aplicado', 'estado');

}
?>