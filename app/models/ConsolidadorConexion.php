<?php
/**
* 
*/
class ConsolidadorConexion
{
    static function consultar($metodo, $data)
    {//return json_decode('{"state":"success","localizer":"258-341132","book_number":"R00265","pax":"Nomei Rojas","price":"218.71","currency":"USD"}');
        // real
        //$url    = 'http://viacontours.com/viaconapp-hotelesviacon/viaconapp-rest/api/hotel/' . $metodo;
        // prueba
        $url    = Config::get('app.airUrl') . $metodo;

        $curl   = curl_init();
        
        //$data = '{"ocupacion":{"requestType":0,"destCode":"BOG","destType":"SIMPLE","checkin":"20160316","checkout":"20160321","roomCount":1,"page":1,"orderPrice":"ASC","providers":[1,23],"hotelName":"","adultCount_1":2,"childCount_1":0,"totalChildCount":0}}';

        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json'                                                                       
        )); 

        $result = curl_exec($curl);

        curl_close($curl);

        return json_decode($result);
    }
}

?>