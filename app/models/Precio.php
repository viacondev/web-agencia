<?php
/**
* 
*/
class Precio extends Eloquent
{

	protected $table 	= 'air_precio';
	protected $fillable = array('precio', 'moneda', 'tipo_pax', 'cantidad_pax', 'time_limit', 'fee', 'idpnr', 'idaerolinea');

	public function aerolinea()
	{
		return $this->belongsTo('Aerolinea', 'idaerolinea');
	}

}
?>