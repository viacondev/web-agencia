<?php
/**
* 
*/
class Documento extends Eloquent
{

	protected $table 	= 'bb_documento';
	protected $fillable = array('numero', 'tipo_doc', 'idpasajero', 'idpais');

	public function pais()
	{
		return $this->belongsTo('Pais', 'idpais');
	}

}
?>