<?php
/**
* 
*/
class Promocion extends Eloquent
{

	protected $table 	= 'pqt_publicacion';
	protected $fillable = array('tipo_publicacion', 'titulo', 'fecha_caducidad');

	public static function obtenerPromocionesRecientes()
	{
		return Promocion::where('tipo_publicacion', '=', 'O')->where('fecha_caducidad', '>', date('Y-m-d H:i:s'))->where('estado', '=', 1)->take(4)->get(array('idpublicacion', 'titulo', 'precio_desde', 'resaltar'));
	}

}
?>