<?php
/**
* 
*/
class FrequentFly extends Eloquent
{

	protected $table 	= 'bb_frequent_fly';
	protected $fillable = array('codigo', 'nombre_registrado', 'apellido_registrado', 'idpasajero', 'idaerolinea');

	public function aerolinea()
	{
		return $this->belongsTo('Aerolinea', 'idaerolinea');
	}

}
?>