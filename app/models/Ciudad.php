<?php
/**
* 
*/
class Ciudad extends Eloquent
{

	protected $table 	= 'bb_ciudad';
	protected $fillable = array('codigo_iata', 'nombre', 'codigo_telf', 'idpais');

	public function pais()
	{
		return $this->belongsTo('Pais', 'idpais');
	}

}
?>