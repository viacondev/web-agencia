<?php
/**
* 
*/
use Illuminate\Auth\UserInterface;

class Usuario extends Eloquent implements UserInterface
{

	protected $table 	= 'bb_usuario';
	protected $fillable = array('nombre', 'password', 'nombre_completo', 'mails', 'telefonos', 'rol', 'idcliente');

	// metodos que se deben implementar por la interfaz
        // este metodo se debe implementar por la interfaz
        public function getAuthIdentifier()
        {
            return $this->getKey();
        }
        
        //este metodo se debe implementar por la interfaz
        // y sirve para obtener la clave al momento de validar el inicio de sesión 
        public function getAuthPassword()
        {
            return $this->password;
        }

        public function getRememberToken()
        {
            return $this->remember_token;
        }

        public function setRememberToken($value)
        {
            $this->remember_token = $value;
        }

        public function getRememberTokenName()
        {
            return 'remember_token';
        }

    public function cliente()
    {
    	return $this->belongsTo('Cliente', 'idcliente');
    }

    public function pnrs()
    {
    	return $this->hasMany('Pnr', 'idusuario');
    }

}
?>