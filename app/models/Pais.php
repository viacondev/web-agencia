<?php
/**
* 
*/
class Pais extends Eloquent
{

	protected $table 	= 'bb_pais';
	protected $fillable = array('codigo_iata', 'nombre');

	public function ciudades()
	{
		return $this->hasMany('Ciudad', 'idpais');
	}

}
?>