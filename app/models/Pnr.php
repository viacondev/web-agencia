<?php
/**
* 
*/
class Pnr extends Eloquent
{

	protected $table 	= 'air_pnr';
	protected $fillable = array('codigo', 'fecha', 'ruta', 'estado', 'idusuario');

	public function usuario()
	{
		return $this->belongsTo('Usuario', 'idusuario');
	}

	public function pasajeros()
	{
		return $this->belongsToMany('Pasajero', 'air_pnr_pasajero', 'idpnr', 'idpasajero');
	}

	public function precios()
	{
		return $this->hasMany('Precio', 'idpnr');
	}

	public function emisiones()
	{
		return $this->hasMany('Emision', 'idpnr');
	}

	public function emisionesPendientes()
	{
		return $this->emisiones()->where('estado', '=', 0)->get();
	}

}

?>