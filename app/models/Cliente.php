<?php
/**
* 
*/
class Cliente extends Eloquent
{

	protected $table 	= 'bb_cliente';
	protected $fillable = array('nombre', 'mails', 'direccion', 'telefonos', 'autorizado_emision_aereo', 'idcliente', 'idciudad');

	public function ciudad()
	{
		return $this->belongsTo('Ciudad', 'idciudad');
	}

	public function usuarios()
	{
		return $this->hasMany('Usuario', 'idcliente');
	}

	public function pnrsPorCliente($estado, $busqueda = '')
	{
		// busca los usuarios que pertenecen a este cliente
		$a_usuarios 	= array();
		foreach ($this->usuarios as $user) 
		{
			$a_usuarios[] = $user->id;
		}

		// busca aquellos pnr que tienen apellido o nombre de pasajero parecido al ingresado
		$a_pnrs 	= array();
		if($busqueda != '')
		{
			$ids_pnr 	= DB::select("SELECT AP.idpnr FROM bb_pasajero P, air_pnr_pasajero AP Where AP.idpasajero=P.id AND (P.nombre LIKE '%" . $busqueda . "%' OR P.primer_apellido LIKE '%" . $busqueda . "%' OR P.segundo_apellido LIKE '%" . $busqueda . "%')");
			foreach ($ids_pnr as $id_pnr) 
			{
				$a_pnrs[] = $id_pnr->idpnr;
			}

			$pnrs 	= Pnr::whereIn('idusuario', $a_usuarios)->whereIn('id', $a_pnrs)->where('estado', '=', $estado)->orderBy('created_at', 'desc')->take(20)->get();
		}
		else
			$pnrs 	= Pnr::whereIn('idusuario', $a_usuarios)->where('estado', '=', $estado)->orderBy('created_at', 'desc')->take(20)->get();

		foreach($pnrs as $pnr)
		{
			$pnr->pasajeros;

			$a_totales	= array();
			foreach($pnr->precios as $precio)
			{
				if(array_key_exists($precio->moneda, $a_totales))
					$a_totales[$precio->moneda] 	+= $precio->precio + $precio->fee;
				else
					$a_totales[$precio->moneda] 	= $precio->precio + $precio->fee;
			}
			$pnr->totales 	= $a_totales;

			$pnr->usuario;
		}

		return $pnrs;
	}

	public function pasajeros()
	{
		return $this->hasMany('Pasajero', 'idcliente');
	}

	public function configuracion()
	{
		return $this->hasOne('AirConfig', 'idcliente');
	}

	public function feesAereo()
	{
		return $this->hasMany('AirFee', 'idcliente');
	}

}
?>