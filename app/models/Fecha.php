<?php
/**
* 
*/
class Fecha
{
	
	public static function fechaEspaniol_dM($fecha)
	{
		$a_fecha 	= explode('T', $fecha);

		$a_meses 	= array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

		$fecha 		= strtotime($a_fecha[0]);

		return date('d', $fecha) . ' de ' . $a_meses[date('m', $fecha)-1];
	}

	public static function fechaEspaniol_dMY($fecha)
	{
		$a_meses 	= array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

		$fecha 		= strtotime($fecha);

		return date('d', $fecha) . ' de ' . $a_meses[date('m', $fecha)-1] . ' de ' . date('Y', $fecha);
	}

	public static function fechaEspaniol_WdM($fecha)
	{
		$a_dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
		$a_meses 	= array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

		$f = strtotime($fecha);

		return $a_dias[date('w', $f)] . ', ' . date('d', $f) . ' de ' . $a_meses[date('m', $f)-1];
	}

	public static function fechaEspaniol__wdm($fecha)
	{
		$a_dias = array('Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb');
		$a_meses 	= array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');

		$f = strtotime($fecha);

		return $a_dias[date('w', $f)] . ', ' . date('d', $f) . ' ' . $a_meses[date('m', $f)-1];	
	}

	public static function fechaEspaniol_WdMHi($fecha)
	{
		$a_dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
		$a_meses 	= array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

		$f = strtotime($fecha);

		return $a_dias[date('w', $f)] . ', ' . date('d', $f) . ' de ' . $a_meses[date('m', $f)-1] . ' a horas ' . date('H:i', $f);
	}

	public static function tiempoDescripcion($minutos)
	{
		$min = $minutos % 60;

		$hr = ($minutos - $min) / 60;

		return $hr . 'hr ' . $min . 'min';
	}

	public static function tiempoDescripcionConPunto($tiempo)
	{
		$a_hora = explode('.', $tiempo);
		return $a_hora[0] . 'hr ' . $a_hora[1] . 'min';
	}

	public static function diferenciaHorasMinutos($date)
	{
		$fecha = strtotime($date);
		$now   = strtotime('now');

		$diferencia = $now - $fecha;

		$respuesta = '';

		if($diferencia > 86400)
		{
			$dias = floor($diferencia/86400);
			
			if($dias > 1)
				return  $dias . ' días';
			else
				return '1 día';
		}
		else if($diferencia > 3600)
		{
			return floor($diferencia/3600) . 'hr ';
		}
		else
			return floor(($diferencia % 3600)/60) . 'min';
	}

	public static function fechaEspaniolDM($fecha)
	{
		$a_meses 	= array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');

		$fecha 		= strtotime($fecha);

		return date('d', $fecha) . '/' . $a_meses[date('m', $fecha)-1];
	}

	public static function fechaEspaniolDMSinSlash($fecha)
	{
		$a_meses 	= array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');

		$fecha 		= strtotime($fecha);

		return date('d', $fecha) . $a_meses[date('m', $fecha)-1];
	}

	public static function fechaEspaniolDiaFechaMes($f)
	{
		$a_dias = array('Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb');
		$a_meses 	= array('Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');

		return ((object)array('dia' => $a_dias[date('w', $f)], 'fecha' => date('d', $f) . ' ' . $a_meses[date('m', $f)-1]));
	}

	public static function edad($fecha)
	{
		list($Y,$m,$d) = explode("-",$fecha);
    	
    	return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
	}

	public static function fechadmYConSlash($fecha)
	{
		list($Y,$m,$d) = explode("-",$fecha);

		return $d . '/' . $m . '/' . $Y;
	}

}
?>