<?php
/**
* 
*/
class Telefono extends Eloquent
{

	protected $table 	= 'bb_telefono';
	protected $fillable = array('numero', 'idciudad', 'idpasajero');

	public function ciudad()
	{
		return $this->belongsTo('Ciudad', 'idciudad');
	}

}
?>