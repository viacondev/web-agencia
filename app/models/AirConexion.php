<?php
/**
* 
*/
class AirConexion
{
	static function consultar($ruta, $postdata)
	{
		$opts 	= array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded',
		        'content' => $postdata
		    )
		);

		$context  	= stream_context_create($opts);

		$results 	= fopen(Config::get('app.airUrlMotor') . $ruta, "r", false, $context);

		$output = '';
		
		while (($buffer = fgets($results, 4096)) !== false) 
        {
            $output =$output.$buffer;
        }
        fclose($results); 

		return json_decode($output);
	}
}

?>