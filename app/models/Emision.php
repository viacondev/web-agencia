<?php
/**
* 
*/
class Emision extends Eloquent
{

    protected $table    = 'air_emision';
    protected $fillable = array('pendientes', 'estado', 'idpnr', 'idusuario');

    public function pnr()
    {
        return $this->belongsTo('Pnr', 'idpnr');
    }

    public function usuario()
    {
        return $this->belongsTo('Usuario', 'idusuario');
    }

}

?>