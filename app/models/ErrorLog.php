<?php
/**
* 
*/
class ErrorLog extends Eloquent
{
    
    protected $table = 'bb_error_log';
    protected $fillable = array('mensaje', 'archivo', 'linea', 'pila');

}

?>