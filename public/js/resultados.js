$(function () {

	$.ajax({
		type 		: 'get',
		url			: 'resultado',
        cache 		: false,
		data		: 'n=0',
		beforeSend	: function() {
			$('.loading').removeClass('hidden');
		},
		complete 	: function(data) {
			$('.before-price').click(function() {
				$('.consulta_anterior' + $(this).attr('id')).submit();
			});
		},
		success 	: function(data) {
			$('.loading').addClass('hidden');
			$('.msj-booking').addClass('hidden');
			$('.results').html(data);
			$('.mejor-precio').html('Mejor Precio Encontrado<br/>' + $('#mejor-precio').val());

			mostrarResultados();
		},
		error 		: function(errors) {
			$('.loading').addClass('hidden');
			$('.error-panel').removeClass('hidden');
		}
	});

});

function mostrarResultados()
{
	$('.row-air-details-container').hide();
	// $('.price-details').hide();

	////------- Custom Carousel
	$('.custom-carousel').each(function(){
		var owl = jQuery(this),
			itemsNum = $(this).attr('data-appeared-items'),
			sliderNavigation = $(this).attr('data-navigation');
			
		if ( sliderNavigation == 'false' || sliderNavigation == '0' ) {
			var returnSliderNavigation = false
		}else {
			var returnSliderNavigation = true
		}
		if( itemsNum == 1) {
			var deskitemsNum = 1;
			var desksmallitemsNum = 1;
			var tabletitemsNum = 1;
		} 
		else if (itemsNum >= 2 && itemsNum < 4) {
			var deskitemsNum = itemsNum;
			var desksmallitemsNum = itemsNum - 1;
			var tabletitemsNum = itemsNum - 1;
		} 
		else if (itemsNum >= 4 && itemsNum < 8) {
			var deskitemsNum = itemsNum -1;
			var desksmallitemsNum = itemsNum - 2;
			var tabletitemsNum = itemsNum - 3;
		} 
		else {
			var deskitemsNum = itemsNum -3;
			var desksmallitemsNum = itemsNum - 6;
			var tabletitemsNum = itemsNum - 8;
		}
		owl.owlCarousel({
			slideSpeed : 300,
			stopOnHover: true,
			autoPlay: false,
			navigation : returnSliderNavigation,
			pagination: false,
			lazyLoad : true,
			items : itemsNum,
			itemsDesktop : [1000,deskitemsNum],
			itemsDesktopSmall : [900,desksmallitemsNum],
			itemsTablet: [600,tabletitemsNum],
			itemsMobile : false,
			transitionStyle : "goDown",
		});
	});

	$('.owl-prev').html('<i class="fa fa-angle-left"></i>');
	$('.owl-next').html('<i class="fa fa-angle-right"></i>');

	$('[data-toggle="tooltip"]').tooltip()

	$('.icon-air').click(function() {
		if($(this).hasClass('fa-arrow-circle-down'))
		{
			$(this).removeClass('fa-arrow-circle-down');
			$(this).addClass('fa-arrow-circle-up');
			$('.details' + $(this).attr('id')).slideDown();
		}
		else
		{
			$(this).removeClass('fa-arrow-circle-up');	
			$(this).addClass('fa-arrow-circle-down');
			$('.details' + $(this).attr('id')).slideUp();
		}
	});

	$('.icon-price').click(function () {

		if($(this).hasClass('fa-chevron-down'))
		{
			$(this).removeClass('fa-chevron-down');
			$(this).addClass('fa-chevron-up');
			$('.price-details' + $(this).attr('id')).slideDown();
		}
		else
		{
			$(this).removeClass('fa-chevron-up');	
			$(this).addClass('fa-chevron-down');
			$('.price-details' + $(this).attr('id')).slideUp();
		}

	});	

	$('.btn-select').click(function() {

		var opcion = $(this).attr('id');

		$.ajax({
			type 		: 'get',
			url			: 'select_itinerario/' + opcion,
			cache 		: false,
			beforeSend	: function() {

				$('.results').html('');
				$('.loading').removeClass('hidden');

			},
			complete 	: function(data) {

			},
			success 	: function(data) {

				if(data == 'ERROR')
				{
					$('.loading').addClass('hidden');
					$('.error-panel').removeClass('hidden');
				}
				else if(data == 'CORRECTO')
				{
					window.location = "pasajeros";
				}
				else
				{
					$('.loading').addClass('hidden');
					$('.results').html(data);
					mostrarResultados();
				}

			},
			error 		: function(errors) {
				$('.loading').addClass('hidden');
				$('.error-panel').removeClass('hidden');
			}
		});

	});

	$('.icon-air-select').click(function() {

		var valor = $(this).attr('id');
		var tramo = valor.split('_')[1];

		if($(this).hasClass('fa-square-o'))
		{
			$.ajax({
				type 		: 'get',
				url			: 'select_tramo/' + valor,
				cache 		: false,
				beforeSend	: function() {
					$('.results').html('');
					$('.loading').removeClass('hidden');
				},
				complete 	: function(data) {

				},
				success 	: function(data) {
					$('.loading').addClass('hidden');
					$('.results').html(data);
					mostrarResultados();
				},
				error 		: function(errors) {
					$('.loading').addClass('hidden');
					$('.error-panel').removeClass('hidden');
				}
			});
		}
		else
		{
			$.ajax({
				type 		: 'get',
				url			: 'quitar_tramo/' + valor,
				cache 		: false,
				beforeSend	: function() {
					$('.results').html('');
					$('.loading').removeClass('hidden');
				},
				complete 	: function(data) {

				},
				success 	: function(data) {
					$('.loading').addClass('hidden');
					$('.results').html(data);
					mostrarResultados();
				},
				error 		: function(errors) {
					$('.loading').addClass('hidden');
					$('.error-panel').removeClass('hidden');
				}
			});
		}

	});

	$('.item-resumen').click(function() {

		var aerolinea = $(this).attr('data-airline');
		var escalas = $(this).attr('data-escalas');

		$.ajax({
			type 		: 'get',
			url			: 'select_aer_esc/' + aerolinea + '/' + escalas,
			cache 		: false,
			beforeSend	: function() {
				$('.results').html('');
				$('.loading').removeClass('hidden');
			},
			complete 	: function(data) {

			},
			success 	: function(data) {
				$('.loading').addClass('hidden');
				$('.results').html(data);
				mostrarResultados();
			},
			error 		: function(errors) {
				$('.loading').addClass('hidden');
				$('.error-panel').removeClass('hidden');
			}
		});

	});

	$('.paginacion').click(function() {

		var pagina = $(this).attr('id');

		$.ajax({
			type 		: 'get',
			url			: 'ir_pagina/' + pagina,
			cache 		: false,
			beforeSend	: function() {
				$('.results').html('');
				$('.loading').removeClass('hidden');
			},
			complete 	: function(data) {

			},
			success 	: function(data) {
				$('.loading').addClass('hidden');
				$('.results').html(data);
				mostrarResultados();
			},
			error 		: function(errors) {
				$('.loading').addClass('hidden');
				$('.error-panel').removeClass('hidden');
			}
		});

	});

	$('.link-busqueda').click(function() {

		$('.form-' + $(this).attr('id')).submit();

	});

	$('.ver-todos').click(function() {

		$.ajax({
			type 		: 'get',
			url			: 'quitar_filtros',
			cache 		: false,
			beforeSend	: function() {
				$('.results').html('');
				$('.loading').removeClass('hidden');
			},
			complete 	: function(data) {

			},
			success 	: function(data) {
				$('.loading').addClass('hidden');
				$('.results').html(data);
				mostrarResultados();
			},
			error 		: function(errors) {
				$('.loading').addClass('hidden');
				$('.error-panel').removeClass('hidden');
			}
		});

	});
}