$(function() {

	$('.add-doc').click(function() {

		var cant_doc 	= parseInt($('#cant_doc').val()) + 1;

		var html 	= $('.template-doc').html();
		html 		= html.replace();
		html 		= html.replace(/_x_/g, cant_doc);

		$('.panel-doc').append(html);

		$('#cant_doc').val(cant_doc);

	});

	$('.del-doc').click(function() {

		var cant_doc 	= parseInt($('#cant_doc').val());

		if($('#iddoc' + cant_doc).length)
		{
			if(confirm('Este documento está registrado,¿ está seguro de eliminarlo ?'))
			{
				$('#docs_deleted').val($('#docs_deleted').val() + $('#iddoc' + cant_doc).val() + ';');

				$('.doc' + cant_doc).remove();

				$('#cant_doc').val(cant_doc-1);		
			}
		}
		else
		{
			$('.doc' + cant_doc).remove();

			$('#cant_doc').val(cant_doc-1);
		}

	});

	$('.add-telf').click(function() {

		var cant_telf 	= parseInt($('#cant_telf').val()) + 1;

		var html 	= $('.template-telf').html();
		html 		= html.replace();
		html 		= html.replace(/_x_/g, cant_telf);

		$('.panel-telf').append(html);

		$('#cant_telf').val(cant_telf);

	});

	$('.del-telf').click(function() {

		var cant_telf 	= parseInt($('#cant_telf').val());

		if($('#idtelf' + cant_telf).length)
		{
			if(confirm('Este teléfono está registrado,¿ está seguro de eliminarlo ?'))
			{
				$('#telfs_deleted').val($('#telfs_deleted').val() + $('#idtelf' + cant_telf).val() + ';');

				$('.telf' + cant_telf).remove();

				$('#cant_telf').val(cant_telf-1);
			}
		}
		else
		{
			$('.telf' + cant_telf).remove();

			$('#cant_telf').val(cant_telf-1);
		}

	});

	$('.add-ff').click(function() {

		var cant_ff 	= parseInt($('#cant_ff').val()) + 1;

		var html 	= $('.template-ff').html();
		html 		= html.replace();
		html 		= html.replace(/_x_/g, cant_ff);

		$('.panel-ff').append(html);

		$('#cant_ff').val(cant_ff);

	});

	$('.del-ff').click(function() {

		var cant_ff 	= parseInt($('#cant_ff').val());

		if($('#idff' + cant_ff).length)
		{
			if(confirm('Este ViajFrecuente está registrado,¿ está seguro de eliminarlo ?'))
			{
				$('#ffs_deleted').val($('#ffs_deleted').val() + $('#idff' + cant_ff).val() + ';');

				$('.ff' + cant_ff).remove();

				$('#cant_ff').val(cant_ff-1);
			}
		}
		else
		{
			$('.ff' + cant_ff).remove();

			$('#cant_ff').val(cant_ff-1);
		}

	});

	$('#form-pax').submit(function() {

		$('.field-required').removeClass('required');

		var registrar = true;
		for (var i = 0; i < $('.field-required').length; i++) 
		{
			if($($('.field-required')[i]).val() == '' && $($('.field-required')[i]).attr('name').indexOf('_x_') == -1)
			{
				$($('.field-required')[i]).addClass('required');
				registrar = false;
			}
		};

		if(registrar)
		{
			$('#form-pax').submit();
		}

		return false;

	});

});