$(function(){

	$('.link-busqueda').click(function() {
		$('.buscar' + $(this).attr('id')).submit();
	});

	/*
	* Checkbox para tarifas de operadora
	*/

		$('.check-mayorista').click(function() {

			if($('.tarifa_mayorista').is(':checked'))
			{
				$('.check-mayorista').removeClass('check-mayorista-clicked');
				$('.check-mayorista').removeClass('fa-check-square-o');
				$('.check-mayorista').addClass('fa-square-o');
				$('.text-mayorista').removeClass('check-mayorista-clicked');
				$(".tarifa_mayorista").prop("checked", false);
			}
			else
			{
				$('.check-mayorista').addClass('check-mayorista-clicked');
				$('.check-mayorista').removeClass('fa-square-o');
				$('.check-mayorista').addClass('fa-check-square-o');
				$('.text-mayorista').addClass('check-mayorista-clicked');
				$(".tarifa_mayorista").prop("checked", true);
			}

		});

	/*
	* Checkbox para fechas flexibles
	*/

		$('.check-flex').click(function() {

			if($('.fecha_flexible').is(':checked'))
			{
				$('.check-flex').removeClass('check-flex-clicked');
				$('.check-flex').removeClass('fa-check-square-o');
				$('.check-flex').addClass('fa-square-o');
				$('.text-flex').removeClass('check-flex-clicked');
				$(".fecha_flexible").prop("checked", false);
			}
			else
			{
				$('.check-flex').addClass('check-flex-clicked');
				$('.check-flex').removeClass('fa-square-o');
				$('.check-flex').addClass('fa-check-square-o');
				$('.text-flex').addClass('check-flex-clicked');
				$(".fecha_flexible").prop("checked", true);
			}

		});

	/*
	* Agregar y Quitar vuelo
	*/

		$('#add-fly').click(function() {

			var cantidad = parseInt($('#vuelos').val());
			if(cantidad < 3)
			{
				cantidad++;

				var html = $('#multidestino-template').html();
				html = html.replace(/_x_/g, (cantidad+2));
				$('#multidestino-container').append(html);		

				$('#vuelos').val(cantidad);

				$("#origen_" + (cantidad+2)).autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('origen_' + (cantidad+2)); } });
			    $("#destino_" + (cantidad+2)).autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('destino_' + (cantidad+2)); } });

				$("#fecha_salida_" + (cantidad+2)).datepicker({ minDate: $("#fecha_salida_" + (cantidad+1)).datepicker("getDate"), numberOfMonths: 1});
    			$("#fecha_salida_" + (cantidad+2)).datepicker("option", "dateFormat", 'dd/mm/yy');
    			$("#fecha_salida_" + (cantidad+1)).datepicker("option", "onClose", function(date) { $("#fecha_salida_" + (cantidad+2)).datepicker("option", "minDate", date); } );
			}

		});

		$('#delete-fly').click(function() {

			var cantidad = parseInt($('#vuelos').val());
			if(cantidad > 0)
			{
				$('#fly-' + (cantidad + 2)).remove();
				$('#vuelos').val(cantidad - 1);	
			}

		});

	/*
	* Selectores de fechas
	*/

		var fecha_salida1 = $("#fecha_salida1").val();
		$("#fecha_salida1").datepicker({ minDate: 0, numberOfMonths: 1});
    	$("#fecha_salida1").datepicker("option", "dateFormat", 'dd/mm/yy');
    	$("#fecha_salida1").val(fecha_salida1);

    	var fecha_salida2 = $("#fecha_salida2").val();
    	$("#fecha_salida2").datepicker({ minDate: 0, numberOfMonths: 1, onClose: function(date) { $("#fecha_retorno").datepicker("option", "minDate", date); }});
    	$("#fecha_salida2").datepicker("option", "dateFormat", 'dd/mm/yy');
    	$("#fecha_salida2").val(fecha_salida2);

    	var fecha_retorno = $("#fecha_retorno").val();
    	$("#fecha_retorno").datepicker({ minDate: 0, numberOfMonths: 1 });
    	$("#fecha_retorno").datepicker("option", "dateFormat", 'dd/mm/yy');
    	$("#fecha_retorno").val(fecha_retorno);

    	for (var i=1; $("#fecha_salida_" + i).length; i++) 
    	{
    		var fecha 	= $("#fecha_salida_" + i).val();

    		if($("#fecha_salida_" + (i+1)).length)
    			$("#fecha_salida_" + i).datepicker({ minDate: 0, numberOfMonths: 1, onClose: function(date) { $("#fecha_salida_" + (i+1)).datepicker("option", "minDate", date); }});
    		else
    			$("#fecha_salida_" + i).datepicker({ minDate: 0, numberOfMonths: 1});

    		$("#fecha_salida_" + i).datepicker("option", "dateFormat", 'dd/mm/yy');
    		$("#fecha_salida_" + i).val(fecha);
    	};

    /*
    * Autocompletado de aeropuertos
    */

    	$("#origen1").autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('origen1'); } });
	    $("#destino1").autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('destino1'); } });

	    $("#origen2").autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('origen2'); } });
	    $("#destino2").autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('destino2'); } });

	    $("#origen_1").autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('origen_1'); } });
	    $("#destino_1").autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('destino_1'); } });

	    $("#origen_2").autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('origen_2'); } });
	    $("#destino_2").autocomplete({ source: lsa, change: function( event, ui ) { actualizar_codigo_aeropuerto('destino_2'); } });

	/*
	* Validacion de formulario
	*/

		$('.submit-booking').submit(function(){
			var id = $(this).attr('id');
			var sw = true;

			switch(id) {
				case '1':
				{
					if($('#codigo_origen1').val() == '')
					{
						$('#origen1').addClass('required');
						sw = false;	
					}
					else
						$('#origen1').removeClass('required');

					if($('#codigo_destino1').val() == '')
					{
						$('#destino1').addClass('required');
						sw = false;	
					}
					else
						$('#destino1').removeClass('required');

					if($('#fecha_salida1').val() == '')
					{
						$('#fecha_salida1').addClass('required');
						sw = false;	
					}
					else
						$('#fecha_salida1').removeClass('required');

					return sw;
				}
				break;

				case '2':
				{
					if($('#codigo_origen2').val() == '')
					{
						$('#origen2').addClass('required');
						sw = false;	
					}
					else
						$('#origen2').removeClass('required');

					if($('#codigo_destino2').val() == '')
					{
						$('#destino2').addClass('required');
						sw = false;	
					}
					else
						$('#destino2').removeClass('required');

					if($('#fecha_salida2').val() == '')
					{
						$('#fecha_salida2').addClass('required');
						sw = false;	
					}
					else
						$('#fecha_salida2').removeClass('required');

					if($('#fecha_retorno').val() == '')
					{
						$('#fecha_retorno').addClass('required');
						sw = false;	
					}
					else
						$('#fecha_retorno').removeClass('required');

					return sw;
				}
				break;

				case '3':
				{
					var cantidad = parseInt($('#vuelos').val()) + 2;
					for (var i = 1; i <= cantidad; i++) 
					{
						if($('#codigo_origen_' + i).val() == '')
						{
							$('#origen_' + i).addClass('required');
							sw = false;	
						}
						else
							$('#origen_' + i).removeClass('required');

						if($('#codigo_destino_' + i).val() == '')
						{
							$('#destino_' + i).addClass('required');
							sw = false;	
						}
						else
							$('#destino_' + i).removeClass('required');

						if($('#fecha_salida_' + i).val() == '')
						{
							$('#fecha_salida_' + i).addClass('required');
							sw = false;	
						}
						else
							$('#fecha_salida_' + i).removeClass('required');
					}

					return sw;
				}
				break;

				default:
				{
					return false;
				}
				break;
			}
		});
});

function actualizar_codigo_aeropuerto(input) 
{
    a = $('#' + input).val();
    
    if (a.length > 5) 
    {
        iata = a.substring(a.indexOf("(") + 1 , a.indexOf(")"));
        $('#codigo_' + input).val(iata.toUpperCase());
    }
    else if (a.length == 3)
        $('#codigo_' + input).val(a.toUpperCase());
}

