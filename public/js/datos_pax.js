$(function() {

	$('.buscar-pasajero').hide();

	var fecha_actual = new Date(parseInt($('#anio_actual').val(), 10), parseInt($('#mes_actual').val(), 10) - 1, parseInt($('#fecha_actual').val(), 10));

	$(".fecha_nac_ADT").datepicker({ numberOfMonths: 1, changeMonth: true, changeYear: true, maxDate: fecha_actual, minDate: new Date(fecha_actual.getFullYear() - 90, fecha_actual.getMonth(), fecha_actual.getDate() + 1), yearRange: (fecha_actual.getFullYear() - 90) + ':' + fecha_actual.getFullYear()});
	$(".fecha_nac_ADT").datepicker("option", "dateFormat", 'dd/mm/yy');

	$(".fecha_nac_CNN").datepicker({ numberOfMonths: 1, changeMonth: true, changeYear: true, maxDate: fecha_actual, minDate: new Date(fecha_actual.getFullYear() - 12, fecha_actual.getMonth(), fecha_actual.getDate() + 1), yearRange: (fecha_actual.getFullYear() - 12) + ':' + fecha_actual.getFullYear()});
	$(".fecha_nac_CNN").datepicker("option", "dateFormat", 'dd/mm/yy');

	$(".fecha_nac_INF").datepicker({ numberOfMonths: 1, changeMonth: true, changeYear: true, maxDate: fecha_actual, minDate: new Date(fecha_actual.getFullYear() - 2, fecha_actual.getMonth(), fecha_actual.getDate() + 1), yearRange: (fecha_actual.getFullYear() - 2) + ':' + fecha_actual.getFullYear()});
	$(".fecha_nac_INF").datepicker("option", "dateFormat", 'dd/mm/yy');

	/*$('.icon-close').click(function() {

		$('.panel-pax' + $(this).attr('id')).slideUp();

	});*/

	/*$('.title-pax').click(function() {

		var nombre 		= $('#nombre_' + $(this).attr('id')).val();
		var apellido 	= $('#apellido_' + $(this).attr('id')).val();
		var myid 		= $(this).attr('id');

		if(nombre != '' && apellido != '')
		{
			$.ajax({
				type 		: 'get',
				url			: 'buscar_datos_pax/' + nombre + '/' + apellido + '/' + myid,
				cache 		: false,
				beforeSend	: function() {

					var myid = $(this).attr('id');

				},
				complete 	: function(data) {

				},
				success 	: function(data) {
					var a_data = data.split('<!--Separador-->');

					$('.busqueda_pasajeros' + a_data[1].trim()).html(a_data[0]);
					$('.panel-pax' + a_data[1].trim()).slideDown();

					$('.btn-select-pax' + a_data[1].trim()).click(function() {

						var id 		= $(this).attr('data-id');
						var index 	= $(this).attr('data-index');

						$('#id_' + id).val($('#id' + id + '-' + index).val());
						$('#nombre_' + id).val($('#nombre' + id + '-' + index).val());
						$('#apellido_' + id).val($('#apellido' + id + '-' + index).val());

						if($('#doc' + id + '-' + index + ' option:selected').length > 0)
						{
						   	$('#documento_' + id).val($('#doc' + id + '-' + index + ' option:selected').attr('data-numero'));
							$('#pais_' + id).val($('#doc' + id + '-' + index + ' option:selected').attr('data-pais'));
						}

						$('#nit_' + id).val($('#nit' + id + '-' + index).val());
						$('#correo_' + id).val($('#mail' + id + '-' + index).val());
						$('#nacimiento_' + id).val($('#nacimiento' + id + '-' + index).val());
						$('#sexo_' + id).val($('#sexo' + id + '-' + index).val());
						
						var selected_ff = $('#ff' + id + '-' + index + ' option:selected');
						if($("#programa_aerolinea_" + id + " option[value='" + selected_ff.attr('data-aerolinea') + "']").length > 0)
						{
							
							$('#ffly_' + id).val(selected_ff.attr('data-codigo'));
							$('#programa_aerolinea_' + id).val(selected_ff.attr('data-aerolinea'));

							$('#nombre_' + id).val(selected_ff.attr('data-nombre'));
							$('#apellido_' + id).val(selected_ff.attr('data-apellido'));
						}

						$('#telefono_' + id).val($('#telf' + id + '-' + index + ' option:selected').attr('data-numero'));
						$('#cod_telefono_' + id).val($('#telf' + id + '-' + index + ' option:selected').attr('data-ciudad'));

					});
				},
				error 		: function(errors) {
					
				}
			});

		}

	});*/

	$('.submit-booking').submit(function() {

		$('.field-required').removeClass('required');	

		var valido = true;
		for (var i = 0; i < $('.field-required').length; i++) 
		{
			if($($('.field-required')[i]).val() == "")
			{
				$($('.field-required')[i]).addClass('required');
				valido = false;
			}
		};

		if(valido)
		{
			$(this).hide();
			$('.loading').removeClass('hidden');

			var form = $(this);

			$.ajax({
				type 		: form.attr('method'),
				url			: form.attr('action'),
				cache 		: false,
				data		: form.serialize(),
				beforeSend	: function() {
					
				},
				complete 	: function(data) {

				},
				success 	: function(data) {
					var a_data = data.split(':');
					
					if(a_data[0] == 'CORRECTO')
					{
						window.location = "pnr/" + a_data[1];
					}
					else if(data == 'DUPLICIDAD')
					{
						$('.loading').addClass('hidden');
						$('.duplicado-panel').removeClass('hidden');	
					}
					else if(data == 'DESCARTAR')
					{
						window.location = "descartar";
					}
					else
					{
						$('.loading').addClass('hidden');
						$('.error-panel').removeClass('hidden');
					}
				},
				error 		: function(errors) {
					$('.loading').addClass('hidden');
					$('.error-panel').removeClass('hidden');
				}
			});
		}

		return false;

	});

	$('.show-form').click(function() {

		$('.error-panel').addClass('hidden');
		$('.duplicado-panel').addClass('hidden');
		$('.submit-booking').show();

	});

});