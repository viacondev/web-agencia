$(function() {

	$('.link-print-area').click(function() {
        $('#print-area').printArea();
    });

	$('.btn-view').click(function() {

		var id = $(this).attr('id');

		$('.vista-pnr').addClass('hidden');
		$('.' + id).removeClass('hidden');

		$('.btn-view').removeClass('view-current');
		$(this).addClass('view-current');

		/*var other_id = ( id == 'vista-pantalla' ) ? 'vista-impresion' : 'vista-pantalla';

		$('.' + other_id).hide();
		$('.' + id).show();

		$(this).addClass('view-current');
		$('#' + other_id).removeClass('view-current');*/

	});

	$('.check-flex').click(function() {

		if($(this).hasClass('check-flex-clicked'))
		{
			$('.check-flex').removeClass('check-flex-clicked');
			$('.check-flex').removeClass('fa-check-square-o');
			$('.check-flex').addClass('fa-square-o');
			$('.text-flex').removeClass('check-flex-clicked');
			$('.mostrar-precios').hide();
		}
		else
		{
			$('.check-flex').addClass('check-flex-clicked');
			$('.check-flex').removeClass('fa-square-o');
			$('.check-flex').addClass('fa-check-square-o');
			$('.text-flex').addClass('check-flex-clicked');
			$('.mostrar-precios').show();
		}

	});

	$('.rad-precio').change(function() {

		if($('.rad-precio:checked').val() == 0)
		{
			$('.precios-finales').addClass('hidden');
			$('.precios-agencia').removeClass('hidden');
		}
		else
		{
			$('.precios-agencia').addClass('hidden');
			$('.precios-finales').removeClass('hidden');
		}

	});

	$('.fee-aplicado').change(function() {

		var valor 	= parseFloat($(this).val());
		var id 		= $(this).attr('id');

		var monto		= parseFloat($('#monto' + id).val());
		var moneda		= $('#moneda' + id).val();
		var cod_moneda	= $('#cod_moneda' + id).val();
		var cantidad	= parseInt($('#cantidad' + id).val());

		$('#final' + id).val(moneda + ' ' + Math.ceil(monto+valor));
		$('#subtotal' + id).val(moneda + ' ' + Math.ceil((monto+valor) * cantidad));
		$('#monto_subtotal' + id).val(Math.ceil((monto+valor) * cantidad));

		var total = 0;
		for (var i = 0; $('#monto_subtotal' + i).length > 0; i++) 
		{
			if($('#cod_moneda' + i).val() == cod_moneda)
				total += parseFloat($('#monto_subtotal' + i).val());
		};

		$('#total' + cod_moneda).html(moneda + ' ' + Math.ceil(total));

	});

	$('.form-mail-pnr').submit(function() {

		var form = $(this);
		if($('#direccion-email').val() != '')
		{
			$.ajax({
				type 		: form.attr('method'),
				url			: form.attr('action'),
				data		: form.serialize(),
				beforeSend	: function() {
					$('#btn-enviar-mail').addClass('hidden');
					$('#loading-enviar-mail').removeClass('hidden');
				},
				complete 	: function(data) {

				},
				success 	: function(data) {
					
					$('#loading-enviar-mail').addClass('hidden');

					if(data == 'CORRECTO')
						$('.enviar-mail-sucess').removeClass('hidden');
					else
						$('.enviar-mail-error').removeClass('hidden');

					setTimeout(function(){ 
						$('.enviar-mail-sucess').addClass('hidden');
						$('.enviar-mail-error').addClass('hidden');
						$('#btn-enviar-mail').removeClass('hidden');
						$('.enviar-mail').modal('hide');
					}, 2000);
				},
				error 		: function(errors) {
					$('#loading-enviar-mail').addClass('hidden');

					$('.enviar-mail-error').removeClass('hidden');

					setTimeout(function(){ 
						$('.enviar-mail-error').addClass('hidden');
						$('#btn-enviar-mail').removeClass('hidden');
						$('.enviar-mail').modal('hide');
					}, 2000);	
				}
			});
		}

		return false;
	});

	$('#form-emision').submit(function() {

		var form 	= $(this);

		$.ajax({
			type 		: form.attr('method'),
			url			: form.attr('action'),
			data		: form.serialize(),
			beforeSend	: function() {
				$('#resultado-emision-mensaje').attr('class', 'alert');
				$('#panel-envio-emision').addClass('hidden');	
				$('#loading-envio-emision').removeClass('hidden');
			},
			complete 	: function(data) {
				$('#loading-envio-emision').addClass('hidden');	
				$('#resultado-emision').removeClass('hidden');
			},
			success 	: function(data) {
			
				data = JSON.parse(data);

				if('correcto' in data)
				{
					$('#resultado-emision-mensaje').html(data.correcto);
					$('#resultado-emision-mensaje').addClass('alert-success');

				    $("#progressTimer").progressTimer({
						timeLimit 		: parseInt(data.tiempo_estimado, 10),
						baseStyle 		: '',  //bootstrap progress bar style at the beginning of the timer
				        warningStyle 	: 'progress-bar-warning',  //bootstrap progress bar style in the warning phase
				        completeStyle 	: 'progress-bar-success',  //bootstrap progress bar style at completion of timer
				        onFinish 		: function() { location.reload(); }
					});
				}
				else if('errores' in data)
				{
					$('#resultado-emision-mensaje').html(data.errores);
					$('#resultado-emision-mensaje').addClass('alert-danger');
				}

			},
			error 		: function(errors) {
				$('#resultado-emision-mensaje').html('No se pudo completar la transacci&oacute;n, verifique la conexi&oacute;n a Internet.');
				$('#resultado-emision').addClass('alert-danger');
			}
		});

		return false;
	});

});