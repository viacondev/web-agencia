$(function() {
	
	$('#archivo').change(function(){

		$('#url-archivo').val($(this).val());

		var form 	= $('#subir-logo');

		var data 	= new FormData();

		data.append('logo', document.getElementById('archivo').files[0]);

		$.ajax({
			type 		: form.attr('method'),
			url 		: form.attr('action'),
			contentType : false,
            processData : false,
			data 		: data,
			beforeSend 	: function() {
				$('.config-logo-sucess').addClass('hidden');
				$('.config-logo-error').addClass('hidden');
				$('#config-logo').addClass('hidden');
				$('#loading-config-logo').removeClass('hidden');
			},
			complete 	: function(data) {

			},
			success 	: function(data) {
				if(data == '1')
				{
					$('#el-logo').attr('src', $('#el-logo').attr('src') + '?' + new Date().getTime());
					$('.config-logo-sucess').removeClass('hidden');
				}
				else
					$('.config-logo-error').removeClass('hidden');

				$('#loading-config-logo').addClass('hidden');
				$('#config-logo').removeClass('hidden');
			},
			error 		: function(errors) {
				$('.config-logo-error').removeClass('hidden');
				$('#loading-config-logo').addClass('hidden');
				$('#config-logo').removeClass('hidden');
			}
		});

	});

	$('#airconfig').click(function() {

		var form = $('#form_airconfig');

		$.ajax({
			type 		: form.attr('method'),
			url			: form.attr('action'),
			data		: form.serialize(),
			beforeSend	: function() {
				$('.config-gral-sucess').addClass('hidden');
				$('.config-gral-error').addClass('hidden');
				$('#config-gral').addClass('hidden');
				$('#loading-config-gral').removeClass('hidden');
			},
			complete 	: function(data) {

			},
			success 	: function(data) {
				if(data == '1')
					$('.config-gral-sucess').removeClass('hidden');
				else
					$('.config-gral-error').removeClass('hidden');

				$('#loading-config-gral').addClass('hidden');
				$('#config-gral').removeClass('hidden');
			},
			error 		: function(errors) {
				$('.config-gral-error').removeClass('hidden');
				$('#loading-config-gral').addClass('hidden');
				$('#config-gral').removeClass('hidden');
			}
		});

	});

	$('#add-fee').click(function() {

		var cant_fee 	= parseInt($('#cant_fee').val()) + 1;

		var html 		= $('.template-fee').html();
		html 			= html.replace();
		html 			= html.replace(/_x_/g, cant_fee);

		$('.panel-fee-config').append(html);

		$('#cant_fee').val(cant_fee);

	});

	$('#del-fee').click(function() {

		var cant_fee 	= parseInt($('#cant_fee').val());

		if($('#idfee' + cant_fee).length)
		{
			if(confirm('Este fee está registrado,¿ está seguro de eliminarlo ?'))
			{
				$('#fees_deleted').val($('#fees_deleted').val() + $('#idfee' + cant_fee).val() + ';');

				$('.fee' + cant_fee).remove();

				$('#cant_fee').val(cant_fee-1);		
			}
		}
		else
		{
			$('.fee' + cant_fee).remove();

			$('#cant_fee').val(cant_fee-1);
		}

	});

	$('#guardar_fee').click(function() {

		var form = $('#editar_fee');

		$.ajax({
			type 		: form.attr('method'),
			url			: form.attr('action'),
			data		: form.serialize(),
			beforeSend	: function() {
				$('.config-fee-sucess').addClass('hidden');
				$('.config-fee-error').addClass('hidden');
				$('#config-fee').addClass('hidden');
				$('#loading-config-fee').removeClass('hidden');
			},
			complete 	: function(data) {

			},
			success 	: function(data) {
				if(data == '1')
					$('.config-fee-sucess').removeClass('hidden');
				else
					$('.config-fee-error').removeClass('hidden');

				$('#loading-config-fee').addClass('hidden');
				$('#config-fee').removeClass('hidden');
			},
			error 		: function(errors) {
				$('.config-fee-error').removeClass('hidden');
				$('#loading-config-fee').addClass('hidden');
				$('#config-fee').removeClass('hidden');
			}
		});

	});

	$('#contraseña').click(function() {

		$('#idusuario').val($(this).attr('sectionId'));

	});

	$('#form-password').submit(function(){
		
		$('#confirm').empty();

		var valor1 = $('#password_new').val();
		var valor2 = $('#password_repeat').val();
		
		if(valor1 != valor2) 
		{
			$('#confirm').append("Las contraseña no son iguales").removeClass('ok').addClass('ko');
		}
		if(valor1.length==0 || valor1=="")
		{
			$('#confirm').append("Las contraseña no son iguales").removeClass('ok').addClass('ko');
		}
		if(valor1.length!=0 && valor1==valor2)
		{
			var form = $(this);

			$.ajax({
				type 		: form.attr('method'),
				url			: form.attr('action'),
				data		: form.serialize(),
				beforeSend	: function() {
					$('.loading').show();
				},
				complete 	: function(data) {

				},
				success 	: function(data) {
					if(data=="no")
					{
						$('.loading').hide();
						$('#confirm').append("Las contraseña actual no es correcta").removeClass('ok').addClass('ko');
						setTimeout(function(){ $("#confirm").slideUp(); }, 3000)
					}
					else
					{
						$('.loading').hide();
						$('#confirm').append("Las contraseña fue guardada correctamente").removeClass('ko').addClass('ok').slideDown();
						$('#password_new, #password_repeat, #password_old').val('');
						setTimeout(function(){ $("#confirm").slideUp();$(".close").click();  }, 3000)
					}
				},
				error 		: function(errors) {
					$('.config-gral-error').removeClass('hidden');
					$('#loading-config-gral').addClass('hidden');
					$('#config-gral').removeClass('hidden');
				}
			});
		}	
		return false;

	});

});